<?php

ini_set('display_errors', 1);

require_once 'vendor/autoload.php';
require_once 'google_mysql_cfg.php';

echo 'SELECT * FROM servolab_medrep.request_detail where lead_result_created is not null and lead_result_created between "2020-10-01" and "2020-10-07" order by id<br>';

use TheIconic\Tracking\GoogleAnalytics\Analytics;

$analytics = new Analytics();

$rs = $link->query('SELECT * FROM servolab_medrep.request_detail where lead_result_created is not null and lead_result_created between "2020-10-09" and "2020-10-10" order by id');

while($detail = $rs->fetch_assoc()) {
    $rs2 = $link->query('select * from servolab_medrep.request where uid != "" and uid is not null and id='.$detail['request_id']);
    if($rs2->num_rows != 1) continue;
    $request = $rs2->fetch_assoc();
    $cookieId = rand(1000000000, 2147483647) . '.' . time();
// Build the order data programmatically, including each order product in the payload
// Take notice, if you want GA reports to tie this event with previous user actions
// you must get and set the same ClientId from the GA Cookie
// First, general and required hit data
    $response = $analytics->setProtocolVersion('1')
        ->setTrackingId(GA_TRACKING_ID)
        ->setClientId($cookieId)
        ->setUserId($request['uid']);

//    echo '<hr><h3>First</h3>';
//    var_dump($response);

    $event_time = strtotime($detail['lead_result_created']) * 1000;

// Then, include the transaction data
    $response = $analytics->setTransactionId('7778922')
        ->setAffiliation($request['request_type'])
        ->setRevenue($detail['actual_comissions']/100)
        ->setTax(0)
        ->setShipping(0)
        ->setCouponCode($detail['lead_code'])
        ->setUserTimingTime($event_time);

//    echo '<hr><h3>Second</h3>';
//    var_dump($response);
/*
// Include a product
    $productData1 = [
        'sku' => $detail['id'],
        'name' => $request['request_type'],
        'category' => $request['request_type'],
        'price' => $detail['actual_comissions']/100,
        'quantity' => 1,
        'coupon_code' => $detail['lead_code']
    ];

    $analytics->addProduct($productData1);

// Don't forget to set the product action, in this case to PURCHASE
    $analytics->setProductActionToPurchase();
*/
// Finally, you must send a hit, in this case we send an Event
    $response = $analytics->setEventCategory('Checkout')
        ->setEventAction('Purchase')
        ->setDebug(true)
        ->sendEvent();
    echo '<hr><h3>Final</h3>';
//    var_dump($response);

    $debugResponse = $response->getDebugResponse();

// The debug response is an associative array, you could use print_r to view its contents
    print_r($debugResponse);

}
?>
<p>ok</p>
