<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 9/27/18
 * Time: 12:20 PM
 */

require_once("cfg.php");
////////////////////////////////////////////////
$link = mysqli_connect(DB_HOST, DB_USER, DB_PWD, DB_NAME);
if ($link) {
    $link->query("set names utf8mb4");
    $link->query('SET time_zone = "Europe/Kiev"');
} else die("Can't connect to DB");
////////////////////////////////////////////////
?>
