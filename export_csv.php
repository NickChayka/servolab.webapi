<?php
ini_set('display_errors', 1);

define('DB_HOST', '146.148.29.126');
define('DB_USER', 'medrep_admin');
define('DB_PWD', 'mie5eeV5tie.xoow');
define('DB_NAME', 'servolab_medrep');

$link = mysqli_connect(DB_HOST, DB_USER, DB_PWD, DB_NAME);
if(!$link) die('Mysql connect error');
$link->query("set names utf8");

$fp = fopen('servolab.txt', 'r');
if(!$fp) die("Can't open servolab.txt");

$medreps = [];
$rs = $link->query('select * from medrep');
while($r = $rs->fetch_assoc()) {
    $medreps[$r['cmt']] = $r['id'];
}
$rs->close();

$link->query('truncate table import_tmp');
$c=0;

while($str = fgets($fp)) {
    if(trim($str)=='') continue;
    $sql = 'insert into import_tmp(doc_id, doc_phone, doc_phone_str, doc_name, medrep_name, medrep_id, created, valid_from, created_timestamp, valid_from_timestamp, hospital_id) values(';
    list($doc_id, $doc_phone, $doc_name, $medrep_name, $cdate, $hospital_id) = explode(',', $str);
    $doc_phone = trim($doc_phone);
    $doc_id = trim($doc_id);
    $doc_name = trim($doc_name);
    $medrep_name = trim($medrep_name);
    if(!array_key_exists($medrep_name, $medreps)) $mid = 0;
    else $mid = (int)$medreps[$medrep_name];
    $medrep_id = $mid;
    $dates = parse_date(trim($cdate));
    $sql.='"'.(int)$doc_id.'","'.(int)$doc_phone.'","+'.(int)$doc_phone.'","'.$link->real_escape_string($doc_name).'","'.$link->real_escape_string($medrep_name).'","'.$medrep_id.'",';
    $sql.='"'.$dates['intvalue'].'","'.$dates['intvalue'].'","'.$dates['timestamp'].'","'.$dates['timestamp'].'"';
    $sql.=')';
    $link->query($sql);
    echo $sql.'<br>';
    $c++;
}

$test = $link->query('SELECT doc_phone, COUNT( id ) cnt FROM import_tmp GROUP BY doc_phone HAVING cnt >1');
if($test->num_rows > 0) {
    $str = '<h3>Обнаружены проблемы с данными, импорт остановлен.</h3>';
    while($phone = $test->fetch_assoc()) {
        $rs = $link->query('select * from import_tmp where doc_phone = "'.$link->escape_string($phone['doc_phone']).'"');
        $str .= '<br>Номер телефона <b>'.$phone['doc_phone'].'</b>:<br>';
        while($r = $rs->fetch_assoc()) {
            $str .= 'Доктор: '.$r['doc_name'].', ID: '.$r['doc_id'].'<br>';
        }
        $rs->close();
    }
    $test->close();
    $message = '<html><head><title>Doctor list has an error</title></head><body>'.$str.'</body></html>';
    $headers  = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $headers .= "To: kz@servolab.one\r\n";
    $headers .= "Cc: inna.torkotiuk@servolab.one\r\n";
    $headers .= "From: Servolab Bot API <nickch@2kgroup.com>\r\n";
    @mail('kz@servolab.one, inna.torkotiuk@servolab.one', "Doctor list has an error", $message, $headers);
} else {
    $link->query('truncate medrep_doc');
    $link->query('insert into medrep_doc SELECT null, doc_phone, doc_phone_str, medrep_id, created, valid_from, created_timestamp, valid_from_timestamp FROM import_tmp where medrep_id>0');
    echo 'ok. '.$c.' rows added.';
}

$link->close();

////////////////////////////////////////
function parse_date($str) {
    if($str=='') {
        $ret = [
            'intvalue' => time(),
            'timestamp' => date('Y-m-d H:i:s')
        ];
    } else {
        list($day, $month, $year) = preg_split("/[\s\.\/]+/", $str);
        $ret['intvalue'] = mktime(0, 0, 1, $month, $day, $year);
        $ret['timestamp'] = date('Y-m-d H:i:s', $ret['intvalue']);
    }
    return $ret;
}

?>