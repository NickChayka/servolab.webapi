<?php

$ip=getenv("REMOTE_ADDR");
if(getenv('HTTP_X_REAL_IP') and $ip!=getenv('HTTP_X_REAL_IP')) $ip=getenv('HTTP_X_REAL_IP');
if(getenv('HTTP_X_FORWARDED_FOR') and $ip!=getenv('HTTP_X_FORWARDED_FOR')) $ip=getenv('HTTP_X_FORWARDED_FOR');

//echo $ip;

//if($ip!='157.230.96.108') die(' Not allowed IP');

$jret = [];

if($_POST) {
    $msg = trim((string)$_POST['msg']);
} else {
    $msg = trim((string)$_GET['msg']);
}

if($msg!='') $msg = str_replace('<br>', "\n", base64_decode($msg));
else {
    $jret = [
        'success' => false,
        'result' => 'No message'
    ];
    die(json_encode($jret));
}

require_once("vendor_viber/autoload.php");
require_once("cfg.php");
/*******************************************************/

use Viber\Bot;
use Viber\Api\Sender;

$botSender = new Sender([
    'name' => 'TopMedClinic Bot',
    'avatar' => 'https://staging-webapi.servolab.one/TopMed/topmed512.png',
]);

try {
    $bot = new Bot(['token' => VIBER_API_KEY]);
    foreach($VIBER_USERS as $user_id) {
        $bot->getClient()->sendMessage(
            (new \Viber\Api\Message\Text())
                ->setSender($botSender)
                ->setReceiver($user_id)
                ->setText($msg)
        );
    }
    $jret = [
        'success' => true,
        'result' => 'Ok'
    ];
} catch (Exception $e) {
    $jret = [
        'success' => false,
        'result' => 'BOT Exception: '.$e->getMessage()
    ];
}

echo json_encode($jret);
?>