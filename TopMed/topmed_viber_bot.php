<?php

ini_set('session.gc_maxlifetime', 86400);

header("HTTP/1.0 200 OK");

//die('Ok');

if ( !session_id() ) @session_start();
require_once("vendor_viber/autoload.php");
require_once("cfg.php");

/*******************************************************/
include_once('function_viber.php');
/*******************************************************/
use Viber\Bot;
use Viber\Api\Sender;

    // reply name
$botSender = new Sender([
    'name' => 'TopMedClinic Bot',
    'avatar' => 'https://staging-webapi.servolab.one/TopMed/topmed512.png',
]);

try {
    $bot = new Bot(['token' => VIBER_API_KEY]);
    $bot
        ->onConversation(function ($event) use ($bot, $botSender) {
            $txt = 'Добро пожаловать.';
            $menu_type = 'home';
            return (new \Viber\Api\Message\Text())
                ->setSender($botSender)
                ->setText($txt)
                ->setKeyboard(
                    (new \Viber\Api\Keyboard())
                        ->setButtons(menu($menu_type))
                );
        })
        ->onText('|^home$|si', function ($event) use ($bot, $botSender) {
            $USER_ID = $event->getSender()->getId();
            $txt = "Это бот-информатор. Сейчас он не выполняет никаких других функций.";
            $menu_type = 'home';
            $bot->getClient()->sendMessage(
                (new \Viber\Api\Message\Text())
                    ->setSender($botSender)
                    ->setReceiver($USER_ID)
                    ->setText($txt)
                    ->setKeyboard(
                        (new \Viber\Api\Keyboard())
                            ->setButtons(menu($menu_type))
                    )
                );
        })
        ->onText('|^info$|si', function ($event) use ($bot, $botSender) {
            $USER_ID = $event->getSender()->getId();
            $menu_type = 'home';
            $txt = "Ваш Viber ID для работы с этим ботом:\n".$USER_ID;
            $bot->getClient()->sendMessage(
                (new \Viber\Api\Message\Text())
                    ->setSender($botSender)
                    ->setReceiver($USER_ID)
                    ->setText($txt)
                    ->setKeyboard(
                        (new \Viber\Api\Keyboard())
                            ->setButtons(menu($menu_type))
                    )
            );
        })
        ->run();
} catch (Exception $e) {
    // todo - log exceptions
    $msg = date('Y-m-d')."\n".$e->getMessage()."\n";
    $fp = fopen('bot_error.log', 'a+');
    if ($bot) {
        $msg.='Actual sign: ' . $bot->getSignHeaderValue()."\n";
        $msg.='Actual body: ' . $bot->getInputBody()."\n";
    }
    fputs($fp, $msg."\n");
    fclose($fp);
}

?>
