<?php
    ini_set('display_errors', 1);
    error_reporting(E_ERROR | E_WARNING | E_PARSE);

    $raw_post_data = file_get_contents('php://input');
    $raw_post_array = explode('&', $raw_post_data);

    $ip=getenv("REMOTE_ADDR");
    if(getenv('HTTP_X_REAL_IP') and $ip!=getenv('HTTP_X_REAL_IP')) $ip=getenv('HTTP_X_REAL_IP');
    if(getenv('HTTP_X_FORWARDED_FOR') and $ip!=getenv('HTTP_X_FORWARDED_FOR')) $ip=getenv('HTTP_X_FORWARDED_FOR');


    $headers = array();
    foreach ($_SERVER as $key => $value) {
        if (strpos($key, 'HTTP_') === 0) {
           $headers[]=str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower(substr($key, 5))))).' = '.$value;
        }
    }

    $fp = fopen('/home/servolab/webapi/www/TopMed/callback.log', 'a+');
    fputs($fp, 'Date: '.date('Y-m-d H:i:s')."\n");
    fputs($fp, "IP: ".$ip."\n");
    fputs($fp, "Headers:\n");
    fputs($fp, join("\n", $headers)."\n");
    fputs($fp, "Post body:\n");
    fputs($fp, $raw_post_data."\n-------------------------------------\n\n");
    fclose($fp);

    echo $raw_post_data;