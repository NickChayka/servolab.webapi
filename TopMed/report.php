<pre>
<?php

require_once("google_mysql_cfg.php");

include('vendor/autoload.php'); //Подключаем библиотеку
use Telegram\Bot\Api;

///////////////////////////////////////////////////////////////

function report_to_telegram($report) {
    if(count($report) > 0) {
        $telegram = new Api(BOT_TOKEN); //Устанавливаем токен, полученный у BotFather
        $text = join("\n", $report);
        $text = str_replace('<li>', '- ', $text);
        $text = strip_tags(str_replace('<li>', '- ', $text));
        if($text!='') var_dump($telegram->sendMessage([ 'chat_id' => CHAT_ID, 'text' => $text]));
    }
}

///////////////////////////////////////////////////////////////

$start = mktime(0,0,0, date('n'), date('j')-1, date('Y'));
$end = mktime(23,59,59, date('n'), date('j')-1, date('Y'));

// -1 - достигнут предел
// -2 - не забираю, так как оканчивается менее чем через 2 часа или не в наши рабочие часы.
// -3 - не забираю, так как это не "Врач на дом" запрос.
// -4 - пытались забрать, но не смогли, случилась ошибка

$stat = [];
$total = 0;
$fail = 0;
$rs = $link->query('SELECT result, count(id) cnt FROM topmed.req_log where stime between '.$start.' and '.$end.' group by result');
while($r = $rs->fetch_assoc()) {
    $stat['s'.$r['result']] = $r['cnt'];
    $total+=$r['cnt'];
    if($r['result'] < 0) $fail+=$r['cnt'];
    if($r['result'] == 1) $last_success_pickup = (int)$r['max_etime'];
}

$report[] = 'Отчет за вчера ('.date('d/m/Y', $start).')';
$report[] = 'Заявок просмотрено: '.$total;
$report[] = 'Забрано для обработки: '.(int)$stat['s1'].' (максимум - '.MAX_APP_COUNT.')';
$report[] = 'Не забрал: '.(int)$fail;
$report[] = 'Стоп слова: '.(int)$stat['s-5'];
$report[] = 'Не успел забрать: '.(int)$stat['s-4'];
$report[] = 'Время окончания менее 2 часов: '.(int)$stat['s-2'];
$report[] = 'Достигнут максимум задач: '.(int)$stat['s-1'];

report_to_telegram($report);

//var_dump($report);