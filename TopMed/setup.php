<?php
ini_set('display_errors', 1);
require_once("vendor_viber/autoload.php");
use Viber\Client;
$apiKey = '4b4470f88127dcfc-c007872eb8ef8bf8-c261d55d1574a45f'; // <- PLACE-YOU-API-KEY-HERE
$webhookUrl = 'https://staging-webapi.servolab.one/TopMed/topmed_viber_bot.php'; // <- PLACE-YOU-HTTPS-URL
try {
    $client = new Client([ 'token' => $apiKey ]);
    $result = $client->setWebhook($webhookUrl);
    echo "Success!\n";
} catch (Exception $e) {
    echo "Error: ". $e->getMessage() ."\n";
}
?>