<pre>
<?php
ini_set('display_errors', 1);
require_once("google_mysql_cfg.php");

include('vendor/autoload.php'); //Подключаем библиотеку
use Telegram\Bot\Api;

///////////////////////////////////////////////////////////////
function is_our_request($obj, $total) {
    $ret = false;
    $reason = 'не забираю, так как оканчивается менее чем через 2 часа или не в наши рабочие часы.';
    $reason_int = -2;
    if($obj->inTypeInt == 1) {
        if( preg_match("/\bобл\.\b/i", $obj->inApplicationPlace) or
            preg_match("/Бортнічі/i", $obj->inApplicationPlace) or
            preg_match("/Бортничи/i", $obj->inApplicationPlace) or
            preg_match("/бровар/i", $obj->inApplicationPlace) or
            preg_match("/област/i", $obj->inApplicationPlace)) { // отсекаем стоп-слова
            $ret=false;
            $reason_int = -5;
            $reason = 'не забираю, так как эта заявка содержит стоп-слова.';
        } else {
            $intervals = $obj->inApplicationParams->intervals;
            foreach($intervals as $interval) {
                list($y, $m, $d) = explode('-', $interval->DateOfTimeInterval);
                list($from_hour, $from_minuts) = explode(':', $interval->From);
                list($to_hour, $to_minuts) = explode(':', $interval->Untill);
                if((int)$from_hour < WORK_HOUR_TO and (int)$to_hour > WORK_HOUR_FROM) {
                    $app_time = mktime($to_hour, $to_minuts, 0, $m, $d, $y);
                    if( ($app_time - time()) >= 2*60*60 ) {
                        if($total < MAX_APP_COUNT) {
                            $ret=true;
                            $reason_int = 0;
                            $reason = 'можно будет и забрать..';
                        } else {
                            $ret=false;
                            $reason_int = -1;
                            $reason = 'не забираю, так как забрал уже максимум (' . MAX_APP_COUNT . ') на сегодня.';
                        }
                        break;
                    }
                }
            }
        }
    } else {
        $reason_int = -3;
        $reason = 'не забираю, так как это не "Врач на дом" запрос.';
    }
    return ['result' => $ret, 'reason' => $reason, 'reason_int' => $reason_int];
}
function report_to_telegram($report) {
    if(count($report) > 0) {
        $telegram = new Api(BOT_TOKEN); //Устанавливаем токен, полученный у BotFather
        $text = join("\n", $report);
        $text = str_replace('<li>', '- ', $text);
        $text = strip_tags(str_replace('<li>', '- ', $text));
        if($text!='') {
            var_dump($telegram->sendMessage([ 'chat_id' => CHAT_ID, 'text' => substr($text, 0, 4096)]));
        }
    }
}
function exit_from_app($report) {
//    $report[] = 'Bot finished at '.date('Y-m-d H:i:s');
    report_to_telegram($report);
    die('</pre>');
}

function close_active_app($link, $active_app) {
    // итак, эта функция получает список тех заявок, которые были открыты ранее, а теперь пропали из выдачи, т.е. их забрали.
    // обновим им статус и проинформируем юзера
    $ret = [];
    foreach($active_app as $inID => $item) {
        if($inID > 0) {
            $ret[] = 'Заявка #'.$inID.' пропала из доступных через '.secondsToTime( time() - $item['stime'] );
            $link->query('update req_log set etime = '.time().' where id='.$item['log_id']);
        }
    }
    return $ret;
}

function secondsToTime($seconds) {
    $dtF = new \DateTime('@0');
    $dtT = new \DateTime("@$seconds");
    if($seconds < 60) $format = '%s секунд';
    elseif($seconds < 60*60) $format = '%i минут';
    elseif($seconds < 60*60*24) $format = '%h часов, %i минут';
    else $format = '%a дней, %h часов, %i минут';
    return $dtF->diff($dtT)->format($format);
}

function send_msg_to_viber($str) {
    $myCurl = curl_init();

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => WEB_SITE.'push_msg_to_viber.php',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
        CURLOPT_POSTFIELDS => 'msg='.urlencode(base64_encode($str))
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Accept: application/json",
        "Content-type: application/x-www-form-urlencoded"
    ));

    $response = curl_exec($myCurl);

    $json = json_decode($response);
    return $json->success;
}

function clean_application($application) {
    $ret['inID'] = $application->inID;
    $ret['inGUID'] = $application->inGUID;
    $ret['inTypeInt'] = $application->inTypeInt;
    $ret['inType'] = $application->inType;
    $ret['inStamp'] = $application->inStamp;
    $ret['inPersonAge'] = $application->inPersonAge;
    $ret['inApplicationPlace'] = $application->inApplicationPlace;
    $ret['inApplicationDate'] = $application->inApplicationDate;
    $ret['inApplicationNotes'] = $application->inApplicationNotes;

    $tmp = [];
    $intervals = $application->inApplicationParams->intervals;
    foreach($intervals as $interval) {
        $tmp[] = [
            'DateOfTimeInterval' => $interval->DateOfTimeInterval,
            'description' => $interval->description
        ];
    }
    $ret['Intervals'] = $tmp;

    return print_r($ret, true);
}
///////////////////////////////////////////////////////////////

// для начала получим список заявок, которые мы зафиксировали и которые ещё не были забраны
$active_app = [];
$rs = $link->query('select id, inID, stime from req_log where etime=0');
while($r = $rs->fetch_assoc()) {
    $active_app[$r['inID']] = [
        'log_id' => $r['id'],
        'stime' => $r['stime']
    ];
}

$report = [];
//$report[] = 'Bot started at '.date('Y-m-d H:i:s');
//$report[] = 'Enviroment: '.APP_ENV;

// авторизуемся
$myCurl = curl_init();

curl_setopt_array($myCurl, array(
    CURLOPT_URL => API_URI.'Validate',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POST => true,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_SSL_VERIFYHOST => 0,
    CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
    CURLOPT_POSTFIELDS => 'P='.urlencode(API_PWD_HASH).'&L='.urlencode(API_LOGIN)
));

curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
    "Accept: application/json",
    "Content-type: application/x-www-form-urlencoded"
));

$response = curl_exec($myCurl);
$info = curl_getinfo($myCurl);

//var_dump($response);

$json = json_decode($response);
$_status = 'Ok';

if(is_object($json)) {
    if($json->UserID > 0) define('ACCESS_TOKEN', $json->Key);
    else $_status = "Fail: unknown user. Please check login/password.";
} else $_status = "Fail: Can't parse auth response as JSON Object!";

//$report[] = 'Authorisation:...'.$_status;
if($_status != 'Ok') exit_from_app($report);

// так, по идее всё хорошо тут, и надо забрать все новые заявки

//$report[] = 'Get new applications';
$myCurl = curl_init();

curl_setopt_array($myCurl, array(
    CURLOPT_URL => API_URI.'GetApplications?key='.ACCESS_TOKEN,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_SSL_VERIFYHOST => 0,
    CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
));

curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
    "Accept: application/json",
    "Content-type: application/x-www-form-urlencoded"
));

$response = curl_exec($myCurl);
$info = curl_getinfo($myCurl);

//echo('***'.$response.'***');

if($response == '' or $response == '[]') {
//    $report[] = 'No new applications found.';
    $report = array_merge($report, close_active_app($link, $active_app));
    exit_from_app($report);
}

$json = json_decode($response);

//var_dump($json);

// выясним, сколько мы уже сегодня забрали
$start = mktime(0,0,0, date('n'), date('j'), date('Y'));
$end = mktime(23,59,59, date('n'), date('j'), date('Y'));

$rs = $link->query('select * from topmed.req_log where stime between '.$start.' and '.$end.' and result=1 and operation="booking"');
$total = $rs->num_rows;

if(is_array($json)) {
//    $report[] = 'Found '.count($json).' new applications.';
    foreach($json as $application) {
        if($active_app[$application->inID]) { // такая заявка у нас уже есть, пропускаем и ждём
            unset($active_app[$application->inID]);
        } else { // нет у нас такой заявки, надо записать

            $rs = $link->query('select id from topmed.req_log where inID = "'.(int)$application->inID.'"');
            if($rs->num_rows > 0) continue;

            $check = is_our_request($application, $total);
            $answer = clean_application($application);

            if($check['result']) { // пробуем забрать
                $myCurl = curl_init();

                curl_setopt_array($myCurl, array(
                    CURLOPT_URL => API_URI.'SetApplicationStatus?key='.ACCESS_TOKEN.'&ApplicationID='.$application->inID.'&NewStatus=5',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_SSL_VERIFYHOST => 0,
                    CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
                ));

                curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
                    "Accept: application/json",
                    "Content-type: application/x-www-form-urlencoded"
                ));

                $response = curl_exec($myCurl);
                $info = curl_getinfo($myCurl);

                $rst = json_decode($response);
                if($rst[0]->Error == 0) { // Успех! забрали заявку
                    $check['reason'] = 'успешно забрали, можно работать.';
                    $check['reason_int'] = 1;
                    // сообщим про это в вайбер
                    $viber_str = "Подобрал новую заявку:\n";
                    $viber_str.="Номер заявки: ".$application->inID."\n";
                    $viber_str.="inGUID: ".$application->inGUID."\n";
                    $viber_str.="Тип: ".$application->inType."\n";
                    $viber_str.="Дата создания: ".$application->inStamp."\n";
                    $viber_str.="Возраст пациента: ".$application->inPersonAge."\n";
                    $viber_str.="Адрес: ".$application->inApplicationPlace."\n";
                    $viber_str.="Заявка актуальна на: ".$application->inApplicationDate."\n";
                    $viber_str.="Примечания: ".$application->inApplicationNotes."\n";
                    $viber_str.="Временные интервалы в заявке:\n";
                    $intervals = $application->inApplicationParams->intervals;
                    foreach($intervals as $interval) {
                        $viber_str .= $interval->DateOfTimeInterval.' '.$interval->description;
                    }
                    send_msg_to_viber($viber_str);
                } else {
                    $check['result'] = false;
                    $check['reason_int'] = -4;
                    $check['reason'] = 'пытались забрать заявку, но не получилось. Ответ сервера: '.$response;
                }
            }

            $report[] = $result_text = 'Обнаружена новая заявка #' . $application->inID . ' - ' . $check['reason'];
            $report[] = $answer;
            echo '<b>Application ' . ($check['result'] ? '<font color="green">Ok</font>' : '<font color="red">Ignored. ' . $check['reason'] . '</font>') . '</b><br>';

            $sql = 'insert into req_log set etime='.($check['result'] ? time() : 0).', stime=' . time() . ', operation="booking", inID="' . (int)$application->inID . '", 
                answer="' . $link->escape_string($answer) . '", result=' . $check['reason_int'] . ', result_text="' . $link->escape_string($result_text) . '"';

            $link->query($sql);
            //var_dump($application);
            echo '<p>';
        }
    }
} else {
//    $report[] = "Can't parse list of applications response as JSON Object!";
    exit_from_app($report);
}

$report = array_merge($report, close_active_app($link, $active_app));
exit_from_app($report);
