<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 1/10/19
 * Time: 12:13 PM
 */

function menu($type = 'home') {
    switch($type) {
        default:
            $buttons = [
                button_home(),
                button_about()
            ];
            break;
    }
    return $buttons;
}
////////////////////////////////////////////////////////////////
function button_about() {
    return (new \Viber\Api\Keyboard\Button())
        ->setBgColor('#421ab3')
        ->setColumns(3)
        ->setRows(1)
        ->setActionType('reply')
        ->setActionBody('info')
        ->setTextVAlign("middle")
        ->setTextHAlign("middle")
        ->setText('<font color="#ffffff">Обо мне</font>');
}
function button_home($label = 'К началу') {
    return (new \Viber\Api\Keyboard\Button())
        ->setBgColor('#421ab3')
        ->setColumns(3)
        ->setRows(1)
        ->setActionType('reply')
        ->setActionBody('home')
        ->setTextVAlign("middle")
        ->setTextHAlign("middle")
        ->setText('<font color="#ffffff">'.$label.'</font>');
}
////////////////////////////////////////////////////////////////
?>