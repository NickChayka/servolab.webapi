<?php
    ini_set('display_errors', 1);

    define('API_HOST', 'https://test.usi.net.ua/api/dsz.php');

    define('API_LOGIN', 'test');
    define('API_PWD', 'usitest');

    function connect($task, $data, $token) {
        $params = array(
            'task' => $task,
            'token' => $token,
            'data' => json_encode($data)
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, API_HOST);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $result = curl_exec($ch);
        if(curl_errno($ch) !== 0) {
            die('cURL error when connecting to ' . API_HOST . ': ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    $task = 'get_token';
    $data = array(
        'login' => API_LOGIN,
        'pass' => API_PWD
    );
    $auth = json_decode(connect($task, $data, ''));

    var_dump($auth);

//    $token = $auth->token;
//    $valid_until = $auth->valid_until;