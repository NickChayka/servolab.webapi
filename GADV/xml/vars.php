
[SERVICE_ID] => 100031

[CAMPAIGN_NAME] => city_21_service_100031

[BUDGET_TOTAL] => 50000000

[BUDGET_PER_DAY] => 2000000

[LOCATIONS] =>
<locationId>1012859</locationId>
<locationPath>Lviv, Lviv Oblast, Ukraine</locationPath>

[LABEL] => 5

[LANGUAGE] => ru

[KEYWORDS_LIST] =>
<keyword operation="add">
    <keyword-data>
        <labels/>
        <trackingUrl/>
        <finalUrlSuffix/>
        <finalUrls/>
        <finalMobileUrls/>
        <status>Enabled</status>
        <maxCpc>0</maxCpc>
        <maxCpm>0</maxCpm>
        <maxCpv>0</maxCpv>
        <type>Phrase</type>
        <text>[KEYWORD]</text>
        <customParameters/>
    </keyword-data>
</keyword>

[KEYWORD] => общий анализ крови цена

[PATHS] =>
<path1>Львов</path1>
<path2>Анализ_Крови</path2>

[FINAL_URL] => https://www.servolab.ua/ru/services/100031?city_id=21

[HEADLINES]
<headline1>Где сдать анализ крови общий?</headline1>
<headline1Pos>1</headline1Pos>
<headline2>5% скидка при онлайн заказе</headline2>
<headline2Pos>2</headline2Pos>

[DESCRIPTIONS]
<description1>5% скидка при заказе онлайн. Цены на общий анализ крови от 123 грн. Срок выполения 1 день.</description1>
<description1Pos>1</description1Pos>
<description2>Делаем анализы 7 дней в неделю. Служба поддержки работает 7 дней в неделю.</description2>
<description2Pos>2</description2Pos>


//////////////////////////////////////////////////////
для Киева такие данные показывает:

<geotargets>
    <geotarget id="1118969077876">
        <geotarget-data>
            <locationPath>(3km:50.431289:30.516869)</locationPath>
            <countryCode>UA</countryCode>
            <radius>3.0</radius>
            <unit>km</unit>
            <bidModifier/>
        </geotarget-data>
    </geotarget>
    <geotarget id="1030550">
        <geotarget-data>
            <locationId>1030550</locationId>
            <locationPath>Вишневое, Киевская область, Украина</locationPath>
            <countryCode/>
            <radius/>
            <unit/>
            <bidModifier/>
        </geotarget-data>
    </geotarget>
    <geotarget id="9041339">
        <geotarget-data>
            <locationId>9041339</locationId>
            <locationPath>Аэропорт Киев Борисполь, Киевская область, Украина</locationPath>
            <countryCode/>
            <radius/>
            <unit/>
            <bidModifier/>
        </geotarget-data>
    </geotarget>
    <geotarget id="9061011">
        <geotarget-data>
            <locationId>9061011</locationId>
            <locationPath>Голосеевский район, город Киев, Украина</locationPath>
            <countryCode/>
            <radius/>
            <unit/>
            <bidModifier>-40</bidModifier>
        </geotarget-data>
    </geotarget>
    <geotarget id="9061012">
        <geotarget-data>
            <locationId>9061012</locationId>
            <locationPath>Оболонский район, город Киев, Украина</locationPath>
            <countryCode/>
            <radius/>
            <unit/>
            <bidModifier/>
        </geotarget-data>
    </geotarget>
    <geotarget id="9061013">
        <geotarget-data>
            <locationId>9061013</locationId>
            <locationPath>Печерский район, город Киев, Украина</locationPath>
            <countryCode/>
            <radius/>
            <unit/>
            <bidModifier/>
        </geotarget-data>
    </geotarget>
    <geotarget id="9061014">
        <geotarget-data>
            <locationId>9061014</locationId>
            <locationPath>Подольский район, город Киев, Украина</locationPath>
            <countryCode/>
            <radius/>
            <unit/>
            <bidModifier/>
        </geotarget-data>
    </geotarget>
    <geotarget id="9061015">
        <geotarget-data>
            <locationId>9061015</locationId>
            <locationPath>Шевченковский район, город Киев, Украина</locationPath>
            <countryCode/>
            <radius/>
            <unit/>
            <bidModifier>10</bidModifier>
        </geotarget-data>
    </geotarget>
    <geotarget id="9061016">
        <geotarget-data>
            <locationId>9061016</locationId>
            <locationPath>Соломенский район, город Киев, Украина</locationPath>
            <countryCode/>
            <radius/>
            <unit/>
            <bidModifier/>
        </geotarget-data>
    </geotarget>
    <geotarget id="9061017">
        <geotarget-data>
            <locationId>9061017</locationId>
            <locationPath>Святошинский район, город Киев, Украина</locationPath>
            <countryCode/>
            <radius/>
            <unit/>
            <bidModifier/>
        </geotarget-data>
    </geotarget>
    <geotarget id="9061018">
        <geotarget-data>
            <locationId>9061018</locationId>
            <locationPath>Дарницкий район, город Киев, Украина</locationPath>
            <countryCode/>
            <radius/>
            <unit/>
            <bidModifier/>
        </geotarget-data>
    </geotarget>
    <geotarget id="9061019">
        <geotarget-data>
            <locationId>9061019</locationId>
            <locationPath>Деснянский район, город Киев, Украина</locationPath>
            <countryCode/>
            <radius/>
            <unit/>
            <bidModifier/>
        </geotarget-data>
    </geotarget>
    <geotarget id="9061020">
        <geotarget-data>
            <locationId>9061020</locationId>
            <locationPath>Днепровский район, город Киев, Украина</locationPath>
            <countryCode/>
            <radius/>
            <unit/>
            <bidModifier/>
        </geotarget-data>
    </geotarget>
</geotargets>