<?php
include_once 'cfg.php';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <title>Add Google ADS Campaign</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style>
        table {font: 12px Verdana, Tahoma, Geneva, Arial, Helvetica, sans-serif; color: #505050; text-decoration: none}
        .button {font: 12px Verdana, arial, helvetica, sans-serif; background-color: #384d0a; color: #ffffff; border-color: #384d0a}
    </style>
</head>
<body>
<table border="1" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <th height="25" width="50">ID</th>
        <th>Name</th>
        <th width="180">Created</th>
        <th width="120">Action</th>
    </tr>
    <?php
        $sql = 'select * from servolab_watchdog.ads_preset order by id';
        $rs = $link->query($sql);
        while($r = $rs->fetch_assoc()) {
            echo '<tr>';
            echo '<td height="25" align="center">'.$r['id'].'</td>';
            echo '<td>'.$r['name'].'</td>';
            echo '<td align="center">'.$r['created'].'</td>';
            echo '<td align="center">
                <a href="#" onclick="opener.load_preset('.$r['id'].'); window.close(); return false">select</a>
            </td>';
            echo '</tr>';
        }

    ?>
</table>
</body>
</html>
