<?php
include_once 'cfg.php';

if($_POST) {
    if(trim($_POST['template_name']) != '') { // давайте сохраним
        if((int)$_POST['template_id'] > 0) $sql = 'update';
        else $sql = 'insert into';
        $sql .= ' servolab_watchdog.ads_preset set created = "'.date('Y-m-d H:i:s').'",
            name = "'.$link->escape_string($_POST['template_name']).'",
            body = "'.$link->escape_string(serialize($_POST)).'"';
        if((int)$_POST['template_id'] > 0) $sql.=' where id='.(int)$_POST['template_id'];
        $link->query($sql);
    }
    header('Location: tpl_manager.php');
    exit;
} else {

    define('TPL_ID', (int)$_GET['id']);

    if(TPL_ID > 0) { // это редактирование шаблона, надо подгрузить данные
        $rs = $link->query('select * from servolab_watchdog.ads_preset where id='.TPL_ID);
        if($rs->num_rows == 1) {
            $r = $rs->fetch_assoc();
            $template_name = $r['name'];
            $data = unserialize($r['body']);
            $budget_daily = $data['budget_daily'];
            $max_cpc = $data['max_cpc'];
            $label = $data['label'];
            $language = $data['language'];
            $service_id = $data['service_id'];
            $keywords = $data['keywords'];
            for($i=1; $i<=15; $i++) {
                $var = 'headline'.$i;
                $$var = $data[$var];
                $var = 'headline'.$i.'Pos';
                $$var = $data[$var];
                $var = 'description'.$i;
                $$var = $data[$var];
                $var = 'description'.$i.'Pos';
                $$var = $data[$var];
            }
        } else {
            header('Location: tpl_manager.php');
            exit;
        }

    } else { // это новый шаблон
        $budget_daily = 500;
        $max_cpc = 200;
        $label = '';
        $language = 'ua';
        $service_id = '?';
        $keywords = '';
        for($i=1; $i<=15; $i++) {
            $var = 'headline'.$i;
            $$var = '';
            $var = 'headline'.$i.'Pos';
            $$var = '';
            $var = 'description'.$i;
            $$var = '';
            $var = 'description'.$i.'Pos';
            $$var = '';
        }
    }
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <title>Add/Edit Google ADS Template</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="application/javascript">
        function check_form(clone) {
            let o = document.getElementById('template_name');
            if(o.value.length > 0) {
                if(clone) document.forms[0].elements['template_id'].value = 0;
                document.forms[0].submit();
            } else {
                alert('Please, fill the Template Name field');
                o.focus();
            }
        }
    </script>
    <style>
        table {font: 11px Verdana, Tahoma, Geneva, Arial, Helvetica, sans-serif; color: #505050; text-decoration: none}
        .button {font: 12px Verdana, arial, helvetica, sans-serif; background-color: #384d0a; color: #ffffff; border-color: #384d0a}
    </style>
</head>
<body>
<table border="0" bgcolor="#7fffd4">
    <tr>
        <td height="30"><button onclick="document.location.href='tpl_manager.php'" class="button">Back</button></td>
        <?php if(TPL_ID > 0) { ?>
            <td>&nbsp;<button onclick="if(confirm('Are you sure?')) document.location.href='tpl_manager.php?action=delete&id=<?php echo TPL_ID?>'" class="button">Delete Template</button></td>
        <?php } ?>
    </tr>
</table><p>
<form method="POST" onsubmit="return false">
    <input type="hidden" name="template_id" value="<?php echo TPL_ID?>">
        <table border="0" cellpadding="2" cellspacing="0">
        <tr>
            <td align="right">TEMPLATE NAME</td>
            <td align="left"><input type="text" name="template_name" id="template_name" value="<?php echo $template_name?>"></td>
            <td align="right">LANGUAGE</td>
            <td align="left">
                <select name="language" id="language">
                    <option value="ua" <?php if($language == 'ua') echo 'selected'?>>ua</option>
                    <option value="ru" <?php if($language == 'ru') echo 'selected'?>>ru</option>
                </select>
            </td>
            <td align="right">SERVICE_ID</td>
            <td align="left"><input type="text" name="service_id" id="service_id" value="<?php echo $service_id?>"></td>

        </tr>
        <tr>
            <td align="right">LABEL</td>
            <td align="left"><input type="text" name="label" id="label" value="<?php echo $label?>"></td>
            <td align="right">BUDGET_DAILY (в коп)</td>
            <td align="left"><input type="text" name="budget_daily" id="budget_daily" value="<?php echo $budget_daily?>"></td>
            <td align="right">MAX_CPC (в коп)</td>
            <td align="left"><input type="text" name="max_cpc" id="max_cpc" value="<?php echo $max_cpc?>"></td>
        </tr>
    </table>
    <table border="0" bgcolor="#ffadf0" cellpadding="2" cellspacing="0">
        <tr>
            <td bgcolor="yellow" height="30">Список допустимых переменных: [CITY], [MIN_PRICE], [SERVICE_ID], [CAMPAIGN_NAME], [BUDGET_DAILY], [MAX_CPC], [LABEL], [LANGUAGE], [FINAL_URL]</td>
        </tr>
        <tr>
            <td align="left">KEYWORD's</td>
        </tr>
        <tr>
            <td><textarea cols="140" rows="15" name="keywords" id="keywords"><?php echo $keywords?></textarea></td>
        </tr>
    </table>
    <table border="0" cellpadding="2" cellspacing="0">
        <tr>
            <td colspan="2" align="left">HEADLINES</td>
            <td colspan="2" align="left">DESCRIPTIONS</td>
        </tr>
        <tr bgcolor="aqua">
            <td><input type="text" size="10" name="headline1Pos" id="headline1Pos" value="<?php echo $headline1Pos?>"></td>
            <td><input type="text" size="30" name="headline1" id="headline1" value="<?php echo $headline1?>"></td>
            <td><input type="text" size="10" name="description1Pos" id="description1Pos" value="<?php echo $description1Pos?>"></td>
            <td><input type="text" size="90" name="description1" id="description1" value="<?php echo $description1?>"></td>
        </tr>
        <tr>
            <td><input type="text" size="10" name="headline2Pos" id="headline2Pos" value="<?php echo $headline2Pos?>"></td>
            <td><input type="text" size="30" name="headline2" id="headline2" value="<?php echo $headline2?>"></td>
            <td><input type="text" size="10" name="description2Pos" id="description2Pos" value="<?php echo $description2Pos?>"></td>
            <td><input type="text" size="90" name="description2" id="description2" value="<?php echo $description2?>"></td>
        </tr>
        <tr  bgcolor="aqua">
            <td><input type="text" size="10" name="headline3Pos" id="headline3Pos" value="<?php echo $headline3Pos?>"></td>
            <td><input type="text" size="30" name="headline3" id="headline3" value="<?php echo $headline3?>"></td>
            <td><input type="text" size="10" name="description3Pos" id="description3Pos" value="<?php echo $description3Pos?>"></td>
            <td><input type="text" size="90" name="description3" id="description3" value="<?php echo $description3?>"></td>
        </tr>
        <tr>
            <td><input type="text" size="10" name="headline4Pos" id="headline4Pos" value="<?php echo $headline4Pos?>"></td>
            <td><input type="text" size="30" name="headline4" id="headline4" value="<?php echo $headline4?>"></td>
            <td><input type="text" size="10" name="description4Pos" id="description4Pos" value="<?php echo $description4Pos?>"></td>
            <td><input type="text" size="90" name="description4" id="description4" value="<?php echo $description4?>"></td>
        </tr>
        <tr  bgcolor="aqua">
            <td><input type="text" size="10" name="headline5Pos" id="headline5Pos" value="<?php echo $headline5Pos?>"></td>
            <td><input type="text" size="30" name="headline5" id="headline5" value="<?php echo $headline5?>"></td>
            <td><input type="text" size="10" name="description5Pos" id="description5Pos" value="<?php echo $description5Pos?>"></td>
            <td><input type="text" size="90" name="description5" id="description5" value="<?php echo $description5?>"></td>
        </tr>
        <tr>
            <td><input type="text" size="10" name="headline6Pos" id="headline6Pos" value="<?php echo $headline6Pos?>"></td>
            <td><input type="text" size="30" name="headline6" id="headline6" value="<?php echo $headline6?>"></td>
            <td><input type="text" size="10" name="description6Pos" id="description6Pos" value="<?php echo $description6Pos?>"></td>
            <td><input type="text" size="90" name="description6" id="description6" value="<?php echo $description6?>"></td>
        </tr>
        <tr  bgcolor="aqua">
            <td><input type="text" size="10" name="headline7Pos" id="headline7Pos" value="<?php echo $headline7Pos?>"></td>
            <td><input type="text" size="30" name="headline7" id="headline7" value="<?php echo $headline7?>"></td>
            <td><input type="text" size="10" name="description7Pos" id="description7Pos" value="<?php echo $description7Pos?>"></td>
            <td><input type="text" size="90" name="description7" id="description7" value="<?php echo $description7?>"></td>
        </tr>
        <tr>
            <td><input type="text" size="10" name="headline8Pos" id="headline8Pos" value="<?php echo $headline8Pos?>"></td>
            <td><input type="text" size="30" name="headline8" id="headline8" value="<?php echo $headline8?>"></td>
            <td><input type="text" size="10" name="description8Pos" id="description8Pos" value="<?php echo $description8Pos?>"></td>
            <td><input type="text" size="90" name="description8" id="description8" value="<?php echo $description8?>"></td>
        </tr>
        <tr  bgcolor="aqua">
            <td><input type="text" size="10" name="headline9Pos" id="headline9Pos" value="<?php echo $headline9Pos?>"></td>
            <td><input type="text" size="30" name="headline9" id="headline9" value="<?php echo $headline9?>"></td>
            <td><input type="text" size="10" name="description9Pos" id="description9Pos" value="<?php echo $description9Pos?>"></td>
            <td><input type="text" size="90" name="description9" id="description9" value="<?php echo $description9?>"></td>
        </tr>
        <tr>
            <td><input type="text" size="10" name="headline10Pos" id="headline10Pos" value="<?php echo $headline10Pos?>"></td>
            <td><input type="text" size="30" name="headline10" id="headline10" value="<?php echo $headline10?>"></td>
            <td><input type="text" size="10" name="description10Pos" id="description10Pos" value="<?php echo $description10Pos?>"></td>
            <td><input type="text" size="90" name="description10" id="description10" value="<?php echo $description10?>"></td>
        </tr>
        <tr  bgcolor="aqua">
            <td><input type="text" size="10" name="headline11Pos" id="headline11Pos" value="<?php echo $headline11Pos?>"></td>
            <td><input type="text" size="30" name="headline11" id="headline11" value="<?php echo $headline11?>"></td>
            <td><input type="text" size="10" name="description11Pos" id="description11Pos" value="<?php echo $description11Pos?>"></td>
            <td><input type="text" size="90" name="description11" id="description11" value="<?php echo $description11?>"></td>
        </tr>
        <tr>
            <td><input type="text" size="10" name="headline12Pos" id="headline12Pos" value="<?php echo $headline12Pos?>"></td>
            <td><input type="text" size="30" name="headline12" id="headline12" value="<?php echo $headline12?>"></td>
            <td><input type="text" size="10" name="description12Pos" id="description12Pos" value="<?php echo $description12Pos?>"></td>
            <td><input type="text" size="90" name="description12" id="description12" value="<?php echo $description12?>"></td>
        </tr>
        <tr  bgcolor="aqua">
            <td><input type="text" size="10" name="headline13Pos" id="headline13Pos" value="<?php echo $headline13Pos?>"></td>
            <td><input type="text" size="30" name="headline13" id="headline13" value="<?php echo $headline13?>"></td>
            <td><input type="text" size="10" name="description13Pos" id="description13Pos" value="<?php echo $description13Pos?>"></td>
            <td><input type="text" size="90" name="description13" id="description13" value="<?php echo $description13?>"></td>
        </tr>
        <tr>
            <td><input type="text" size="10" name="headline14Pos" id="headline14Pos" value="<?php echo $headline14Pos?>"></td>
            <td><input type="text" size="30" name="headline14" id="headline14" value="<?php echo $headline14?>"></td>
            <td><input type="text" size="10" name="description14Pos" id="description14Pos" value="<?php echo $description14Pos?>"></td>
            <td><input type="text" size="90" name="description14" id="description14" value="<?php echo $description14?>"></td>
        </tr>
        <tr  bgcolor="aqua">
            <td><input type="text" size="10" name="headline15Pos" id="headline15Pos" value="<?php echo $headline15Pos?>"></td>
            <td><input type="text" size="30" name="headline15" id="headline15" value="<?php echo $headline15?>"></td>
            <td><input type="text" size="10" name="description15Pos" id="description15Pos" value="<?php echo $description15Pos?>"></td>
            <td><input type="text" size="90" name="description15" id="description15" value="<?php echo $description15?>"></td>
        </tr>
    </table>

    <input type="submit" value="Save" class="button" onclick="check_form(false); return false">
    <input type="submit" value="Save as New" class="button" onclick="check_form(true); return false">
    <input type="reset" value="Reset" class="button">
</form>
</body>
</html>