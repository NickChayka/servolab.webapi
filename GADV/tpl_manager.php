<?php
include_once 'cfg.php';

if((int)$_GET['id'] > 0 and $_GET['action'] == 'delete') {
    $link->query('delete from servolab_watchdog.ads_preset where id='.(int)$_GET['id']);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ADS Template Manager</title>
    <style>
        table {font: 11px Verdana, Tahoma, Geneva, Arial, Helvetica, sans-serif; color: #505050; text-decoration: none}
        .button {font: 12px Verdana, arial, helvetica, sans-serif; background-color: #384d0a; color: #ffffff; border-color: #384d0a}
    </style>
</head>
<body>
<table border="0" bgcolor="#7fffd4">
    <tr>
        <td height="30"><button onclick="document.location.href='index.php'" class="button">Back</button></td>
        <td>&nbsp;<button onclick="document.location.href='edit_template.php'" class="button">Add a new Template</button></td>
    </tr>
</table><p>
<table border="1" cellpadding="0" cellspacing="0" width="800">
    <tr>
        <th height="25" width="50">ID</th>
        <th>Name</th>
        <th width="180">Created</th>
        <th width="120">Action</th>
    </tr>
    <?php
        $sql = 'select * from servolab_watchdog.ads_preset order by id';
        $rs = $link->query($sql);
        while($r = $rs->fetch_assoc()) {
        echo '<tr>';
        echo '<td height="25" align="center">'.$r['id'].'</td>';
        echo '<td>'.$r['name'].'</td>';
        echo '<td align="center">'.$r['created'].'</td>';
        echo '<td align="center">
            <a href="edit_template.php?id='.$r['id'].'">edit</a>&nbsp;
            <a href="tpl_manager.php?action=delete&id='.$r['id'].'" onclick="return confirm(\'Are you sure?\')">delete</a>
        </td>';
        echo '</tr>';
    }

    ?>
</table>

</body>
</html>