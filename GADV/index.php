<?php
include_once 'cfg.php';

$City = [];
$sql = 'SELECT id, city_name city_name_ua, city_name_ru, region_name region_name_ua, region_name_ru, gps_lat, gps_long 
        FROM servolab_views.city order by region_name, city_name';
$rs = $link->query($sql);
$city = 1;
while($r = $rs->fetch_assoc()) {
    $City[$r['id']] = [
        'ru' => [
            'city_name' => $r['city_name_ru'],
            'region_name' => $r['region_name_ru']
        ],
        'ua' => [
            'city_name' => $r['city_name_ua'],
            'region_name' => $r['region_name_ua']
        ],
        'lat' => $r['gps_lat'],
        'long' => $r['gps_long']
    ];
}
$rs->close();

$PATH = [
    'ru' => 'Анализы',
    'ua' => 'Аналізи'
];

if($_POST) {
    $language = $_POST['language'];
    $service_id = $_POST['service_id'];
    $label = $_POST['label'];
    $budget_daily = $_POST['budget_daily'];
    $max_cpc = $_POST['max_cpc'];
    $keywords = $_POST['keywords'];
    for($i=1; $i<=15; $i++) {
        $var = 'headline'.$i;
        $$var = $_POST[$var];
        $var = 'headline'.$i.'Pos';
        $$var = $_POST[$var];
        $var = 'description'.$i;
        $$var = $_POST[$var];
        $var = 'description'.$i.'Pos';
        $$var = $_POST[$var];
    }

    $xml = file_get_contents('xml/header.xml');
    $selected_city_str = trim($_POST['selected_city']);
    $selected_city = explode(',', $selected_city_str);
    foreach($selected_city as $city_id) { // и теперь для каждого города строим хмл

        if((int)$city_id > 0) {
            $tpl = file_get_contents('xml/data.xml');

            $radius = (int)$_POST['radius_'.$city_id];
            $city_min_price = $_POST['min_price_'.$city_id];
            $campaign_name = $_POST['camp_name_'.$city_id];
            $final_url = $_POST['url_'.$city_id];
            if ($radius <= 0) $radius = 10;
            $location = '(' . $radius . 'km:' . $City[$city_id]['lat'] . ':' . $City[$city_id]['long'] . ')';
            $city_name = $City[$city_id][$language]['city_name'];

            $tmp  = "<path1>" . $PATH[$language] . "</path1>\n";
            $tmp .= "<path2>" . $city_name . "</path2>\n";

            $tpl = str_replace('[PATHS]', $tmp, $tpl);

            $ktpl = file_get_contents('xml/keyword.xml');
            $data = explode("\n", $keywords);
            $tmp = '';
            for ($i = 0; $i < count($data); $i++) {
                if (trim($data[$i]) != '')
                    $tmp .= str_replace('[KEYWORD]', trim($data[$i]), $ktpl) . "\n";
            }
            $tpl = str_replace('[KEYWORDS_LIST]', $tmp, $tpl);

            $tmp = "<locationPath>".$location."</locationPath>
              <countryCode>UA</countryCode>
              <radius>" . $radius . ".0</radius>
              <unit>km</unit>
              <bidModifier/>";
            $tpl = str_replace('[LOCATIONS]', $tmp, $tpl);

            $c = 1;
            $tmp = '';
            for ($i = 1; $i <= 15; $i++) {
                $var = 'headline' . $i;
                $pos = 'headline' . $i . 'Pos';
                if (trim($$var) != '') {
                    $tmp .= "<headline" . $c . ">" . $$var . "</headline" . $c . ">\n";
                    if ($$pos == '') $$pos = '-';
                    $tmp .= "<headline" . $c . "Pos>" . $$pos . "</headline" . $c . "Pos>\n";
                    $c++;
                }
            }
            $tpl = str_replace('[HEADLINES]', $tmp, $tpl);

            $c = 1;
            $tmp = '';
            for ($i = 1; $i <= 15; $i++) {
                $var = 'description' . $i;
                $pos = 'description' . $i . 'Pos';
                if (trim($$var) != '') {
                    $tmp .= "<description" . $c . ">" . $$var . "</description" . $c . ">\n";
                    if ($$pos == '') $$pos = '-';
                    $tmp .= "<description" . $c . "Pos>" . $$pos . "</description" . $c . "Pos>\n";
                    $c++;
                }
            }
            $tpl = str_replace('[DESCRIPTIONS]', $tmp, $tpl);

            $tpl = str_replace('[SERVICE_ID]', $service_id, $tpl);
            $tpl = str_replace('[CAMPAIGN_NAME]', $campaign_name, $tpl);
            $tpl = str_replace('[BUDGET_DAILY]', $budget_daily*10000, $tpl);
            $tpl = str_replace('[MAX_CPC]', $max_cpc*10000, $tpl);
            $tpl = str_replace('[LABEL]', $label, $tpl);
            $tpl = str_replace('[LANGUAGE]', $language, $tpl);
            $tpl = str_replace('[FINAL_URL]', $final_url, $tpl);

            $tpl = str_replace('[CITY]', $city_name, $tpl);
            $tpl = str_replace('[MIN_PRICE]', $city_min_price, $tpl);

            $xml .= $tpl;
        }
    }

    // всёёёёё... теперт надо выдать это как xml
    $xml .= file_get_contents('xml/footer.xml');
    header('Content-Type: application/octetstream');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    if(PMA_USR_BROWSER_AGENT == 'IE') {
        header('Content-Disposition: inline; filename="camp_'.$service_id.'_'.$language.'.xml"');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
    } else {
        header('Content-Disposition: attachment; filename="camp_'.$service_id.'_'.$language.'.xml"');
        header('Pragma: no-cache');
    }
    echo $xml;
    exit;

} else {
    $budget_daily = 500;
    $max_cpc = 200;
    $label = '';
    $language = 'ua';
    $service_id = '?';
    $keywords = '';
    for($i=1; $i<=15; $i++) {
        $var = 'headline'.$i;
        $$var = '';
        $var = 'headline'.$i.'Pos';
        $$var = '';
        $var = 'description'.$i;
        $$var = '';
        $var = 'description'.$i.'Pos';
        $$var = '';
    }
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <title>Add Google ADS Campaign</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script language="JavaScript" src="ajax/JsHttpRequest.js"></script>
    <script language="JavaScript" src="ajax/ajax_func.js"></script>
    <script type="application/javascript">
        let City = new Array;
        let CitySelected = new Array;
        <?php
        foreach($City as $id => $data) {
            echo "City[$id] = new Array;\n";
            echo "City[$id]['ru'] = new Array;\n";
            echo "City[$id]['ru']['city_name'] = '".$data['ru']['city_name']."';\n";
            echo "City[$id]['ru']['region_name'] = '".$data['ru']['region_name']."';\n";
            echo "City[$id]['ua'] = new Array;\n";
            echo "City[$id]['ua']['city_name'] = '".$data['ua']['city_name']."';\n";
            echo "City[$id]['ua']['region_name'] = '".$data['ua']['region_name']."';\n";
        }
        ?>

        Array.prototype.in_array = function(p_val) {
            for(var i = 0, l = this.length; i < l; i++)  {
                if(this[i] == p_val) return true;
            }
            return false;
        }

        function make_final_url() {
            let o = document.getElementById('language');
            let lang='?';
            for(let i=0; i<o.length; i++) {
                if(o[i].selected) {
                    lang = o[i].value;
                    break;
                }
            }
            o = document.getElementById('service_id');
            let s = '?';
            if(o.value.length > 0) {
                s = o.value;
            }
            for(let i=0; i<CitySelected.length; i++) {
                o = document.getElementById('url_'+CitySelected[i]);
                if(o) o.value = "https://www.servolab.ua/"+lang+"/services/"+s+"?city_id="+CitySelected[i];
            }
        }
        function make_camp_name() {
            let o = document.getElementById('language');
            let lang='?';
            for(let i=0; i<o.length; i++) {
                if(o[i].selected) {
                    lang = o[i].value;
                    break;
                }
            }
            o = document.getElementById('service_id');
            let s = '?';
            if(o.value.length > 0) {
                s = o.value;
            }
            for(let i=0; i<CitySelected.length; i++) {
                o = document.getElementById('camp_name_'+CitySelected[i]);
                if(o) o.value = "<?php echo CAMP_PREFIX?>city_"+CitySelected[i]+"_service_"+s+"_"+lang;
            }
        }
        function get_min_price() {
            for(let i=0; i<CitySelected.length; i++) {
                o = document.getElementById('min_price_'+CitySelected[i]);
                if(o) ajax_get_min_price(CitySelected[i]);
            }
        }
        function add_one_city() {
            let o = document.getElementById('new_city');
            let city_id = 0;
            for(let i=0; i<o.length; i++) {
                if(o[i].selected) {
                    city_id=o[i].value;
                    break;
                }
            }
            add_city_row(city_id);
            make_final_url();
            make_camp_name();
            get_min_price();
        }
        function add_all_city() {
            let o = document.getElementById('new_city');
            for(let i=0; i<o.length; i++) {
                if(!CitySelected.in_array(o[i].value))
                    add_city_row(o[i].value);
            }
            make_final_url();
            make_camp_name();
            get_min_price();
        }

        function add_city_row(city_id) {
            if(city_id > 0 && !CitySelected.in_array(city_id)) { // есть город, добавим его
                let tbodyRef = document.getElementById('city_list').getElementsByTagName('tbody')[0];
                let newRow = tbodyRef.insertRow();
                newRow.id = 'row_' + city_id;
                // city name
                let cell = newRow.insertCell();
                let f1 = document.createElement('input');
                f1.id = "city_" + city_id;
                f1.name = "city_" + city_id;
                f1.value = City[city_id]['ru']['city_name'];
                cell.appendChild(f1);
                // city radius
                cell = newRow.insertCell();
                let f2 = document.createElement('input');
                f2.id = "radius_" + city_id;
                f2.name = "radius_" + city_id;
                f2.size = 3;
                f2.value = document.getElementById('new_radius').value;
                cell.appendChild(f2);
                // min_price
                cell = newRow.insertCell();
                let f3 = document.createElement('input');
                f3.id = "min_price_" + city_id;
                f3.name = "min_price_" + city_id;
                f3.value = '?';
                cell.appendChild(f3);
                // campaign name
                cell = newRow.insertCell();
                let f4 = document.createElement('input');
                f4.id = "camp_name_" + city_id;
                f4.name = "camp_name_" + city_id;
                f4.value = '?';
                f4.size=30;
                cell.appendChild(f4);
                // final url
                cell = newRow.insertCell();
                let f5 = document.createElement('input');
                f5.id = "url_" + city_id;
                f5.name = "url_" + city_id;
                f5.value = '?';
                f5.size=50;
                cell.appendChild(f5);
                // remove btn
                cell = newRow.insertCell();
                cell.style.textAlign = "right";
                cell.innerHTML = '<input type="button" value="Remove" onClick="remove_city_row(' + city_id + '); return false"/>';
                // апнем список городов
                CitySelected.push(city_id);
            }
        }
        function remove_city_row(city_id) {
            let tbodyRef = document.getElementById('city_list').getElementsByTagName('tbody')[0];
            let row = document.getElementById('row_'+city_id);
            tbodyRef.removeChild(row);
            let tmp = new Array;
            let tmp_val;
            for(let i=0; i<CitySelected.length; i++) {
                tmp_val = CitySelected[i];
                if(tmp_val != city_id) tmp.push(tmp_val);
            }
            CitySelected = tmp;
            make_final_url();
            make_camp_name();
            get_min_price();
        }
        function set_selected_field() {
            let o = document.getElementById('selected_city');
            let s = "";
            for(let i=0; i<CitySelected.length; i++) {
                s += CitySelected[i]+",";
            }
            o.value = s;
            document.forms[0].submit();
        }
        function open_template_selector() {
            window.open("find_template.php", "template_manager", "width=800,height=400");
        }
        function load_preset(id) {
            clear_form();
            ajax_load_preset(id);
        }
        function clear_form() {
            let o = document.getElementById('service_id');
            if(o) o.value = '';
            o = document.getElementById('label');
            if(o) o.value = '';
            o = document.getElementById('budget_daily');
            if(o) o.value = '';
            o = document.getElementById('max_cpc');
            if(o) o.value = '';
            o = document.getElementById('preset_name_tmp');
            if(o) o.value = '';
            o = document.getElementById('keywords');
            if(o) o.value = '';
            for(let i=1; i<=15; i++) {
                o = document.getElementById('headline' + i);
                if(o) o.value = '';
                o = document.getElementById('headline' + i + 'Pos');
                if(o) o.value = '';
                o = document.getElementById('description' + i);
                if(o) o.value = '';
                o = document.getElementById('description' + i + 'Pos');
                if(o) o.value = '';
            }
            // и города
            for(let i=0; i<CitySelected.length; i++) {
                remove_city_row(CitySelected[i]);
            }
        }
    </script>
    <style>
        table {font: 11px Verdana, Tahoma, Geneva, Arial, Helvetica, sans-serif; color: #505050; text-decoration: none}
        .button {font: 12px Verdana, arial, helvetica, sans-serif; background-color: #384d0a; color: #ffffff; border-color: #384d0a}
    </style>
</head>
<body>
<table border="0" bgcolor="#7fffd4">
    <tr>
        <td height="30">Template:</td>
        <td id="template_name" style="font-weight: bold">&nbsp;</td>
        <td>&nbsp;<button onclick="open_template_selector();" class="button">Load from template</button></td>
        <td>&nbsp;<button onclick="document.location.href='tpl_manager.php'" class="button">Templates manager</button></td>
    </tr>
</table><p>
<form method="POST" onsubmit="return false">
    <input type="hidden" name="preset_name" id="preset_name">
        <table border="0" cellpadding="2" cellspacing="0">
        <tr>
            <td align="right">LANGUAGE</td>
            <td align="left">
                <select name="language" id="language" onchange="make_final_url()">
                    <option value="ua" <?php if($language == 'ua') echo 'selected'?>>ua</option>
                    <option value="ru" <?php if($language == 'ru') echo 'selected'?>>ru</option>
                </select>
            </td>
            <td align="right">SERVICE_ID</td>
            <td align="left"><input type="text" name="service_id" id="service_id" value="<?php echo $service_id?>" onkeyup="make_camp_name(); make_final_url(); get_min_price();" onchange="make_camp_name(); make_final_url();"></td>
            <td align="right">LABEL</td>
            <td align="left"><input type="text" name="label" id="label" value="<?php echo $label?>"></td>
        </tr>
        <tr>
            <td align="right">BUDGET_DAILY (в коп)</td>
            <td align="left"><input type="text" name="budget_daily" id="budget_daily" value="<?php echo $budget_daily?>"></td>
            <td align="right">MAX_CPC (в коп)</td>
            <td align="left"><input type="text" name="max_cpc" id="max_cpc" value="<?php echo $max_cpc?>"></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <input type="hidden" name="selected_city" id="selected_city">
    <fieldset style="background-color: #7fffd4">
        <legend>City</legend>
        <table border="1" cellpadding="2" cellspacing="0" id="city_list">
            <tbody>
            <tr>
                <td>
                    <select name="new_city" id="new_city">
                        <?php
                        foreach($City as $id=>$data) {
                            echo '<option value="'.$id.'">'.$data['ru']['region_name'].', '.$data['ru']['city_name'].'</option>';
                        }
                        ?>
                    </select>
                </td>
                <td><input type="text" name="new_radius" id="new_radius" value="10" size="3"></td>
                <td colspan="4" align="right">
                    <input type="button" value="Add All" onclick="add_all_city()">&nbsp;&nbsp;
                    <input type="button" value="Add One Selected" onclick="add_one_city()">
                </td>
            </tr>
            <tr>
                <th>CITY</th>
                <th>RADIUS</th>
                <th>MIN_PRICE</th>
                <th>CAMPAIGN_NAME</th>
                <th>FINAL_URL</th>
                <th>Action</th>
            </tr>
            </tbody>
        </table>
    </fieldset>

    <table border="0" bgcolor="#ffadf0" cellpadding="2" cellspacing="0">
        <tr>
            <td align="left">KEYWORD's</td>
        </tr>
        <tr>
            <td><textarea cols="150" rows="15" name="keywords" id="keywords"><?php echo $keywords?></textarea></td>
        </tr>
    </table>
    <table border="0" cellpadding="2" cellspacing="0">
        <tr>
            <td colspan="2" align="left">HEADLINES</td>
            <td colspan="2" align="left">DESCRIPTIONS</td>
        </tr>
        <tr bgcolor="aqua">
            <td><input type="text" size="10" name="headline1Pos" id="headline1Pos" value="<?php echo $headline1Pos?>"></td>
            <td><input type="text" size="30" name="headline1" id="headline1" value="<?php echo $headline1?>"></td>
            <td><input type="text" size="10" name="description1Pos" id="description1Pos" value="<?php echo $description1Pos?>"></td>
            <td><input type="text" size="90" name="description1" id="description1" value="<?php echo $description1?>"></td>
        </tr>
        <tr>
            <td><input type="text" size="10" name="headline2Pos" id="headline2Pos" value="<?php echo $headline2Pos?>"></td>
            <td><input type="text" size="30" name="headline2" id="headline2" value="<?php echo $headline2?>"></td>
            <td><input type="text" size="10" name="description2Pos" id="description2Pos" value="<?php echo $description2Pos?>"></td>
            <td><input type="text" size="90" name="description2" id="description2" value="<?php echo $description2?>"></td>
        </tr>
        <tr  bgcolor="aqua">
            <td><input type="text" size="10" name="headline3Pos" id="headline3Pos" value="<?php echo $headline3Pos?>"></td>
            <td><input type="text" size="30" name="headline3" id="headline3" value="<?php echo $headline3?>"></td>
            <td><input type="text" size="10" name="description3Pos" id="description3Pos" value="<?php echo $description3Pos?>"></td>
            <td><input type="text" size="90" name="description3" id="description3" value="<?php echo $description3?>"></td>
        </tr>
        <tr>
            <td><input type="text" size="10" name="headline4Pos" id="headline4Pos" value="<?php echo $headline4Pos?>"></td>
            <td><input type="text" size="30" name="headline4" id="headline4" value="<?php echo $headline4?>"></td>
            <td><input type="text" size="10" name="description4Pos" id="description4Pos" value="<?php echo $description4Pos?>"></td>
            <td><input type="text" size="90" name="description4" id="description4" value="<?php echo $description4?>"></td>
        </tr>
        <tr  bgcolor="aqua">
            <td><input type="text" size="10" name="headline5Pos" id="headline5Pos" value="<?php echo $headline5Pos?>"></td>
            <td><input type="text" size="30" name="headline5" id="headline5" value="<?php echo $headline5?>"></td>
            <td><input type="text" size="10" name="description5Pos" id="description5Pos" value="<?php echo $description5Pos?>"></td>
            <td><input type="text" size="90" name="description5" id="description5" value="<?php echo $description5?>"></td>
        </tr>
        <tr>
            <td><input type="text" size="10" name="headline6Pos" id="headline6Pos" value="<?php echo $headline6Pos?>"></td>
            <td><input type="text" size="30" name="headline6" id="headline6" value="<?php echo $headline6?>"></td>
            <td><input type="text" size="10" name="description6Pos" id="description6Pos" value="<?php echo $description6Pos?>"></td>
            <td><input type="text" size="90" name="description6" id="description6" value="<?php echo $description6?>"></td>
        </tr>
        <tr  bgcolor="aqua">
            <td><input type="text" size="10" name="headline7Pos" id="headline7Pos" value="<?php echo $headline7Pos?>"></td>
            <td><input type="text" size="30" name="headline7" id="headline7" value="<?php echo $headline7?>"></td>
            <td><input type="text" size="10" name="description7Pos" id="description7Pos" value="<?php echo $description7Pos?>"></td>
            <td><input type="text" size="90" name="description7" id="description7" value="<?php echo $description7?>"></td>
        </tr>
        <tr>
            <td><input type="text" size="10" name="headline8Pos" id="headline8Pos" value="<?php echo $headline8Pos?>"></td>
            <td><input type="text" size="30" name="headline8" id="headline8" value="<?php echo $headline8?>"></td>
            <td><input type="text" size="10" name="description8Pos" id="description8Pos" value="<?php echo $description8Pos?>"></td>
            <td><input type="text" size="90" name="description8" id="description8" value="<?php echo $description8?>"></td>
        </tr>
        <tr  bgcolor="aqua">
            <td><input type="text" size="10" name="headline9Pos" id="headline9Pos" value="<?php echo $headline9Pos?>"></td>
            <td><input type="text" size="30" name="headline9" id="headline9" value="<?php echo $headline9?>"></td>
            <td><input type="text" size="10" name="description9Pos" id="description9Pos" value="<?php echo $description9Pos?>"></td>
            <td><input type="text" size="90" name="description9" id="description9" value="<?php echo $description9?>"></td>
        </tr>
        <tr>
            <td><input type="text" size="10" name="headline10Pos" id="headline10Pos" value="<?php echo $headline10Pos?>"></td>
            <td><input type="text" size="30" name="headline10" id="headline10" value="<?php echo $headline10?>"></td>
            <td><input type="text" size="10" name="description10Pos" id="description10Pos" value="<?php echo $description10Pos?>"></td>
            <td><input type="text" size="90" name="description10" id="description10" value="<?php echo $description10?>"></td>
        </tr>
        <tr  bgcolor="aqua">
            <td><input type="text" size="10" name="headline11Pos" id="headline11Pos" value="<?php echo $headline11Pos?>"></td>
            <td><input type="text" size="30" name="headline11" id="headline11" value="<?php echo $headline11?>"></td>
            <td><input type="text" size="10" name="description11Pos" id="description11Pos" value="<?php echo $description11Pos?>"></td>
            <td><input type="text" size="90" name="description11" id="description11" value="<?php echo $description11?>"></td>
        </tr>
        <tr>
            <td><input type="text" size="10" name="headline12Pos" id="headline12Pos" value="<?php echo $headline12Pos?>"></td>
            <td><input type="text" size="30" name="headline12" id="headline12" value="<?php echo $headline12?>"></td>
            <td><input type="text" size="10" name="description12Pos" id="description12Pos" value="<?php echo $description12Pos?>"></td>
            <td><input type="text" size="90" name="description12" id="description12" value="<?php echo $description12?>"></td>
        </tr>
        <tr  bgcolor="aqua">
            <td><input type="text" size="10" name="headline13Pos" id="headline13Pos" value="<?php echo $headline13Pos?>"></td>
            <td><input type="text" size="30" name="headline13" id="headline13" value="<?php echo $headline13?>"></td>
            <td><input type="text" size="10" name="description13Pos" id="description13Pos" value="<?php echo $description13Pos?>"></td>
            <td><input type="text" size="90" name="description13" id="description13" value="<?php echo $description13?>"></td>
        </tr>
        <tr>
            <td><input type="text" size="10" name="headline14Pos" id="headline14Pos" value="<?php echo $headline14Pos?>"></td>
            <td><input type="text" size="30" name="headline14" id="headline14" value="<?php echo $headline14?>"></td>
            <td><input type="text" size="10" name="description14Pos" id="description14Pos" value="<?php echo $description14Pos?>"></td>
            <td><input type="text" size="90" name="description14" id="description14" value="<?php echo $description14?>"></td>
        </tr>
        <tr  bgcolor="aqua">
            <td><input type="text" size="10" name="headline15Pos" id="headline15Pos" value="<?php echo $headline15Pos?>"></td>
            <td><input type="text" size="30" name="headline15" id="headline15" value="<?php echo $headline15?>"></td>
            <td><input type="text" size="10" name="description15Pos" id="description15Pos" value="<?php echo $description15Pos?>"></td>
            <td><input type="text" size="90" name="description15" id="description15" value="<?php echo $description15?>"></td>
        </tr>
    </table>

    <input type="submit" value="Generate" class="button" onclick="set_selected_field(false); return false">
    <input type="reset" value="Reset" class="button">
</form>
</body>
</html>