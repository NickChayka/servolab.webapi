function ajax_get_min_price(city_id) {
	let o = document.getElementById('service_id');
	let service = o.value;

	if(service.length >= 6) {

        let req = new JsHttpRequest();
        req.onreadystatechange = function() {
            if(req.readyState == 4) {
                if(req.responseJS) {
                    let o=document.getElementById('min_price_'+city_id);
                    if(req.responseJS.ret==0) {
                        if (o) o.value = req.responseJS.response;
                    } else if (o) o.value='?';
                }
            }
        }
        req.caching = false;
        req.open('POST', 'ajax/php_fun.php?type=get_min_price', true);
        req.send({ ct: city_id, sv: service});
    }
}

function ajax_load_preset(id) {
    let req = new JsHttpRequest();
    req.onreadystatechange = function() {
        if(req.readyState == 4) {
            if(req.responseJS) {
                if(req.responseJS.ret==0) {
                    let data = req.responseJS.data;

                    let o = document.getElementById('service_id');
                    if(o) o.value = data['service_id'];
                    o = document.getElementById('label');
                    if(o) o.value = data['label'];
                    o = document.getElementById('budget_daily');
                    if(o) o.value = data['budget_daily'];
                    o = document.getElementById('max_cpc');
                    if(o) o.value = data['max_cpc'];
                    o = document.getElementById('template_name');
                    if(o) o.innerHTML = data['template_name'];
                    o = document.getElementById('language');
                    for(let i=0; i<o.length; i++) {
                        if(o[i].value == data['language']) {
                            o[i].selected = true;
                            break;
                        }
                    }
                    o = document.getElementById('keywords');
                    if(o) o.value = data['keywords'];
                    for(let i=1; i<=15; i++) {
                        o = document.getElementById('headline' + i);
                        if(o) o.value = data['headline' + i];
                        o = document.getElementById('headline' + i + 'Pos');
                        if(o) o.value = data['headline' + i + 'Pos'];
                        o = document.getElementById('description' + i);
                        if(o) o.value = data['description' + i];
                        o = document.getElementById('description' + i + 'Pos');
                        if(o) o.value = data['description' + i + 'Pos'];
                    }
                    // города
                    // let selected_city_str = data['selected_city'];
                    // let selected_city = selected_city_str.split(',');
                    // for(let i=0; i<selected_city.length; i++) {
                    //     add_city_row(selected_city[i]);
                    //     o = document.getElementById('radius_' + selected_city[i]);
                    //     if(o) o.value = data['radius_' + selected_city[i]];
                    //     o = document.getElementById('min_price_' + selected_city[i]);
                    //     if(o) o.value = data['min_price_' + selected_city[i]];
                    //     o = document.getElementById('camp_name_' + selected_city[i]);
                    //     if(o) o.value = data['camp_name_' + selected_city[i]];
                    //     o = document.getElementById('url_' + selected_city[i]);
                    //     if(o) o.value = data['url_' + selected_city[i]];
                    // }

                } else alert(req.responseJS.error);
            }
        }
    }
    req.caching = false;
    req.open('POST', 'ajax/php_fun.php?type=load_preset', true);
    req.send({ pi: id});
}