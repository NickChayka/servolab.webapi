/*
* Copyright 2010 2K-Group. All rights reserved.
* 2K-GROUP PROPRIETARY/CONFIDENTIAL. 
* http://www.2k-group.com
* Author Nick Chayka
*/
var first_timeout=1000;
var timeout=3000;
var close_timeout=2000;
var show=false;
var timers_arr=new Array;	// ��������
var close_timer;		// ������� ��� ��������
var row_id=0;			// ������������� �����
var old_str='';			// �������� ������ ������ ��� ���������� ������ ������������
var current_row_id=1;	// ������������� ��������� ������ ��� ����������
var result_rows_chain = new Array;
var active_row_mouse_color='#76fff4';
var active_row_keyb_color='#83ff92';
var default_row_color='#f0f0f0';
var Shift=false;
var Ctrl=false;
var Alt=false;
var mover=false;
var found_user=false;
var request_id;
var last_task_row=0;
var crop=false;
var last_user_row=0;
var max_result_win_size=getClientHeight()/2;
if(max_result_win_size<300) max_result_win_size=300;
function close_search_window() {
	if(!mover) autoclose();
}
function goto_link(id) {
	if(show) {
		var o=document.getElementById('r_'+id);
		if(o) document.location.href=o.getAttribute('nurl');
	}
}
function highlite_move(d) {
	var old_row=current_row_id;
	if(d==1) current_row_id=result_rows_chain[current_row_id]['next'];
	else current_row_id=result_rows_chain[current_row_id]['prev'];
	if(current_row_id<0) current_row_id=0;
	else if(current_row_id>row_id) current_row_id=row_id;
	var o=document.getElementById('r_'+current_row_id);
	if(old_row!=current_row_id && o) {
		document.getElementById('r_'+old_row).style.backgroundColor=default_row_color;
		o.style.backgroundColor=active_row_keyb_color;
		if(crop) {
			// ������� ���������: 
			var obj=document.getElementById('result');
			var win_top=obj.scrollTop;
			// ������������ ����� ������ ���� ������� �� �������� � ������
			var delta=0;
			var tmp=o.offsetTop+20;
			if(o.offsetTop<win_top) delta=o.offsetTop-win_top;
			else if(tmp>(win_top+max_result_win_size)) delta=tmp-win_top-max_result_win_size;
			obj.scrollTop+=delta;
		}
	} else current_row_id=old_row;
}
function show_help() {
	var new_str=document.getElementById('search_str').value;
	if(old_str!=new_str) {
		for(var i=0; i<timers_arr.length; i++) if(timers_arr[i]!=null) clearTimeout(timers_arr[i]);
		timers_arr=new Array;
		old_str=new_str;
		if(new_str!="") {
			request_id=Math.random();
			found_user=false;
			depth=0;
			current_row_id=0;
			row_id=0;
			crop=false;
			result_rows_chain = new Array;
			var dst=document.getElementById('result');
			dst.innerHTML='<span>��� �����..</span>';
			dst.style.height='';
			var inp_pos=getElementPosition('search_str');
			var o=document.getElementById('result');
			o.style.left=(inp_pos['left']-100)+'px';
			var y=inp_pos['top']+20;
			o.style.top=y+'px';
			show=true;
			o.style.display='';
			ajax_get_result(0);
		} else autoclose();
	}
}
function autoclose() {
	show=false;
	for(var i=0; i<timers_arr.length; i++) if(timers_arr[i]!=null) clearTimeout(timers_arr[i]);
	timers_arr=new Array;
	check_search_state();
	var o=document.getElementById('result');
	o.style.display='none';
	old_str='';
}
// Ajax �����
function ajax_get_result(depth) {
	var query = '' + document.getElementById('search_str').value;
	var req = new JsHttpRequest();
	req.onreadystatechange = function() {
		var dst=document.getElementById('result');
		var s='';
		var tmp=0;
		if(req.readyState == 4) {
			if(request_id!=req.responseJS.rid) return; // ���� ID ������� �� ��������� - ����� ������������
			if(req.responseJS) {
				var answer_depth=req.responseJS.depth;
				if(req.responseJS.countz > 0) {
					if(!found_user) {
						dst.innerHTML='';
						result_rows_chain[row_id] = new Array;
						result_rows_chain[row_id]['prev']=0;
						result_rows_chain[row_id]['next']=0;
					}
					var first_element=row_id;
					var last_element=row_id;
					for(var i=0; i<req.responseJS.countz; i++) {
						result_rows_chain[row_id] = new Array;
						tmp=row_id-1; if(tmp<0) tmp=0;
						result_rows_chain[row_id]['prev']=0;
						if(i!=0 && result_rows_chain[tmp]) {
							result_rows_chain[tmp]['next']=row_id;
							result_rows_chain[row_id]['prev']=tmp;
						}
						s+='<div class="li" onmouseover="highlite_res_row('+row_id+', \'on\')" onmouseout="highlite_res_row('+row_id+', \'off\')" onclick="goto_link('+row_id+')"><span id="r_'+row_id+'" nurl="'+eval('req.responseJS.link_'+i)+'" class="ldata';
						if(row_id==current_row_id) s+=' active';
						s+='">'+eval('req.responseJS.text_'+i)+'</span></div>';
						last_element=row_id;
						result_rows_chain[row_id]['next']=++row_id;
					}
				}
				if(s!='') {
					if(answer_depth<3) {
						if(!found_user) {
							dst.innerHTML+='<div id="task_container">'+s+'</div>';
							// ��� ������� - ��������� � ��� ��� ������ ���������� ����� - �� ���� �������� ��� ��������.
							// ������� �� ��� � ������ ������� ����� ����� ������ - ���������� �������� ID ������ ������ � ����� �����
							found_user=true;
						} else {
							document.getElementById('task_container').innerHTML+=s;
							result_rows_chain[first_element]['prev']=last_user_row;
						}
						last_user_row=last_element;
					}
					var test_var=Math.round(max_result_win_size/18);
					if(!crop && row_id>test_var) {
						crop=true;
						document.getElementById('result').style.height=max_result_win_size+'px';
					}
				}
			}
			if(depth==0) {
				if(answer_depth==0) timers_arr[timers_arr.length]=setTimeout('ajax_get_result(1)', timeout);
				timers_arr[timers_arr.length]=setTimeout('ajax_get_result(2)', timeout);
			} else if(depth==2) timers_arr=new Array;
			check_search_state();
		}
	}
	req.caching = true;
	req.open('POST', 'ajax/php_fun.php?type=multisearch', true);
	req.send({ q: query, dpt: depth, rid:request_id });
}
function highlite_res_row(rid, mode) {
	var c_row_id='r_'+current_row_id;
	obj=document.getElementById('r_'+rid);
	if(obj.getAttribute('id')!=c_row_id) {
		if(mode=='on') {
			mover=true;
			if(close_timer!=null) clearTimeout(close_timer);
			obj.style.backgroundColor=active_row_mouse_color;
		} else {
			mover=false;
			close_timer=setTimeout('close_search_window()', close_timeout);
			obj.style.backgroundColor=default_row_color;
		}
	}
}
// ��������� �������
function getElementPosition(elemId) {
	var elem = document.getElementById(elemId);
	var w = elem.offsetWidth;
	var h = elem.offsetHeight;
	var l = 0;
	var t = 0;
	while(elem) {
		l += elem.offsetLeft;
		t += elem.offsetTop;
		elem = elem.offsetParent;
	}
	return {"left":l, "top":t, "width": w, "height":h};
}
function getClientHeight() {
  return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight;
}
function reg_event(evt) {//��������� �������
	evt = (evt) ? evt : window.event;
	code=evt.keyCode;
	if(!show) return true;
	else if(code==16) Shift=true;
	else if(code==17) Ctrl=true;
	else if(code==18) Alt=true;
	else if(code==38) highlite_move(-1);		// ����
	else if(code==40) highlite_move(1);	// �����
	else if(code==13) {
		if(row_id>0) {
			if(current_row_id==0) document.multisearch_form.submit();
			else goto_link(current_row_id);
		} else return false;
	} else if(code==27) {
		mover=false;
		close_timer=setTimeout('close_search_window()', close_timeout);
	} else return true;
	return false;
}
function reg_event_up(evt) {
	evt = (evt) ? evt : window.event;
	code=evt.keyCode;
	if(code==16) Shift=false;
	if(code==17) Ctrl=false;
	if(code==18) Alt=false;
	return false;
}
function check_search_state() {
	var ret=false;
	for(var i=0; i<timers_arr.length; i++)
		if(timers_arr[i]) {
			ret=true;
			break;
		}
	var o=document.getElementById('search_str');
	if(ret) o.className='search';
	else if(!show || row_id>0) o.className='ready';
	else {
		o.className='empty_result';
		document.getElementById('result').innerHTML='<span>�� ������� ������</span>';
	}
}
document.onkeydown=reg_event;
document.onkeyup=reg_event_up;