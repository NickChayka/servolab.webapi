<?php
//********************************************************************
//           Connect with DataBase and include Config
//********************************************************************
    include_once '../cfg.php';
    include('JsHttpRequest.php');

//********************************************************************
    $JsHttpRequest = new JsHttpRequest("utf-8");
    $q = trim($_POST['q']);
    $type = $_GET['type'];
    $ajax_array=array();
    switch($type) {

	    case 'get_min_price':
		    $s='?';
            $city=(int)$_POST['ct'];
            $service=(int)$_POST['sv'];
            $rs = $link->query('SELECT p.service_id, p.service_name_ru, p.service_name_ua, min(p.user_price) min_price
                FROM servolab_views.v_pos_service_price p, servolab_views.pos c, servolab_views.companies comp
                where p.pos_id = c.id and c.city_id='.$city.' and p.service_id='.$service.' and comp.id=c.company_id and comp.partner=1');
            if($rs->num_rows == 1) {
                $r = $rs->fetch_assoc();
                $s = ceil($r['min_price']/100);
            }
            $rs->close();
            $ajax_array['ret']=0;
            $ajax_array['response']=$s;
            break;

        case 'load_preset':
            $id = (int)$_POST['pi'];
            $ajax_array['ret']=0;
            $rs = $link->query('select * from servolab_watchdog.ads_preset where id='.$id);
            if($rs->num_rows != 1) {
                $ajax_array['ret'] = -1;
                $ajax_array['error'] = 'Preset not found';
            } else {
                $r = $rs->fetch_assoc();
                $ajax_array['data'] = unserialize($r['body']);
            }
            break;
    }
    $_RESULT = $ajax_array;