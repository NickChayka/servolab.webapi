<?php

ini_set('display_errors', 1);
ini_set('max_execution_time','0');


require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/google_mysql_cfg.php';

if (php_sapi_name() != 'cli') {
    throw new Exception('This application must be run on the command line.');
}

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Gmail API PHP Quickstart');
    $client->setScopes(Google_Service_Gmail::GMAIL_READONLY);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}


/**
 * Expands the home directory alias '~' to the full path.
 * @param string $path the path to expand.
 * @return string the expanded path.
 */
function expandHomeDirectory($path) {
    $homeDirectory = getenv('HOME');
    if (empty($homeDirectory)) {
        $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
    }
    return str_replace('~', realpath($homeDirectory), $path);
}

/**
 * Get list of Messages in user's mailbox.
 *
 * @param  Google_Service_Gmail $service Authorized Gmail API instance.
 * @param  string $userId User's email address. The special value 'me'
 * can be used to indicate the authenticated user.
 * @return array Array of Messages.
 */
function listMessages($service, $userId, $optArr = []) {
    $pageToken = NULL;
    $messages = array();
    do {
        try {
            if ($pageToken) {
                $optArr['pageToken'] = $pageToken;
            }
            $messagesResponse = $service->users_messages->listUsersMessages($userId, $optArr);
            if ($messagesResponse->getMessages()) {
                $messages = array_merge($messages, $messagesResponse->getMessages());
                $pageToken = $messagesResponse->getNextPageToken();
            }
        } catch (Exception $e) {
            print 'An error occurred: ' . $e->getMessage();
        }
    } while ($pageToken);

    return $messages;
}

function getHeaderArr($dataArr) {
    $outArr = [];
    foreach ($dataArr as $key => $val) {
        $outArr[$val->name] = $val->value;
    }
    return $outArr;
}

function getBody($dataArr) {
    $outArr = [];
    foreach ($dataArr as $key => $val) {
        $outArr[] = base64url_decode($val->getBody()->getData());
        break; // we are only interested in $dataArr[0]. Because $dataArr[1] is in HTML.
    }
    return $outArr;
}

function base64url_decode($data) {
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

function getMessage($service, $userId, $messageId) {
    try {
        $message = $service->users_messages->get($userId, $messageId);
        print 'Message with ID: ' . $message->getId() . ' retrieved.' . "\n";

        return $message;
    } catch (Exception $e) {
        print 'An error occurred: ' . $e->getMessage();
    }
}

function base64_to_jpeg($base64_string, $content_type) {
    $find = ["_","-"]; $replace = ["/","+"];
    $base64_string = str_replace($find,$replace,$base64_string);
    $url_str = 'data:'.$content_type.','.$base64_string;
    $base64_string = "url(".$url_str.")";
    $data = explode(',', $base64_string);
    return base64_decode( $data[ 1 ] );
}


// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Gmail($client);
$userId = 'me';

// Get the messages in the user's account.
$messages = listMessages($service, $userId, [
    'labelIds' => ['INBOX'], // Return messages in inbox.
    'q' => 'from:dnalab@ukr.net newer_than:2d'
]);

$media_types = ["video", "image", "application"];

foreach ($messages as $message_thread) {
    $message = $service->users_messages->get($userId, $message_thread['id']);
    $message_parts = $message->getPayload()->getParts();
    $files = array();

    if($message_parts[1]['parts']) $all_parts = $message_parts[1]['parts'];
    else $all_parts = [$message_parts[1]];

    foreach($all_parts as $part) {

        if($part['filename'] != '' and $part['body']['size']>0) {
            // проверим, а нет ли у нас уже такого вложения
            $rs = $link->query('select id from servolab_watchdog.dna_attachments where mail_id="'.$link->escape_string($message['id']).'"');
            if($rs->num_rows > 0) continue; // есть такое, пропустим

            $attach = $service->users_messages_attachments->get($userId, $message['id'], $part['body']['attachmentId']);
            $hdr = [];
            foreach ($part['headers'] as $key => $value) {
                if($value->name == 'Content-Type') $content_type = $value->value;
                $hdr[] = $value->name.':'.$value->value;
            }
            $content_type_val = current(explode("/",$content_type));
            if(in_array($content_type_val, $media_types )) {
                $body = base64_to_jpeg($attach['data'], $content_type); // Only for Image files
            } else {
                $body = base64_decode($attach['data']); // Other than Image Files
            }

            // есть вложение, запишем его
            $sql='insert into servolab_watchdog.dna_attachments set mail_id="'.$link->escape_string($message['id']).'",
                headers="'.$link->escape_string(serialize($hdr)).'", body="'.$link->escape_string(base64_encode($body)).'",
                fname="'.$link->escape_string($part['filename']).'", cdate='.time().', status=0, parsed=0
            ';

            $link->query($sql);
        }
    }
}

echo file_get_contents(WEB_SITE.'parse_xlsx.php');

echo file_get_contents(WEB_SITE.'rebuild_dna.php');