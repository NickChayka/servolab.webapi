<pre>
<?php
ini_set('display_errors', 1);
require_once('google_mysql_cfg.php');

//////////////////////////////////////////////////////////
function clean_lead_code($code) {
    $ret = $code;
    $patterns = $replacements = [];
    $patterns[] = '/-/'; $replacements[] = '';
    $patterns[] = '/т/'; $replacements[] = 't';
    $patterns[] = '/д/'; $replacements[] = 'd';
    $patterns[] = '/а/'; $replacements[] = 'a';
    $patterns[] = '/м/'; $replacements[] = 'm';
    $patterns[] = '/к/'; $replacements[] = 'k';
    $patterns[] = '/ф/'; $replacements[] = 'f';
    $ret = preg_replace($patterns, $replacements, $ret);
    return $ret;
}

//////////////////////////////////////////////////////////

$now = time();


$rs = $link->query('select * from servolab_watchdog.dna_attachments where cdate >= '.($now - 1*24*3600));

//$rs = $link->query('select * from servolab_watchdog.dna_attachments where id = 4 ');
$result = [];

while($r = $rs->fetch_assoc()) {
    $s = 'Файл "'.$r['fname'].'", получен '.date('Y-m-d H:i:s', $r['cdate'])."\n";
    if($r['status'] < 0) $s.='Файл не обработан. Причина: '.$r['parsed_body']."\n";
    else {
        $rs2 = $link->query('SELECT * FROM servolab_watchdog.dna_lead where att_id='.$r['id'].' and lead_code=""');
        if($rs2->num_rows > 0) {
            $s.='файл содержит строки без указания номера направления ('.$rs2->num_rows.' строк):'."\n";
            while($r2 = $rs2->fetch_assoc()) {
                $s.=$r2['fio'].', '.$r2['cdate']."\n";
            }
        }
        $rs2 = $link->query('SELECT * FROM servolab_watchdog.dna_lead where att_id='.$r['id'].' and length(lead_code)!=8 and lead_code!=""');
        if($rs2->num_rows > 0) {
            $s.='файл содержит строки c некорректными номерами направления ('.$rs2->num_rows.' строк):'."\n";
            while($r2 = $rs2->fetch_assoc()) {
                $s.=$r2['fio'].', '.$r2['cdate'].', указанный номер направления: "'.$r2['lead_code'].'"'."\n";
            }
        }
        $rs2 = $link->query('SELECT l.id, l.lead_code, l.cdate, l.fio, d.analiz_id, d.analiz_code, d.analiz_name, d.analiz_price 
            FROM servolab_watchdog.dna_lead l, servolab_watchdog.dna_lead_descr d
            where l.lead_code!="" and att_id='.$r['id'].' and d.lead_id = l.id
            order by l.lead_code');
        $s.="Всего в файле было обработано ".$rs2->num_rows." строк.\n";
        $s.="Направления:\n\n";
        $rs3 = $link->query('select distinct lead_code from servolab_watchdog.dna_lead where att_id='.$r['id'].' and lead_code!="" and lead_code is not null');
        while($lead = $rs3->fetch_assoc()) {
            $lead_code = substr($lead['lead_code'], 0, 2).'-'.substr($lead['lead_code'], 2, 2).'-'.
                substr($lead['lead_code'], 4, 2).'-'.substr($lead['lead_code'], 6, 2);

            $cmpr = $link->query('call servolab_medrep.compare_dnk_servolab("'.$lead_code.'")') or die($link->error);
            while($link->next_result()) $link->store_result();
            $diff_price = $same_price = $our_miss = $their_miss = [];

            while($cmp = $cmpr->fetch_assoc()) {
                // итак.
                // если sales_company_price == 0 - у нас нет такой продажи
                // если analiz_price == 0 - у них нет такой продажи
                // цены в настоящее время сравниваем по sales_company_price
                if($cmp['sales_company_price'] == "") $our_miss[] = 'Код анализа: "'.$cmp['analiz_code'].'", название: "'.$cmp['analiz_name'].'", цена в файле: "'.$cmp['analiz_price'].'", цена в нашем прайсе: "'.$cmp['company_price'].'"';
                elseif($cmp['analiz_price']==0) $their_miss[] = 'Код анализа: "'.$cmp['analiz_code'].'", название: "'.$cmp['analiz_name'].'", от юзера ждём: "'.$cmp['sales_user_paid'].'", цена в нашем прайсе: "'.$cmp['sales_company_price'].'"';
                elseif($cmp['sales_company_price'] != $cmp['analiz_price']) $diff_price[] = 'Код анализа: "'.$cmp['analiz_code'].'", название: "'.$cmp['analiz_name'].'", цена в файле: "'.$cmp['analiz_price'].'", цена продажи в нашей базе: "'.$cmp['sales_company_price'].'"';
                else $same_price[] = 'Код анализа: "'.$cmp['analiz_code'].'", название: "'.$cmp['analiz_name'].'", цена в файле: "'.$cmp['analiz_price'].'", цена продажи в нашей базе: "'.$cmp['sales_company_price'].'"';
            }
            $s.='Направление <b>'.$lead_code.'</b>';
            if(count($our_miss)>0) $s.="\n<i>Эти продажи отсутствуют в нашей базе:</i>\n".join("\n", $our_miss);
            if(count($their_miss)>0) $s.="\n<i>Эти продажи есть в нашей базе, но нет в файле:</i>\n".join("\n", $their_miss);
            if(count($diff_price)>0) $s.="\n<i>У этих продаж не совпадает цена:</i>\n".join("\n", $diff_price);
            if(count($same_price)>0) $s.="\n<i>Эти продажи совпадают:</i>\n".join("\n", $same_price);
            $s.="\n";
            $cmpr->close();
        }
        $rs3->close();
    }
    $result[] = $s;
}
print_r($result);
?>
</pre>
Done
