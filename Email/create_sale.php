<pre>
<?php
require_once('cfg.php');

// для начала получим Firebase токен
define('FIREBASE_AUTH_TOKEN', get_firebase_token(FIREBASE_LOGIN, FIREBASE_PWD));
if(!FIREBASE_AUTH_TOKEN) die("Can't get Firebase token");

$POS_ID = 100070;
$PATIENT_NAME = 'Озерова Т.';
$src = [
    'T5-20-00-06' => [
        [
            'service_id' => '130981',
            'price' => 700,
            'user_price' => 665,
            'analiz_name' => 'ПЛР.SARS-COV-2(ген E1,Ecomon, N)'
        ]
    ]
];

foreach($src as $lead_code => $lead) {
    // надо найти lead_id на основании кода
    $json = find_lead($lead_code, FIREBASE_AUTH_TOKEN);
    if(!$json) {
        // у Серёжи нет такого направления, это ошибка
        echo "Can't find lead ".$lead_code;
        continue;
    }

    // найдём теперь нужную нам инфу - lead_id и item_id[]
    $lead_id = $json->id;
    $items = [];
    foreach($json->items as $item)
        $items[$item->service_id] = $item->item_id;

    // отлично, теперь строим json для создания продажи по этому направлению
    $data = [
        'urgent' => false,
        'pos_id' => $POS_ID,
        'patient_name' => $PATIENT_NAME
    ];
    foreach($lead as $sale) {
        $item_id = (int)$items[$sale['service_id']] ?? null;

        $data['item_config'][] = [
            'item_id' => $item_id,
            'service_id' => ($item_id ? null : (int)$sale['service_id']),
            'company_original_price' => (int)$sale['price'],
            'company_final_user_price' => (int)$sale['user_price']
        ];
    }

    // и регистрируем продажу
    lead_processing($lead_id, FIREBASE_AUTH_TOKEN, $data);

}
/****************************************************************************/
function get_firebase_token($login, $pwd) {
    $myCurl = curl_init();

    $auth = [
        'email' => $login,
        'password' => $pwd,
        'returnSecureToken' => true
    ];

    $post_data=json_encode($auth);

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key='.FIREBASE_API_KEY,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POST => true,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
        CURLOPT_POSTFIELDS => $post_data
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json"
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    if($response) {
        if ($response === false || $info['http_code'] != 200) {
//            $response = "HTTP code ". $info['http_code']. ".";
//            if (curl_error($myCurl))
//                $response .= " ". curl_error($myCurl);
            return false;
        }
    }
    curl_close($myCurl);

    $json = json_decode($response);
    if(!is_object($json)) return false;

    return $json->idToken;
}
function find_lead($lead_code, $fb_auth_token) {
    // надо найти lead_id на основании кода
    $myCurl = curl_init();

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => API_URI.'leads/?create_on_search=1&code='.$lead_code,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json",
        "Authorization: Bearer ".$fb_auth_token
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    curl_close($myCurl);

    echo '<p>find_lead:<br>HTTP code '.$info['http_code'].'<br>'.$response.'</p>';

    $json = json_decode($response);
    if(!is_object($json)) return false;

    return $json;
}
function lead_processing($lead_id, $fb_auth_token, $data) { // и регистрируем продажу
    $myCurl = curl_init();

    $post_data=json_encode($data);
    var_dump($post_data);

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => API_URI.'leads/'.$lead_id.'/processing/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POST => true,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
        CURLOPT_POSTFIELDS => $post_data
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json",
        "Authorization: Bearer ".$fb_auth_token
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    echo '<p>lead_processing:<br>HTTP code '.$info['http_code'].'<br>'.$response.'</p>';
    curl_close($myCurl);
    return $response;
}
?>
</pre>
