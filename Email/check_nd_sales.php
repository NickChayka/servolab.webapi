<?php

if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    // костыль для Ангуляра
    header("HTTP/1.1 204 No content");
    exit;
}

ini_set('display_errors', 'true');
ini_set('max_execution_time','0');

//use PHPMailer\PHPMailer\PHPMailer;
//use PHPMailer\PHPMailer\Exception;

echo "<br>\nstarted - ".date('Y-m-d H:i:s')."<br>\n";
$fp=fopen('/home/servolab/webapi/www/Email/check_nd_sales.pi','a') or die("Can't create check_nd_sales.pi");
if(!flock($fp, LOCK_EX | LOCK_NB)) echo ('Program already running');
else {
    ftruncate($fp,0);
    fputs($fp, date("d-m-Y: H:i:s")." RPC daemon started");
    ini_set('max_execution_time','0');
//********************************************************************

require_once('google_mysql_cfg.php');

define('LABORATORY_CODE', 'ND');

//require 'PHPMailer/src/Exception.php';
//require 'PHPMailer/src/PHPMailer.php';
//require 'PHPMailer/src/SMTP.php';

// для начала авторизуемся

$post_data = [
   "email" => MEDCLOUD_API_LOGIN,
   "password" => MEDCLOUD_API_PWD
];

$myCurl = curl_init();

curl_setopt_array($myCurl, array(
    CURLOPT_URL => MEDCLOUD_API_URI.'auth/login',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_POST => true,
    CURLOPT_SSL_VERIFYHOST => 0,
    CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
    CURLOPT_POSTFIELDS => json_encode($post_data)
));

curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/json",
    "Accept: application/json",
    "Accept-language: ru_RU"
));

$response = curl_exec($myCurl);
$info = curl_getinfo($myCurl);
$error = false;

//var_dump($response);

$c = 0;

if($response) {
    if ($response === false or $info['http_code'] != 200) {
        $error = "Medcloud Auth error. HTTP code ". $info['http_code']. ". ".$response;
        if (curl_error($myCurl))
            $error .= " ". curl_error($myCurl);
    } else {
        $json = json_decode($response);
    }
}
curl_close($myCurl);

if(!$error) {
    $auth_token = $json->accessToken;
//    echo 'Token: '.$auth_token.'<br>';
    // Теперь получим все продажи
    $start_time = time() - 1 * ONE_DAY; // 3 дня

    $from = date('Y-m-d\TH:i:sP', $start_time);
    $to = date('Y-m-d\TH:i:sP');

    $myCurl = curl_init();

    echo '<br><b>'.MEDCLOUD_API_URI.'v1/order?createdAtFrom='.urlencode($from).'&createdAtTo='.urlencode($to).'</b><br>';

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => MEDCLOUD_API_URI.'v1/order?createdAtFrom='.urlencode($from).'&createdAtTo='.urlencode($to),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYHOST => 0
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
        "Authorization: Bearer ".$auth_token,
        "Content-Type: application/json"
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);
    $error = false;

    $orders = [];
    if($response) {
        if ($response === false or $info['http_code'] != 200) {
            $error = "Medcloud getOrders error. HTTP code ". $info['http_code']. ". ".$response;
            if (curl_error($myCurl))
                $error .= " ". curl_error($myCurl);
        } else {
            $orders = json_decode($response);
        }
    }
    curl_close($myCurl);

    if(!$error) {
        foreach($orders as $order) {
            if($order->payedCashTimestamp) { // это оплаченный ордер
                $data = [
                    "order_id" => $order->id,
                    "created" => $order->createdAt,
                    "client" => $order->surname.' '.$order->name.' '.$order->patronymic,
                    "phone" => $order->phone,
                    "payed_date" => $order->payedCashTimestamp,
                    "payed" => $order->priceToPay
                ];
                // давайте попробуем найти у себя такой заказ
                $rs = $link->query('select id from servolab_watchdog.medcoud_log where external_id = '.$order->id);
                if($rs->num_rows == 0) { // нету ещё такого заказа, сохраним его
                    $sql = 'insert into servolab_watchdog.medcoud_log set ';
                    $sql.= 'created = "'.date('Y-m-d H:i:s').'",';
                    $sql.= 'servolab_code = "",';
                    $sql.= 'external_code = "'.$order->code.'",';
                    $sql.= 'external_id = "'.$order->id.'",';
                    $sql.= 'external_city_id = "'.(int)$order->cityId.'",';
                    $sql.= 'external_filial_id = "'.(int)$order->filialId.'",';
                    $sql.= 'patient_name = "'.$link->escape_string($order->name).'",';
                    $sql.= 'patient_surname = "'.$link->escape_string($order->surname).'",';
                    $sql.= 'patient_patronymic = "'.$link->escape_string($order->patronymic).'",';
                    $sql.= 'patient_phone = "'.$order->phone.'",';
                    $sql.= 'external_created = "'.date('Y-m-d H:i:s', strtotime($order->createdAt)).'",';
                    $sql.= 'payed_datetime = "'.date('Y-m-d H:i:s', strtotime($order->payedCashTimestamp)).'",';
                    $sql.= 'payed = "'.$order->priceToPay.'",';
                    $sql.= 'status = "new",';
                    $sql.= 'cmt = "", lab="'.LABORATORY_CODE.'",';
                    $sql.= 'servolab_payed="0.00",';
                    $sql.='body = "'.$link->escape_string(json_encode($order)).'"';
                    $link->query($sql);
                    $c++;
                }
            }
        }
    }
}

//********************************************************************
    flock($fp, LOCK_UN);
}
fclose($fp);
echo "<br>\nfinished - ".date('Y-m-d H:i:s')."<br>\n";
echo "Total: ".$c." sales detected<br>\n";

/////////////////////////////////////////////////////////////

?>
Done.