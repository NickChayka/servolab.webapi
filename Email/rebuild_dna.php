<?php
ini_set('max_execution_time','0');

require_once('google_mysql_cfg.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

define('DEFAULT_POS_ID', "ПІБ Виборгська");
$POS_ID = [
    "ПІБ Виборгська" => 100069,
    "ПІБ Маяковського" => 100070,
    "ПІБ Гришка" => 100071,
    "ПІБ Подвойського,9" => 100072
];

//////////////////////////////////////////////////////////
function generate_lead_code($start) {
    if($start<0 or $start>99999) return false; // переполнение
    $ret = 't52'.str_pad($start, 5, "0", STR_PAD_LEFT);
    return $ret;
}
function clean_lead_code($code) {
    $ret = $code;
    $patterns = $replacements = [];
    $patterns[] = '/-/'; $replacements[] = '';
    $patterns[] = '/т/'; $replacements[] = 't';
    $patterns[] = '/д/'; $replacements[] = 'd';
    $patterns[] = '/а/'; $replacements[] = 'a';
    $patterns[] = '/м/'; $replacements[] = 'm';
    $patterns[] = '/к/'; $replacements[] = 'k';
    $patterns[] = '/ф/'; $replacements[] = 'f';
    $ret = preg_replace($patterns, $replacements, $ret);
    return $ret;
}
function restore_lead_code($small_code) {
    $tmp = strtoupper($small_code);
    $s = substr($tmp, 0, 2).'-'.substr($tmp, 2, 2).'-'.substr($tmp, 4, 2).'-'.substr($tmp, 6, 2);
    return $s;
}
function is_lead_exists($lead_code, $link) {
    if(substr($lead_code, 0, 1) == 't') { // это печатное направление, их отдельно чекаем
        $int_code = (int)substr($lead_code, 1);
        $check = $link->query('SELECT * FROM servolab_views.printed_lead_allocation where '.$int_code.'>=right(paper_lead, 7) and '.$int_code.'<(right(paper_lead, 7)+amount)');
        $ret = ($check->num_rows > 0);
    } else { // это электронное направление
        $check = $link->query('SELECT * FROM servolab_views.leads where lower(replace(code,"-","")) = "'.$lead_code.'"');
        $ret = ($check->num_rows == 1);
    }
    return $ret;
}
/****************************************************************************/
function get_firebase_token($login, $pwd) {
    $myCurl = curl_init();

    $auth = [
        'email' => $login,
        'password' => $pwd,
        'returnSecureToken' => true
    ];

    $post_data=json_encode($auth);

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key='.FIREBASE_API_KEY,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POST => true,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
        CURLOPT_POSTFIELDS => $post_data
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json"
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    if($response) {
        if ($response === false || $info['http_code'] != 200) {
            return false;
        }
    }
    curl_close($myCurl);

    $json = json_decode($response);
    if(!is_object($json)) return false;

    return $json->idToken;
}
function find_lead($lead_code, $fb_auth_token) { // надо найти lead_id на основании кода
    $myCurl = curl_init();

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => API_URI.'leads/?create_on_search=1&code='.$lead_code,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json",
        "Authorization: Bearer ".$fb_auth_token
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    echo '<p>find_lead:<br>LeadCode: '.$lead_code.'<br>HTTP code '.$info['http_code'].'<br>'.$response.'</p>';

    curl_close($myCurl);

    $json = json_decode($response);
    if(!is_object($json)) return false;

    return $json;
}
function lead_processing($lead_id, $fb_auth_token, $data) { // и регистрируем продажу
    $myCurl = curl_init();

    $post_data=json_encode($data);

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => API_URI.'leads/'.$lead_id.'/processing/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POST => true,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
        CURLOPT_POSTFIELDS => $post_data
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json",
        "Authorization: Bearer ".$fb_auth_token
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    curl_close($myCurl);

    echo '<p>lead_processing:<br>POST params: '.$post_data.'<br>HTTP code '.$info['http_code'].'<br>'.$response.'</p>';

    if($info['http_code'] == 201) $ret = '<span style="color: darkblue">Ok, '.$response.'</span>';
    elseif($info['http_code'] == 400) {
        $err = json_decode($response);
        $ret = '<span style="color: red">ОШИБКА при регистрации продажи! Код ошибки 400. '.$err->error.': ' . $err->description . '</span>';
    } else $ret = 'HTTP code '.$info['http_code'].'. Response: '.$response;
    return $ret;
}
//////////////////////////////////////////////////////////
$now = time();

// для начала получим Firebase токен
define('FIREBASE_AUTH_TOKEN', get_firebase_token(FIREBASE_LOGIN, FIREBASE_PWD));
if(!FIREBASE_AUTH_TOKEN) die("Can't get Firebase token");

$rs = $link->query('SELECT * FROM servolab_medrep.config where var = "lead_generation_start"');
$r = $rs->fetch_assoc();
$LEAD_GENERATION_START = (int)$r['val'];

$rs = $link->query('select * from servolab_watchdog.dna_attachments where status = 0 and parsed = 1');
//$rs = $link->query('select * from servolab_watchdog.dna_attachments where parsed = 1 order by id desc limit 1');
$pretty = $att = $sale = $all_leads = [];
$pos_id = $POS_ID[DEFAULT_POS_ID];

while($r = $rs->fetch_assoc()) {
    $att[$r['id']] = [
        'file' => $r['fname'],
        'cdate' => $r['cdate'],
        'body' => base64_decode($r['body'])
    ];
    $rows = unserialize($r['parsed_body']);
    $row_num = 0;
    foreach($rows as $row) {
        $row_num++;
        if((int)$row[1] == 0) {
            // это похоже просто заголовки
            // тогда надо попробовать определить pos_id
            $pos_id = $POS_ID[trim($row[0])];
            if((int)$pos_id == 0) $pos_id = $POS_ID[DEFAULT_POS_ID];
            continue;
        }
        $lead_code = clean_lead_code(mb_strtolower($row[4]));

        $fio = $row[0];

        $days = $row['2'];
        $date = new DateTime('1899-12-30');
        $date->add(new DateInterval("P{$days}D"));
        list($span, $time) = explode(' ', $row[3]);

        $pretty[$r['id']][$lead_code][$fio][$date->format('Y-m-d').' '.$time][] = [
            'analiz_id' => $row[1],
            'analiz_code' => $row[5],
            'analiz_name' => $row[6],
            'price' => $row[7],
            'user_price' => $row[8],
            'xls_row_num' => $row_num
        ];

        $all_leads[$lead_code] = $pos_id;
    }
    $link->query('update servolab_watchdog.dna_attachments set status = 1 where id = '.$r['id']);
}

//var_dump($pretty);
//var_dump($all_leads);
//die();
////////////////////////////////////////////////////
/// ну вроде как файл считан и разобран в массивы $pretty и $att
/// теперь начинаем проверять направления.
/// суть в чём - берём направление из $pretty и смотрим, чтоб оно у нас было
/// если у нас нет такого направления - записываем его в ошибки
/// если оно у нас есть, проверяем продажу. если есть продажа - записываем в ошибки
/// если же направление есть, но продажи нет - создаём продажу.
/// возникла ошибка - пишем в ошибки.
/// отдельная проверка для строк, где не указан номер направления
/// ну и если нам удалось создать продажу - пишем это в успех
/// в конце обработки файла готовим краткое саммари по нему.
/// Типа название файла, получен тогда-то, успешно созданы продажи по таким направлениям (+детали) и Ошибки (с деталями).
/// после обработки всех файлов - итоговое саммари отсылаем на почту мне, Косте, Инне.
////////////////////////////////////////////////////
$services = [];
$msg = '';
foreach($pretty as $att_id=>$att_content) { // обработаем массив
    $msg .= '<br>Обрабатываем вложение <b>'.$att[$att_id]['file'].' (#'.$att_id.')</b><br>';
    $link->query('delete from servolab_watchdog.dna_lead where att_id='.$att_id);
    foreach($att_content as $_code=>$lead) {
        $lead_code = $_code;
        // для начала проверим, что указан номер направления и он ровно 8 символов
        if(trim($lead_code) == '') {
            $lead_code = generate_lead_code($LEAD_GENERATION_START);
            $all_leads[$lead_code] = $all_leads[$_code];
            $LEAD_GENERATION_START++;
            $msg .= '<br><span style="color: red">Не указан номер направления, генерирую новый номер ['.$lead_code.']</span><br>';
            $msg .= '<pre>'.print_r($lead, true).'</pre><br>';
        } elseif(strlen($lead_code) != 8) {
            $lead_code = generate_lead_code($LEAD_GENERATION_START);
            $all_leads[$lead_code] = $all_leads[$_code];
            $LEAD_GENERATION_START++;
            $msg .= '<br><span style="color: red">Обнаружен некорректный номер направления ['.$_code.'], генерирую новый номер ['.$lead_code.']</span><br>';
            $msg .= '<pre>'.print_r($lead, true).'</pre><br>';
        }
        // проверяем, что у нас есть такое направление. Если нет - ругаемся.
        $msg .= '<br>Направление <b>'.restore_lead_code($lead_code).'</b> - ';
        if(is_lead_exists($lead_code, $link)) $msg .= '<span style="color: green">есть в нашей базе</span><br>';
        else {
            $lead_code = generate_lead_code($LEAD_GENERATION_START);
            $all_leads[$lead_code] = $all_leads[$_code];
            $LEAD_GENERATION_START++;
            $msg .= '<span style="color: red">нет в нашей базе, генерирую новый номер ['.$lead_code.']</span><br>';
            $msg .= '<pre>'.print_r($lead, true).'</pre><br>';
        }
        // теперь проверим, что у нас ещё нет продажи по этому направлению.
        // если есть - то ругаемся и выходим.
        $check = $link->query('SELECT * FROM servolab_views.sales_light where lower(replace(lead_code,"-","")) = "'.$lead_code.'" ');
        if($check->num_rows == 0) $msg .= '<span style="color: green">У нас нет продаж по этому направлению, регистрируем продажу.</span><br>';
        else {
            $msg .= '<span style="color: red">у нас уже есть продажи по этому направлению, нужна проверка оператора</span><br>';
            continue;
        }
        $pos_id = (int)$all_leads[$lead_code];

        // дошли до этого места - значит надо создать продажу по направлению
        foreach($lead as $fio=>$details) {
            $for_sale = [];
            $processing = true;
            foreach($details as $date=>$items) {
                $link->query('insert into servolab_watchdog.dna_lead set att_id='.$att_id.', lead_code="'.$link->escape_string($lead_code).'", cdate = "'.$link->escape_string($date).'", fio = "'.$link->escape_string($fio).'"');
                $lead_id = $link->insert_id;
                foreach($items as $item) {
                    if($item['user_price'] == '') $item['user_price'] = $item['price'];
                    $link->query('insert into servolab_watchdog.dna_lead_descr set lead_id='.$lead_id.', analiz_id='.(int)$item['analiz_id'].', 
                    analiz_code='.(int)$item['analiz_code'].', analiz_name="'.$link->escape_string($item['analiz_name']).'", 
                    analiz_price="'.$link->escape_string($item['price']).'", user_price="'.$link->escape_string($item['user_price']).'"');
                    // увы и ах, надо найти service_id
                    $service_id = $services[$pos_id][$item['analiz_code']]['service_id'];
                    if($service_id == '') {
                        $check = $link->query('SELECT * FROM servolab_views.v_pos_service_price where company_id=50007 and 
                            pos_id='.$pos_id.' and company_custom_services_id='.$item['analiz_code'].' and 
                            price>='.($item['price']*100).' order by price');
                        $check_res = $check->fetch_assoc();
                        $service_id = $check_res['service_id'];
                        $services[$pos_id][$item['analiz_code']]['service_id'] = $service_id;
                        $services[$pos_id][$item['analiz_code']]['our_price'] = $check_res['price'];
                        $services[$pos_id][$item['analiz_code']]['our_user_price'] = $check_res['user_price'];
                    }
                    $for_sale[] = [
                        'service_id' => $service_id,
                        'price' => $services[$pos_id][$item['analiz_code']]['our_price'],
                        'user_price' => $services[$pos_id][$item['analiz_code']]['our_user_price'],
                        'analiz_name' => $item['analiz_name']
                    ];
                    $msg .= '+ '.$item['analiz_name'].' ['.$service_id.'], прайс '.$item['price'].', уплочено '.$item['user_price'];
                    if($service_id == '') {
                        $processing = false;
                        $msg .= ' <span style="color: red">!!! Такого анализа нет в нашей базе</span>';
                    } elseif($item['price'] != $services[$pos_id][$item['analiz_code']]['our_price']/100) {
                        $processing = false;
                        $msg .= ' <span style="color: red">!!! Не совпадает цена сервиса в лаборатории и в Серволаб ('.($services[$pos_id][$item['analiz_code']]['our_price']/100).' грн)</span>';
                    }
                    $msg .= '<br>';
                }
            }
            if(!$processing) continue; // цена не совпадает, либо и вовсе нет анализа - не будем продолжать

            //
            $lcode = restore_lead_code($lead_code);
            $sale[$lcode] = $for_sale;

            // надо найти lead_id на основании кода
            $json = find_lead($lcode, FIREBASE_AUTH_TOKEN);
            if(!$json) {
                // у Серёжи нет такого направления, это ошибка
                $msg .= '<span style="color: red">Странная дичь, не могу найти направление '.$lcode.' в нашей базе</span><br>';
                continue;
            }

            // найдём теперь нужную нам инфу - lead_id и item_id[]
            $lead_id = $json->id;
            $items = [];
            foreach($json->items as $item)
                $items[$item->service_id] = $item->item_id;

            // отлично, теперь строим json для создания продажи по этому направлению
            $data = [
                'urgent' => false,
                'pos_id' => $all_leads[$lead_code],
                'patient_name' => $fio
            ];
            foreach($for_sale as $rw) {
                if((int)$rw['service_id'] > 0) {
                    $item_id = (int)$items[$rw['service_id']] ?? null;
                    $data['item_config'][] = [
                        'item_id' => $item_id,
                        'service_id' => ($item_id ? null : (int)$rw['service_id']),
                        'company_original_price' => (int)$rw['price'],
                        'company_final_user_price' => (int)$rw['user_price']
                    ];
                } else {
                    $data['custom_items'][] = [
                        'company_price' => (int)$rw['price'],
                        'company_final_user_price' => (int)$rw['user_price'],
                        'name' => $rw['analiz_name']
                    ];
                }
            }

//            var_dump($data);

            // и регистрируем продажу
            $sale_result = lead_processing($lead_id, FIREBASE_AUTH_TOKEN, $data);
            $msg .= 'Ответ API при регистрации продажи: '.$sale_result.'<br>';
        }
    }
}

$link->query('delete from servolab_watchdog.dna_lead_descr where lead_id not in (select id from dna_lead)');

if($msg != '') {
    $msg .= '<hr>';
    // отправим почту
    $tmp_files = [];
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        $mail->CharSet = 'utf-8';
        //Server settings
        $mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'report.servolab.one@gmail.com';    // SMTP username
        $mail->Password = 'majpyqqqdldhvgjt';                 // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('report.servolab.one@gmail.com', 'Servolab API');
        $mail->addAddress('nickch@2kgroup.com', 'Servolab DevTeam');     // Add a recipient
        $mail->addAddress('kz@servolab.one', 'Константин Журба');     // Add a recipient
        $mail->addAddress('inna.torkotiuk@servolab.one', 'Инна Торкотюк');     // Add a recipient

//        $mail->addAddress('ellen@example.com');               // Name is optional
//        $mail->addReplyTo('info@example.com', 'Information');
//        $mail->addCC('cc@example.com');
//        $mail->addBCC('bcc@example.com');

        foreach($att as $att_id=>$attachment) {
            $fname = tempnam(sys_get_temp_dir(), 'DNK');
            $fp = fopen($fname, "wb");
            fwrite($fp, $attachment['body']);
            fclose($fp);
            $mail->addAttachment($fname, $attachment['file']);
            $tmp_files[] = $fname;
        }

        //Attachments
//        $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//        $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Результат разбора почты от ДНК-лаб';
        $mail->Body    = 'Текущая дата/время: '.date('Y-m-d H:i:s').'<br>'.$msg;
//        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        echo 'Message has been sent';
    } catch (Exception $e) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    }

    foreach($tmp_files as $file)
        if($file!='' and is_file($file) and file_exists($file))
            @unlink($file);

}

$link->query('update servolab_medrep.config set val = "'.$LEAD_GENERATION_START.'" where var="lead_generation_start"');

?>
<p>Done</p>
