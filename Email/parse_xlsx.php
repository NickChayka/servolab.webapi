<?php

ini_set('display_errors', 1);
ini_set('max_execution_time','0');

require_once('SimpleXLS.php');
require_once('SimpleXLSX.php');

require_once('google_mysql_cfg.php');

$rs = $link->query('select * from servolab_watchdog.dna_attachments where parsed = 0');
while($r = $rs->fetch_assoc()) {
    list($f, $ext) = explode('.', strtolower($r['fname']));
    if($ext == 'xls') {
        if ( $xls = SimpleXLS::parse(base64_decode($r['body']), true) ) {
            $parsed_body = serialize($xls->rows());
            $status = 1;
        } else {
            $parsed_body = SimpleXLS::parseError();
            $status = -2;
        }
    } elseif($ext == 'xlsx') {
        if ( $xlsx = SimpleXLSX::parse(base64_decode($r['body']), true) ) {
            $parsed_body = serialize($xlsx->rows());
            $status = 1;
        } else {
            $parsed_body = SimpleXLSX::parseError();
            $status = -2;
        }
    } else {
        $status = -1;
        $parsed_body = 'Not a xls/xlsx file';
    }
    $link->query('update servolab_watchdog.dna_attachments set parsed="'.$status.'", parsed_body="'.$link->escape_string($parsed_body).'" where id='.$r['id']);
}
?>
Done
