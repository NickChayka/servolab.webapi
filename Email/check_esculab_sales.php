<?php

if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    // костыль для Ангуляра
    header("HTTP/1.1 204 No content");
    exit;
}

ini_set('display_errors', 'true');
ini_set('max_execution_time','0');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

echo "<br>\nstarted - ".date('Y-m-d H:i:s')."<br>\n";
$fp=fopen('/home/servolab/webapi/www/Email/check_esculab_sales.pi','a') or die("Can't create check_esculab_sales.pi");
//$fp=fopen('/home/nick/www/2k/Servolab/Email/check_esculab_sales.pi','a') or die("Can't create check_esculab_sales.pi");
if(!flock($fp, LOCK_EX | LOCK_NB)) echo ('Program already running');
else {
    ftruncate($fp,0);
    fputs($fp, date("d-m-Y: H:i:s")." RPC daemon started");
    ini_set('max_execution_time','0');
//********************************************************************

require_once('google_mysql_cfg.php');

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

$myCurl = curl_init();

curl_setopt_array($myCurl, array(
    CURLOPT_URL => ESCULAB_API_URI.'getOrderSchedule',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_USERPWD => ESCULAB_API_LOGIN.':'.ESCULAB_API_PWD,
    CURLOPT_SSL_VERIFYHOST => 0,
    CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1
));

curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/json",
    "Accept: application/json",
    "Accept-language: ru_RU"
));

$response = curl_exec($myCurl);
$info = curl_getinfo($myCurl);

if($response) {
    if ($response === false) {
        $response = "getOrderSchedule error. HTTP code ". $info['http_code']. ". ".$response;
        if (curl_error($myCurl))
            $response .= " ". curl_error($myCurl);
        $json = "Error: ".$response;
    } else {
        $json = json_decode($response);
    }
}
curl_close($myCurl);

$min_date = time() - 30 * ONE_DAY; // 1 месяц
$msg = '';
$our_items = [];
if(is_array($json)) { // похоже, что это валидный ответ
    $esculab_price = get_esculab_price();
    $fb_token = get_firebase_token(FIREBASE_LOGIN_ESCULAB, FIREBASE_PWD_ESCULAB);
    if($fb_token['success']) {
        $doc_info = json_decode($fb_token['result']);
        define('FIREBASE_AUTH_TOKEN', $doc_info->idToken);
    } else {
        $doc_info = false;
        $msg .= '<br><span style="color: red">Не могу получить FB токен дла доктора '.FIREBASE_LOGIN_ESCULAB.'. Ответ АПИ: '.$fb_token['error'].'</span><br>';
        define('FIREBASE_AUTH_TOKEN', false);
    }
    $leads = [];
    foreach($json as $order) {
        if($order->idOrder > 0) { // ага, это продажа, проверяем дальше
            $created = strtotime($order->dt);
            if($created < $min_date) continue; // мы не проверяем продажи старше минимальной даты
            $msg .= '<br><hr>Обрабатываем заказ с Esculab ID <b>'.$order->id.' (дата '.$order->dt.')</b><br>';
            $lead_code = detect_lead_code($order->note);
            if(!$lead_code) { // не смогли найти код направления
                $msg .= '<br><span style="color: red">Не могу найти код направления</span><br>';
                $msg .= '<pre>'.print_r($order, true).'</pre><br>';
                contimue;
            }
            // попробуем найти pos_id на основании idPunkt
            $_pos_id = 1;
            if((int)$order->idPunkt == 0) {
                $msg .= '<br><span style="color: red">В заказе не указано отделение</span><br>';
                $msg .= '<pre>'.print_r($order, true).'</pre><br>';
                contimue;
            } else {
                if($order->idPunkt == 225) $order->idPunkt = 218;
                elseif($order->idPunkt == 156 or $order->idPunkt == 452 or $order->idPunkt == 453) $order->idPunkt = 1;
                $check = $link->query('SELECT * FROM servolab_views.pos where company_id=50051 and external_id = '.$order->idPunkt);
                if($check->num_rows == 0) {
                    $msg .= '<br><span style="color: red">В нашей базе нет отделения Эскулаб с кодом external_id = '.$order->idPunkt.'</span><br>';
                    $msg .= '<pre>'.print_r($order, true).'</pre><br>';
                    continue;
                } else {
                    $tmp = $check->fetch_assoc();
                    $_pos_id = $tmp['id'];
                }
            }

            $lcode = restore_lead_code($lead_code);
            // проверяем, что у нас есть такое направление. Если нет - ругаемся.
            $msg .= '<br>Направление <b>'.$lcode.'</b> - ';
            $lead_exists = is_lead_exists($lead_code, $link);
            if($lead_exists['result']) {
                $msg .= '<span style="color: green">есть в нашей базе</span><br>';
                if($lead_exists['cdate'] != '') $msg .= 'создано: '.$lead_exists['cdate'].'<br>';
                if($lead_exists['reg_date'] != '') $msg .= 'зарегистрировано на АПИ: '.$lead_exists['reg_date'].'<br>';
            } else {
                $msg .= '<span style="color: red">нет в нашей базе, надо будет сгенерировать новый номер</span><br>';
            }
            // теперь проверим, что у нас ещё нет продажи по этому направлению.
            // если есть - то ругаемся и выходим.
            $check = $link->query('SELECT lead_item_id FROM servolab_views.sales_light where lower(replace(lead_code,"-","")) = "'.$lead_code.'" ');
            if($check->num_rows > 0) {
                $msg .= '<span style="color: red">у нас уже есть продажи по этому направлению, нужна проверка оператора</span><br>';
                $msg .= '<pre>'.print_r($order, true).'</pre><br>';
            } else {
                $msg .= '<span style="color: green">У нас нет продаж по этому направлению, регистрируем продажу.</span><br>';
                // выцепим все анализы с ценами из их ордера
                $items = $for_sale = [];
                $proceed = true;
                for($i=0; $i<count($order->services); $i++) {
//                foreach($order->services as $item) {
                    $item = $order->services[$i];
                    $servolab_db = 'Ooops! нет такого анализа в нашей базе';
                    // попробуем найти инфу по этому анализу в нашей базе
                    if(empty($our_items[$_pos_id][$item->idPacket])) {
                        $sql = 'SELECT * FROM servolab_views.v_pos_service_price where company_id=50051 and company_custom_services_id='.(int)$item->idPacket.' and pos_id='.$_pos_id.' order by price limit 1';
                        $rs = $link->query($sql);
                        if($rs->num_rows == 1) {
                            $r = $rs->fetch_assoc();
                            $our_items[$_pos_id][$item->idPacket] = [
                                'service_id' => $r['service_id'],
                                'price' => $r['price'],
                                'user_price' => $r['user_price'],
                                'service_name_ru' => $r['service_name_ru'],
                                'service_name_ua' => $r['service_name_ua']
                            ];
                        } else {
                            $proceed = false;
                            $msg .= '<span style="color: red">Ошибка! В заказе указан анализ [idPacket = '.(int)$item->idPacket.'], которого нет в нашей базе. Нужна проверка оператора.</span><br>';
                        }
                    }
                    $servolab_db = $our_items[$_pos_id][$item->idPacket];
                    $delta = abs($servolab_db['user_price']/100 - $item->price);
                    if($delta != 0) {
                        $msg .= ' <span style="color: red">!!! Не совпадает цена сервиса (idPunkt = '.$order->idPunkt.', price = '.$item->price.' грн) в лаборатории и в Серволаб (pos_id = '.$_pos_id.', price = '.($servolab_db['price']/100).' грн)</span><br>';
                        if($delta > 2) {
                            $msg .= ' <span style="color: red">!!! разница в ценах больше 2-х грн, продажа не будет импортирована, нужна проверка оператором !!!</span><br>';
                            $proceed = false;
                        } else {
                            $msg .= ' <span style="color: blue">! разница в ценах менее 2-х грн, продажа будет импортирована !</span><br>';
                        }
                    }
                    $eprice = find_analiz_in_esculab($item->idPacket, $esculab_price);
                    $items[] = [
                        'idPacket' => $item->idPacket,
                        'packetName' => $eprice->name,
                        'idConf' => $item->idConf,
                        'price' => $item->price,
                        'discount' => $item->discount,
                        'amount' => $item->amount,
                        'debt' => $item->debt,
                        'servolab_db' => $servolab_db
                    ];
                    $for_sale[] = [
                        'service_id' => $servolab_db['service_id'],
                        'price' => $servolab_db['price'],
                        'user_price' => $servolab_db['user_price'],
                        'analiz_name' => $servolab_db['service_name_ua']
                    ];
                    $order->services[$i]->servolab_db = $servolab_db;
                }
                // пробуем создать продажу
                if($proceed) {
                    $msg.='<pre>'.print_r($order, true).'</pre><br>';
                    if(FIREBASE_AUTH_TOKEN) {
                        // надо найти lead_id на основании кода
                        $json = find_lead($lcode, FIREBASE_AUTH_TOKEN);
                        if(!$json) {
                            // у Серёжи нет такого направления, это ошибка
                            $msg .= '<span style="color: red">Странная дичь, не могу найти направление '.$lcode.' в нашей базе</span><br>';
                            continue;
                        }
                        // ну, всё ж есть, го
                        $lead_id = $json->id;
                        $litems = [];
                        foreach($json->items as $litem)
                            $litems[$litem->service_id] = $litem->item_id;

                        // отлично, теперь строим json для создания продажи по этому направлению
                        $data = [
                            'urgent' => false,
                            'pos_id' => $_pos_id,
                            'patient_name' => $json->patient_name ?? 'Пацієнт'
                        ];
                        foreach($for_sale as $rw) {
                            if((int)$rw['service_id'] > 0) {
                                $item_id = (int)$litems[$rw['service_id']] ?? null;
                                $data['item_config'][] = [
                                    'item_id' => $item_id,
                                    'service_id' => ($item_id ? null : (int)$rw['service_id']),
                                    'company_original_price' => (int)$rw['price'],
                                    'company_final_user_price' => (int)$rw['user_price']
                                ];
                            } else {
                                $data['custom_items'][] = [
                                    'company_price' => (int)$rw['price'],
                                    'company_final_user_price' => (int)$rw['user_price'],
                                    'name' => $rw['analiz_name']
                                ];
                            }
                        }

                        echo '<br>**********************<br>регистрируем продажу<br>';
                        var_dump($data);
                        echo '<br>';


                        // и регистрируем продажу
                        $sale_result = lead_processing($lead_id, FIREBASE_AUTH_TOKEN, $data);
                        $msg .= 'Ответ API при регистрации продажи: '.$sale_result.'<br>';

                    }
                } else {
                    $msg .= '<pre>'.print_r($order, true).'</pre><br>';
                }
            }
        }
    }
    if($msg != '') {
        $msg .= '<hr>';
        // отправим почту
        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            $mail->CharSet = 'utf-8';
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'report.servolab.one@gmail.com';    // SMTP username
            $mail->Password = 'majpyqqqdldhvgjt';                 // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('report.servolab.one@gmail.com', 'Servolab API');
            $mail->addAddress('nickch@2kgroup.com', 'Servolab DevTeam');     // Add a recipient
            $mail->addAddress('kostyaz@2kgroup.com', 'Константин Журба');     // Add a recipient
//            $mail->addAddress('kz@servolab.one', 'Константин Журба');     // Add a recipient
            $mail->addAddress('inna.torkotiuk@servolab.one', 'Инна Торкотюк');     // Add a recipient

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Результат проверки продаж Esculab';
            $mail->Body    = 'Текущая дата/время: '.date('Y-m-d H:i:s').'<br>'.$msg;
            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }

    }
} else { // а тут видимо ошибка в данных, не можем ничего проверять, просто напишем про это
    echo $json;
}
//********************************************************************
    flock($fp, LOCK_UN);
}
fclose($fp);
echo "<br>\nfinished - ".date('Y-m-d H:i:s')."<br>\n";

/////////////////////////////////////////////////////////////
function detect_lead_code($note) {
    $str = strstr($note, 'Servolab: ');
    if(!$str) return false; // не нашли кода вообще
    $code = trim(strstr($str, ' '));
    if(strlen($code) != 11) return false; // код вроде как и есть, но не в том формате
    return str_replace('-', '', $code);
}
function is_lead_exists($lead_code, $link) {
    $ret = ['result' => false, 'cdate' => ''];
    if(substr($lead_code, 0, 1) == 't') { // это печатное направление, их отдельно чекаем
        $int_code = (int)substr($lead_code, 1);
        $check = $link->query('SELECT id FROM servolab_views.printed_lead_allocation where '.$int_code.'>=right(paper_lead, 7) and '.$int_code.'<(right(paper_lead, 7)+amount)');
        if($check->num_rows > 0) {
            $ret['result'] = true;
        }
    } else { // это электронное направление
        $check = $link->query('SELECT created FROM servolab_views.leads where lower(replace(code,"-","")) = "'.$lead_code.'"');
        if($check->num_rows == 1) {
            $r = $check->fetch_assoc();
            $ret['result'] = true;
            $ret['cdate'] = $r['created'];
        }
    }
    if($ret['result']) { // найдём, когда же мы записали это направление у себя
        $check = $link->query('SELECT d.lead_code,  r.request_date
            FROM servolab_medrep.request_detail d, servolab_medrep.request r
            where r.id=d.request_id and lower(replace(d.lead_code, "-",  "")) = "'.$lead_code.'"');
        if($check->num_rows == 1) {
            $r = $check->fetch_assoc();
            $ret['reg_date'] = $r['request_date'];
        }
    }
    return $ret;
}
function restore_lead_code($small_code) {
    $tmp = strtoupper($small_code);
    $s = substr($tmp, 0, 2).'-'.substr($tmp, 2, 2).'-'.substr($tmp, 4, 2).'-'.substr($tmp, 6, 2);
    return $s;
}
function get_esculab_price() {
    $myCurl = curl_init();

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => ESCULAB_API_URI.'getPrice',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_USERPWD => ESCULAB_API_LOGIN.':'.ESCULAB_API_PWD,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json",
        "Accept-language: ru_RU"
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    $ret = false;
    if($response) {
        if ($response === false) {
            $response = "getPrice error. HTTP code ". $info['http_code']. ". ".$response;
            if (curl_error($myCurl))
                $response .= " ". curl_error($myCurl);
        } else {
            $obj = json_decode($response);
            if($obj->error) $response = 'JSON is not an object';
            else $ret = $obj;
        }
    }
    curl_close($myCurl);
    return $ret;
}
function find_analiz_in_esculab($idPacket, $price) {
    $ret = false;
    foreach($price as $panel) {
        foreach($panel->childAnalyzes as $analiz) {
            if($analiz->id == $idPacket) {
                $ret = $analiz;
                return $ret;
            }
        }
    }
    return $ret;
}

function get_firebase_token($doc_email, $doc_pwd) {
    $myCurl = curl_init();

    $auth = [
        'email' => $doc_email,
        'password' => $doc_pwd,
        'returnSecureToken' => true
    ];

    $post_data=json_encode($auth);

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key='.FIREBASE_API_KEY,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POST => true,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
        CURLOPT_POSTFIELDS => $post_data
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json"
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    if($response) {
        if ($response === false || $info['http_code'] != 200) {
            $response = "get_firebase_token error. HTTP code ". $info['http_code']. ". ".$response;
            if (curl_error($myCurl))
                $response .= " ". curl_error($myCurl);
            return ['success' => false, 'error' => $response];
        }
    }
    curl_close($myCurl);
    return ['success' => true, 'result' => $response];
}
function find_lead($lead_code, $fb_auth_token) { // надо найти lead_id на основании кода
    $myCurl = curl_init();

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => API_URI.'leads/?create_on_search=1&code='.$lead_code,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json",
        "Authorization: Bearer ".$fb_auth_token
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    curl_close($myCurl);

    $json = json_decode($response);
    if(!is_object($json)) return false;

    return $json;
}
function lead_processing($lead_id, $fb_auth_token, $data) { // и регистрируем продажу
    $myCurl = curl_init();

    $post_data=json_encode($data);

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => API_URI.'leads/'.$lead_id.'/processing/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POST => true,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
        CURLOPT_POSTFIELDS => $post_data
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json",
        "Authorization: Bearer ".$fb_auth_token
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    curl_close($myCurl);

    echo '<p>lead_processing:<br>POST params: '.$post_data.'<br>HTTP code '.$info['http_code'].'<br>'.$response.'</p>';

    if($info['http_code'] == 201) $ret = '<span style="color: darkblue">Ok, '.$response.'</span>';
    elseif($info['http_code'] == 400) {
        $err = json_decode($response);
        $ret = '<span style="color: red">ОШИБКА при регистрации продажи! Код ошибки 400. '.$err->error.': ' . $err->description . '</span>';
    } else $ret = 'HTTP code '.$info['http_code'].'. Response: '.$response;
    return $ret;
}

?>
Done.