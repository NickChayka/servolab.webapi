<?php

if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    // костыль для Ангуляра
    header("HTTP/1.1 204 No content");
    exit;
}

ini_set('display_errors', 'true');
ini_set('max_execution_time','0');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

define('LABORATORY_CODE', 'ND');

require_once('google_mysql_cfg.php');

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

// Идея такая - берём все продажи, для которых не нашли нашего кода и созданных не позже 14 дней назад
// затем по номеру телефона и дате создания пытаемся найти похожую строчку в нашей базе
// если нашлась - условно считаем, что это и есть наше направление, запишем его в БД
// после этого пробуем сравнить стоимость у нас и стоимость в медклауд - если разная, ругаемся почтой
// и в конце всего скрипта отсылаем саммари, если есть что сказать

$now = time();
$c = 1;

$rs = $link->query('select * from servolab_watchdog.medcoud_log where patient_phone!="" and lab="'.LABORATORY_CODE.'" and servolab_code = "" and external_created>="'.date('Y-m-d', $now - 14 * ONE_DAY).'"');
while($r = $rs->fetch_assoc()) {
    $sql = 'select code, user_paid from servolab_views.leads where patient_phone="'.$r['patient_phone'].'" and created between DATE_SUB("'.$r['external_created'].'", INTERVAL 14 DAY) and "'.$r['external_created'].'" order by created';

    $test = $link->query($sql);
    if($test->num_rows > 0) {
        while($lead = $test->fetch_assoc()) {
            // нашли какой-то код направления. Давай теперь глянем, нет ли его у нас уже.
            $tmp = $link->query('select id from servolab_watchdog.medcoud_log where servolab_code = "'.$lead['code'].'"');
            if($tmp->num_rows > 0) {
                continue; // да, такое направление у нас уже есть
            }
            // если пришли сюда - то у нас есть направление для этого телефона, и оно выписано в течении 2-х недель
            // надо проверить его сумму и сравнить с той, что уплочено.



            // потом запишем этот код и двигаем дальше
            $link->query('update servolab_watchdog.medcoud_log set servolab_code = "'.$lead['code'].'", servolab_payed = "'.((int)$lead['user_paid']/100).'" where id='.$r['id']);
        }
    }
}

// теперь отправим письмо с результатами парсинга



/////////////////////////////////////////////////////////////
/*

function detect_lead_code($note) {
    $str = strstr($note, 'Servolab: ');
    if(!$str) return false; // не нашли кода вообще
    $code = trim(strstr($str, ' '));
    if(strlen($code) != 11) return false; // код вроде как и есть, но не в том формате
    return str_replace('-', '', $code);
}
function is_lead_exists($lead_code, $link) {
    $ret = ['result' => false, 'cdate' => ''];
    if(substr($lead_code, 0, 1) == 't') { // это печатное направление, их отдельно чекаем
        $int_code = (int)substr($lead_code, 1);
        $check = $link->query('SELECT id FROM servolab_views.printed_lead_allocation where '.$int_code.'>=right(paper_lead, 7) and '.$int_code.'<(right(paper_lead, 7)+amount)');
        if($check->num_rows > 0) {
            $ret['result'] = true;
        }
    } else { // это электронное направление
        $check = $link->query('SELECT created FROM servolab_views.leads where lower(replace(code,"-","")) = "'.$lead_code.'"');
        if($check->num_rows == 1) {
            $r = $check->fetch_assoc();
            $ret['result'] = true;
            $ret['cdate'] = $r['created'];
        }
    }
    if($ret['result']) { // найдём, когда же мы записали это направление у себя
        $check = $link->query('SELECT d.lead_code,  r.request_date
            FROM servolab_medrep.request_detail d, servolab_medrep.request r
            where r.id=d.request_id and lower(replace(d.lead_code, "-",  "")) = "'.$lead_code.'"');
        if($check->num_rows == 1) {
            $r = $check->fetch_assoc();
            $ret['reg_date'] = $r['request_date'];
        }
    }
    return $ret;
}
function restore_lead_code($small_code) {
    $tmp = strtoupper($small_code);
    $s = substr($tmp, 0, 2).'-'.substr($tmp, 2, 2).'-'.substr($tmp, 4, 2).'-'.substr($tmp, 6, 2);
    return $s;
}
function get_esculab_price() {
    $myCurl = curl_init();

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => ESCULAB_API_URI.'getPrice',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_USERPWD => ESCULAB_API_LOGIN.':'.ESCULAB_API_PWD,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json",
        "Accept-language: ru_RU"
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    $ret = false;
    if($response) {
        if ($response === false) {
            $response = "getPrice error. HTTP code ". $info['http_code']. ". ".$response;
            if (curl_error($myCurl))
                $response .= " ". curl_error($myCurl);
        } else {
            $obj = json_decode($response);
            if($obj->error) $response = 'JSON is not an object';
            else $ret = $obj;
        }
    }
    curl_close($myCurl);
    return $ret;
}
function find_analiz_in_esculab($idPacket, $price) {
    $ret = false;
    foreach($price as $panel) {
        foreach($panel->childAnalyzes as $analiz) {
            if($analiz->id == $idPacket) {
                $ret = $analiz;
                return $ret;
            }
        }
    }
    return $ret;
}

function get_firebase_token($doc_email, $doc_pwd) {
    $myCurl = curl_init();

    $auth = [
        'email' => $doc_email,
        'password' => $doc_pwd,
        'returnSecureToken' => true
    ];

    $post_data=json_encode($auth);

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key='.FIREBASE_API_KEY,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POST => true,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
        CURLOPT_POSTFIELDS => $post_data
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json"
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    if($response) {
        if ($response === false || $info['http_code'] != 200) {
            $response = "get_firebase_token error. HTTP code ". $info['http_code']. ". ".$response;
            if (curl_error($myCurl))
                $response .= " ". curl_error($myCurl);
            return ['success' => false, 'error' => $response];
        }
    }
    curl_close($myCurl);
    return ['success' => true, 'result' => $response];
}
function find_lead($lead_code, $fb_auth_token) { // надо найти lead_id на основании кода
    $myCurl = curl_init();

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => API_URI.'leads/?create_on_search=1&code='.$lead_code,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json",
        "Authorization: Bearer ".$fb_auth_token
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    curl_close($myCurl);

    $json = json_decode($response);
    if(!is_object($json)) return false;

    return $json;
}
function lead_processing($lead_id, $fb_auth_token, $data) { // и регистрируем продажу
    $myCurl = curl_init();

    $post_data=json_encode($data);

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => API_URI.'leads/'.$lead_id.'/processing/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POST => true,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
        CURLOPT_POSTFIELDS => $post_data
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json",
        "Authorization: Bearer ".$fb_auth_token
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    curl_close($myCurl);

    echo '<p>lead_processing:<br>POST params: '.$post_data.'<br>HTTP code '.$info['http_code'].'<br>'.$response.'</p>';

    if($info['http_code'] == 201) $ret = '<span style="color: darkblue">Ok, '.$response.'</span>';
    elseif($info['http_code'] == 400) {
        $err = json_decode($response);
        $ret = '<span style="color: red">ОШИБКА при регистрации продажи! Код ошибки 400. '.$err->error.': ' . $err->description . '</span>';
    } else $ret = 'HTTP code '.$info['http_code'].'. Response: '.$response;
    return $ret;
}
*/
?>
Done.