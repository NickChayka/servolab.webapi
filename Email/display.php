<pre>
<?php

require_once('google_mysql_cfg.php');

//////////////////////////////////////////////////////////
function clean_lead_code($code) {
    $ret = $code;
    $patterns = $replacements = [];
    $patterns[] = '/-/'; $replacements[] = '';
    $patterns[] = '/т/'; $replacements[] = 't';
    $patterns[] = '/д/'; $replacements[] = 'd';
    $patterns[] = '/а/'; $replacements[] = 'a';
    $patterns[] = '/м/'; $replacements[] = 'm';
    $patterns[] = '/к/'; $replacements[] = 'k';
    $ret = preg_replace($patterns, $replacements, $ret);
    return $ret;
}
//////////////////////////////////////////////////////////
$id = (int)$_GET['id'];
if($id < 1) $id = 1;

$rs = $link->query('select * from servolab_watchdog.dna_attachments where id = '.$id);
$r = $rs->fetch_assoc();

$pretty = [];
$rows = unserialize($r['parsed_body']);

foreach($rows as $row) {
    if((int)$row[1] == 0) continue; // это похоже просто заголовки

    $days = $row['2'];
    $date = new DateTime('1899-12-30');
    $date->add(new DateInterval("P{$days}D"));

    list($span, $time) = explode(' ', $row[3]);

    $lead_code = clean_lead_code(mb_strtolower($row[4]));

    $pretty[$lead_code][$row[0]][] = [
        'date' => $date->format('Y-m-d').' '.$time,
        'analiz_id' => $row[1],
        'analiz_code' => $row[5],
        'analiz_name' => $row[6],
        'price' => $row[7],
    ];

}

print_r($pretty);

//print_r($rows);

?>
</pre>
Done
