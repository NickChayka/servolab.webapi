<?php

define('MEDCLOUD_API_URI', 'https://crm.newdiagnostics.ua/medcloud-api/');
define('MEDCLOUD_API_LOGIN', 'servolab@medcloud.ua');
define('MEDCLOUD_API_PWD', 'j2656972');


function getToken() {
    $data = [
        "email" => MEDCLOUD_API_LOGIN,
        "password" => MEDCLOUD_API_PWD
    ];
    $myCurl = curl_init();

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => MEDCLOUD_API_URI.'auth/login',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_POSTFIELDS => json_encode($data)
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
        "Content-Type: application/json"
    ));

    $response = curl_exec($myCurl);
    curl_close($myCurl);
    $json = json_decode($response);
    if(is_object($json) and $json->accessToken != '') {
        $ret = trim($json->accessToken);
    } else {
        $ret = false;
    }
    return $ret;
}