<?php

ini_set('display_errors', 1);
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('max_execution_time','0');

require_once 'cfg.php';

$f = "2021-01-01";
$t = "2021-08-02";

if($f == '') $f = date('Y-m-01');
if($t == '') $t = date('Y-m-d');

list($y, $m, $d) = explode('-', $f);
$ft = mktime(0, 0, 0, $m, $d, $y );

list($y, $m, $d) = explode('-', $t);
$tt = mktime(23, 59, 59, $m, $d, $y );

$ONE_DAY = 24*3600;
$ONE_WEEK = 7*$ONE_DAY;

$token = getToken();
if(!$token) {
    echo 'не смог получить токен доступа';
    die();
}

for($i = $ft; $i < $tt; $i += $ONE_WEEK+$ONE_DAY) {
    $e = $i+$ONE_WEEK;
    if($e > $tt) $e = $tt;
    echo date('Y-m-d\TH:i:sP', $i).' => '.date('Y-m-d\TH:i:sP', $e).'<br>';

    $from = date('Y-m-d\TH:i:sP', $i);
    $to = date('Y-m-d\TH:i:sP', $e);

    $myCurl = curl_init();

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => MEDCLOUD_API_URI.'v1/order?createdAtFrom='.urlencode($from).'&createdAtTo='.urlencode($to),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYHOST => 0
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
        "Authorization: Bearer ".$token,
        "Content-Type: application/json"
    ));

    $response = curl_exec($myCurl);
    curl_close($myCurl);
//    $json = json_decode($response);

    $fp = fopen('/home/servolab/webapi/www/MedCloud/orders_'.date('Y-m-d', $i).'_'.date('Y-m-d', $e).'.json','w');
    fwrite($fp, $response);
    fclose($fp);

}




?>
<p>Ok
