<?php

ini_set('display_errors', 1);
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('max_execution_time','0');

require_once 'cfg.php';

if(!defined('DB_VIEW_HOST')) define('DB_VIEW_HOST', 'servolab-product-do-user-2754665-0.b.db.ondigitalocean.com:25060');
if(!defined('DB_VIEW_USERNAME')) define('DB_VIEW_USERNAME', 'webapi');
if(!defined('DB_VIEW_PASSWORD')) define('DB_VIEW_PASSWORD', 'cTkERhuYM3pe97NW');
if(!defined('DB_VIEW_NAME')) define('DB_VIEW_NAME', 'servolab_views');

$link = mysqli_connect(DB_VIEW_HOST, DB_VIEW_USERNAME, DB_VIEW_PASSWORD, DB_VIEW_NAME);

/* check connection */
if (mysqli_connect_errno()) {
    throw new RuntimeException(mysqli_connect_error(), 500);
} else {
    $link->query('set names utf8mb4');
    $link->query('SET time_zone = "Europe/Kiev"');
}

$token = getToken();
if(!$token) {
    echo 'не смог получить токен доступа';
    die();
}

$lead_code = '4T-69-9A-61';
// найдём данные по этому направлению
$rs = $link->query('SELECT distinct  l.service_id, p.company_custom_services_id
    FROM servolab_views.leads_light l left join servolab_views.v_pos_service_price p on (p.service_id=l.service_id and p.company_id=50017)
    where l.lead_code="'.$lead_code.'"');

$tests = [];
while($r = $rs->fetch_assoc())
    $tests[] = [
        "code" => $r['company_custom_services_id'],
        "quantity" => 1
    ];

$rs = $link->query('select * from servolab_medrep.request_detail where lead_code="'.$lead_code.'"');
$lead_info = $rs->fetch_assoc();
$cmt = "Тестовый заказ. Servolab: ".$lead_code;
if($lead_info['pos_address'] != '') $cmt.=', адрес: '.$lead_info['pos_address'];
if($lead_info['planned_date'] != '') $cmt.=', дата визита: '.$lead_info['planned_date'];
if($lead_info['planned_time'] != '') $cmt.=', время визита: '.$lead_info['planned_time'];

$data = [
    "name" => "Test",
    "surname" => "Test",
    "patronymic" => "Please ignore",
    "birthday" => null,
    "age" => null,
    "male" => "man",
    "phone" => "+380990899385",
    "email" => null,
    "state" => "created",
    "comments" => $cmt,
    "items" => [
        "tests" => $tests,
        "materialCollectionServices" => null,
        "services" => null,
        "products" => null
    ],
    //"discountPromoCodeId" => 0,
    "additionalData" => []
];

//"{
//    "name": string/null - Ім'я клієнту
//    "surname": string/null  - Прізвище клієнту
//    "patronymic": string/null  - По батькові клієнту
//    "birthday": string (ISO 8601)/null - дата нарождення клієнту
//    "age": integer/null - вік клієнту
//    "male": string (man, woman)/null - стать клієнту
//    "phone": string/null - телефон клієнту
//    "email": string/null - E-mail адреса клієнту
//    "state": string/null - статус замовлення (можливі варіанти: created (створен), cart_confirmed (замовлення підтверджено)). Тільки підтверджені замовлення можуть бути оплачені
//    "comments": string/null - Коментарі
//    "items": {
//        "tests": [{ - перелік аналізів
//                        code: string, - код аналізу
//                        quantity: int/null, - кількість
//        }],
//        "materialCollectionServices": [{ - перелік послуг забору матеріалу
//                        code: string, - код аналізу
//                        quantity: int/null, - кількість
//        }],
//        "services": [{ - перелік послуг
//                        code: string, - код послуги
//                        quantity: int/null, - кількість
//        }],
//        "products": array - перелік товарів
//    },
//    "discountPromoCodeId": integer - ID промо-коду    
//    "additionalData": array - можлива додаткова інформація  { 
//         "language": string - мова 
//        "arrivalType": string - тип рейсу
//     },
//}"



$myCurl = curl_init();

curl_setopt_array($myCurl, array(
    CURLOPT_URL => MEDCLOUD_API_URI.'v1/order',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POST => true,
    CURLOPT_SSL_VERIFYHOST => 0,
    CURLOPT_POSTFIELDS => json_encode($data)
));

curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
    "User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
    "Authorization: Bearer ".$token,
    "Content-Type: application/json"
));

$response = curl_exec($myCurl);
curl_close($myCurl);
$json = json_decode($response);

echo '<pre>';
print_r($json);
echo '</pre>';