<?php

ini_set('display_errors', 1);
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('max_execution_time','0');

require_once 'cfg.php';

$token = getToken();
if(!$token) {
    echo 'не смог получить токен доступа';
    die();
}

// для начала получим список категорий:
$myCurl = curl_init();

curl_setopt_array($myCurl, array(
    CURLOPT_URL => MEDCLOUD_API_URI.'v1/test-categories',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYHOST => 0
));

curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
    "User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
    "Authorization: Bearer ".$token,
    "Content-Type: application/json"
));

$response = curl_exec($myCurl);
curl_close($myCurl);
$json = json_decode($response);

if(is_array($json)) {
    echo '<form method="get">';
    echo 'Select Category:';
    echo '<select name="priceListId">';
    foreach($json as $category) {
        echo '<option value="'.$category->id.'">'.$category->name.' ('.$category->code.')'."\n";
    }
    echo '</select>';
    echo '<input type="submit" value="Получить">';
    echo '</form>';
} else die('Не смог получить список категорий');

// Чудесно. А теперь если у нас есть параметр ID прайса - запросим и сам прайс
if((int)$_GET['priceListId'] > 0) {
    $myCurl = curl_init();

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => MEDCLOUD_API_URI.'v1/grouped-price-list-tests?priceListId='.$_GET['priceListId'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYHOST => 0
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
        "Authorization: Bearer ".$token,
        "Content-Type: application/json"
    ));

    $response = curl_exec($myCurl);
    curl_close($myCurl);
    $json = json_decode($response);

    if(is_array($json)) {
        echo '<hr>';
        echo '<pre>';
        print_r($json);
        echo '</pre>';
    } else die('Не смог получить прайс для категории "'.$_GET['priceListId'].'"');
}