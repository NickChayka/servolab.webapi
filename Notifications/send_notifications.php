<?php
    ini_set('display_errors', 'true');
echo "<br>started - ".date('Y-m-d H:i:s');
$fp=fopen('send_notifications.pi','a') or die("Can't create send_notifications.pi");
if(!flock($fp, LOCK_EX | LOCK_NB)) echo ('Program already running');
else {
ftruncate($fp,0);
fputs($fp, date("d-m-Y: H:i:s")." RPC daemon started");
//********************************************************************
    ini_set('max_execution_time','0');
    ini_set('display_errors', 'true');
    require_once("google_mysql_cfg.php");
////////////////////////////////////////////////
    $bot = [];
    $rs = $link->query('select * from bot_api where status = 1');
    while($r = $rs->fetch_assoc()) {
        $bot[$r['bot_type']][$r['method']] = [
            'api_key' => $r['api_key'],
            'api_uri' => $r['api_uri']
        ];
    }
    $rs->close();

    $sql = 'SELECT * FROM notification where delayed_to<'.time().' and status = 0 ';
    $rs = $link->query($sql);
    while($r = $rs->fetch_assoc()) {
        $action = $bot[$r['bot_type']][$r['method']];
        if($r['method'] == 'viber') { // пока обрабатываем только вайбер
            $uri = trim($action['api_uri']);
            if($uri != '') {
                echo $uri.'?user_id='.urlencode(base64_encode($r['receiver'])).'&msg='.urlencode(base64_encode($r['message'])).'<br>';
//                $str = file_get_contents($uri.'?user_id='.urlencode(base64_encode($r['receiver'])).'&msg='.urlencode(base64_encode($r['message'])));


                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $uri);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,
                    'user_id='.urlencode(base64_encode($r['receiver'])).'&msg='.urlencode(base64_encode($r['message'])));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $str = curl_exec($ch);
                curl_close ($ch);

                $success = -1;
                $json = json_decode($str);
                if(is_object($json) and $json->success) $success = 1;
                $link->query('update notification set status = '.$success.', result = "'.$link->escape_string($str).'" where id='.$r['id']);
            }
        }
    }
    $rs->close();
    $link->close();
///////////////////////////////////////////////////
    flock($fp, LOCK_UN);
}
fclose($fp);
echo "<br>finished - ".date('Y-m-d H:i:s');

?>
Done.