<?php
ini_set('display_errors', 'true');
require_once 'lib.php';

// получим список лабораторий в Эскулабе
$myCurl = curl_init();

curl_setopt_array($myCurl, array(
    CURLOPT_URL => ESCULAB_API_URI.'getPrice',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_USERPWD => ESCULAB_API_LOGIN.':'.ESCULAB_API_PWD,
    CURLOPT_SSL_VERIFYHOST => 0,
    CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1
));

curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/json",
    "Accept: application/json",
    "Accept-language: ru_RU"
));

$response = curl_exec($myCurl);
$info = curl_getinfo($myCurl);

if($response) {
    if ($response === false) {
        $response = "getPrice error. HTTP code ". $info['http_code']. ". ".$response;
        if (curl_error($myCurl))
            $response .= " ". curl_error($myCurl);
        $json = [
            "error" => $response
        ];
    } else {
        $json = json_decode($response);
        //var_dump($json);
        $items = $manipulation = [];
        foreach($json as $panel) {
            foreach($panel->childAnalyzes as $analiz) {
                //            $p = strpos($pos->address, ',');
//            if($p === false) {
//                $name = '???';
//                $city = '???';
//            } else {
//                $city = trim(substr($pos->address, 0, $p));
//                $name = trim(substr($pos->address, $p+1));
//            }
//            $search  = array(' ', '(', ')', '+');
//            $replace = array('', '', '', '');
//            $phone = str_replace($search, $replace, $pos->phone);
//
            $i = [
                'id' => str_pad($analiz->id, 32, "0", STR_PAD_LEFT),
                'subdivision_id' => "UUID лаборатории, где могут забирать этот анализ",
                'code' => $analiz->id,
                'name' => [
                    'UKR' => $analiz->name
                ],
                'category_parent_id' => $panel->idParent,
                'category_id' => $panel->id,
                'category_name' => [
                    'UKR' => $panel->name
                ],
                'base_price' => 100*$analiz->price,
                'price' => 100*$analiz->price,
                'description' => $analiz->duration_day,
                'days_from_get' => $analiz->duration_day,
                'is_active' => 1
            ];
            if($analiz->take != '') {
                $tarr = explode(',', $analiz->take);
                foreach($tarr as $take) {
                    $manipulation[$take][] = $analiz->id;
                    $i['manipulation_ids'][] = [
                        'id' => $take,
                        'name' => ['UKR' => '']
                    ];
                }

            }
            $items[$analiz->id] = $i;
            unset($i);

            }
        }
        // первая постобработка, добавим тип анализа и при необходимости манипуляцию
        $manipulations = array_keys($manipulation);
        foreach($items as $key => $item) {
            if(in_array($key, $manipulations)) { // это манипуляция
                $items[$key]['type'] = 'MANIPULATION';
            } else { // это простой анализ
                $items[$key]['type'] = 'TEST';
            }
            if(count($item['manipulation_ids']) > 0) {
                for($i=0; $i<count($item['manipulation_ids']); $i++) {
                    $items[$key]['manipulation_ids'][$i]['name'] = $items[$item['manipulation_ids'][$i]['id']]['name'];
                }

            }
        }
        // очистка данных
        $items_final = [];
        foreach($items as $item) $items_final[] = $item;

        $res = [
            'org_id' => 'e1c11ab0000000000000000000000002',
            'items' => $items_final
        ];
        var_dump($res);

//        $fb_token = get_firebase_token(FIREBASE_LOGIN_ESCULAB, FIREBASE_PWD_ESCULAB);
//        if($fb_token['success']) {
//            $_token = $fb_token['result'];
//            $answer = make_api_call(API_URI_SERVICES, $res, $_token);
//            if($answer['success']) {
//                echo 'Экспорт успешно завершился. Результат: '.$answer['result'];
//            } else {
//                echo 'Ошибка экспорта: '.$answer['error'];
//            }
//        } else {
//            echo 'Ошибка при авторизации на Firebase: '.$fb_token['error'];
//        }

    }

}
curl_close($myCurl);
