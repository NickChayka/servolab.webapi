<?php

require_once 'cfg.php';

function get_firebase_token($email, $pwd) {
    $myCurl = curl_init();

    $auth = [
        'email' => $email,
        'password' => $pwd,
        'returnSecureToken' => true
    ];

    $post_data=json_encode($auth);

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key='.FIREBASE_API_KEY,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POST => true,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
        CURLOPT_POSTFIELDS => $post_data
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json"
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    if($response) {
        if ($response === false || $info['http_code'] != 200) {
            $response = "get_firebase_token error. HTTP code ". $info['http_code']. ". ".$response;
            if (curl_error($myCurl))
                $response .= " ". curl_error($myCurl);
            return ['success' => false, 'error' => $response];
        }
    }
    curl_close($myCurl);
    $json = json_decode($response);
    return ['success' => true, 'result' => $json->idToken];
}

function make_api_call($uri, $post_data, $token) {

    $myCurl = curl_init();

    curl_setopt_array($myCurl, array(
        CURLOPT_URL => $uri,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POST => true,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
        CURLOPT_POSTFIELDS => json_encode($post_data)
    ));

    curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Accept: application/json",
        "Accept-language: ru_RU",
        "Authorization: Bearer ".$token
    ));

    $response = curl_exec($myCurl);
    $info = curl_getinfo($myCurl);

    if($response) {
        if ($response === false) {
            $response = "create_lead error. HTTP code ". $info['http_code']. ". ".$response;
            if (curl_error($myCurl))
                $response .= " ". curl_error($myCurl);
            return ['success' => false, 'error' => $response];
        } elseif($info['http_code'] != 200 and $info['http_code'] != 201 and $info['http_code'] != 204) {
            return ['success' => false, 'error' => $response];
        }
    }
    curl_close($myCurl);
    return ['success' => true, 'result' => $response];
}