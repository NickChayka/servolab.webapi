<?php
ini_set('display_errors', 'true');
require_once 'lib.php';

// получим список лабораторий в Эскулабе
$myCurl = curl_init();

curl_setopt_array($myCurl, array(
    CURLOPT_URL => ESCULAB_API_URI.'getPunkts',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_USERPWD => ESCULAB_API_LOGIN.':'.ESCULAB_API_PWD,
    CURLOPT_SSL_VERIFYHOST => 0,
    CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1
));

curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/json",
    "Accept: application/json",
    "Accept-language: ru_RU"
));

$response = curl_exec($myCurl);
$info = curl_getinfo($myCurl);

if($response) {
    if ($response === false) {
        $response = "getPunkts error. HTTP code ". $info['http_code']. ". ".$response;
        if (curl_error($myCurl))
            $response .= " ". curl_error($myCurl);
        $json = [
            "error" => $response
        ];
    } else {
        $json = json_decode($response);
        $items = [];
        foreach($json as $pos) {
            $p = strpos($pos->address, ',');
            if($p === false) {
                $name = '???';
                $city = '???';
            } else {
                $city = trim(substr($pos->address, 0, $p));
                $name = trim(substr($pos->address, $p+1));
            }
            $search  = array(' ', '(', ')', '+');
            $replace = array('', '', '', '');
            $phone = str_replace($search, $replace, $pos->phone);

            $i = [
                'id' => str_pad($pos->idPunkt, 32, "0", STR_PAD_LEFT),
                'name' => trim($name),
                'code_name' => $pos->idPunkt,
                'city' => $city,
                'address' => $pos->address
            ];
            if($phone != '') $i['phone'] = '+38'.$phone;
            if(trim($pos->email) != '') $i['email'] = trim($pos->email);

            $items[] = $i;
            unset($i);
        }
        $res = [
            'org_id' => 'e1c11ab0000000000000000000000001',
            'items' => $items
        ];

//        var_dump($res);

        $fb_token = get_firebase_token(FIREBASE_LOGIN_ESCULAB, FIREBASE_PWD_ESCULAB);
        if($fb_token['success']) {
            $_token = $fb_token['result'];
            $answer = make_api_call(API_URI_POS, $res, $_token);
            if($answer['success']) {
                echo 'Экспорт успешно завершился. Результат: '.$answer['result'];
            } else {
                echo 'Ошибка экспорта: '.$answer['error'];
            }
        } else {
            echo 'Ошибка при авторизации на Firebase: '.$fb_token['error'];
        }

    }

}
curl_close($myCurl);
