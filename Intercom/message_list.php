<?php

require "vendor/autoload.php";

use Intercom\IntercomClient;
$client = new IntercomClient('dG9rOjZlOTc4ODRlXzdmOTZfNGYxMF9iYjE3XzRlNDcyMmY5NTdhMzoxOjA=');

$res = $client->conversations->getConversations([
    "type" => "admin",
    "admin_id" => "1395979"
]);

//var_dump($res);

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Language" content="ru">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title>Выберите сообщение</title>
</head>

<body bgcolor="#ffffff" alink="#000000" link="#000000" vlink="#000000">

<h3>Выберите сообщение</h3>

<form action="send_msg.php" method="post">
    <?
        $conversations = $res->conversations;
        $cids = [];
        foreach($conversations as $conversation) {
            $cids[] = $conversation->id;?>
            <input type="radio" id="rcid" name="rcid" value="<?=$conversation->id?>" onclick='show_body(<?=$conversation->id?>); return false'><a href="#" onclick='show_body(<?=$conversation->id?>); return false'><?=$conversation->id?> от <?=date('Y-m-d H:i:s', $conversation->created_at)?></a>
            <div id="l_<?=$conversation->id?>" style="display: none">
                <?=$conversation->conversation_message->body?>
            </div><br>
        <?}
    ?>
    <p><input type="submit" value="Отправить выбранное сообщение для Кости"></p>
</form>
<script language="JavaScript">
    var cids = [<?=join(',', $cids)?>];
    function show_body(id) {
        for(var i=0; i<cids.length; i++) {
            var o = document.getElementById('l_'+cids[i]);
            if(cids[i]==id) o.style.display = '';
            else o.style.display = 'none';
        }
        var c = document.getElementsByName('rcid');
        for(var i=0; i<c.length; i++) {
            if(c[i].value == id) {
                c[i].checked = true;
                break;
            }
        }
    }
    show_body(cids[0]);
</script>

</body></html>