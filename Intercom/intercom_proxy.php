<?php

header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, servolab, servolab_lat, servolab_lon");
header("access-control-expose-headers: Total-Items");
header("access-control-allow-methods: GET, POST, OPTIONS, PUT, DELETE");
header("content-type: application/json");

if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    // костыль для Ангуляра
    header("HTTP/1.1 204 No content");
    exit;
}

define('INTERCOM_TOKEN','dG9rOjZlOTc4ODRlXzdmOTZfNGYxMF9iYjE3XzRlNDcyMmY5NTdhMzoxOjA=');
define('INTERCOM_API_URI','https://api.intercom.io/');

$method = $_POST['method'];

$myCurl = curl_init();

$url=INTERCOM_API_URI.$method;

$search  = array(" ", "\r", "\n");
$replace = array("_", "-", "-");

$url=str_replace($search, $replace, $url);

curl_setopt_array($myCurl, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
//    CURLOPT_POST => true,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_SSL_VERIFYHOST => 0,
    CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
//    CURLOPT_POSTFIELDS => $data
));

curl_setopt($myCurl, CURLOPT_HTTPHEADER, array(
    "Accept: application/json",
    "Content-type: application/json",
    "Authorization: Bearer ".INTERCOM_TOKEN
));

$response = curl_exec($myCurl);
$info = curl_getinfo($myCurl);

echo $response;

?>