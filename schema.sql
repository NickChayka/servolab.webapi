-- ----------------------------------------------------------
-- MDB Tools - A library for reading MS Access database files
-- Copyright (C) 2000-2011 Brian Bruns and others.
-- Files in libmdb are licensed under LGPL and the utilities under
-- the GPL, see COPYING.LIB and COPYING files respectively.
-- Check out http://mdbtools.sourceforge.net
-- ----------------------------------------------------------

-- That file uses encoding UTF-8

CREATE TABLE [Gem]
 (
	[Дослід]			Text (100), 
	[Дата]			DateTime, 
	[Код паціента]			Long Integer, 
	[Еозінофіли]			Text (100), 
	[Базофіли]			Text (100), 
	[Юні]			Text (100), 
	[Палочки]			Text (100), 
	[Сегменти]			Text (100), 
	[СОЭ]			Text (100), 
	[ретикулоцити]			Text (100), 
	[коментар]			Text (500), 
	[WBC]			Text (14), 
	[RBC]			Text (14), 
	[HGB]			Text (14), 
	[HCT]			Text (14), 
	[MCV]			Text (14), 
	[MCH]			Text (14), 
	[MCHC]			Text (14), 
	[RDW]			Text (14), 
	[PLT]			Text (14), 
	[MPV]			Text (14), 
	[THT]			Text (14), 
	[PDW]			Text (14), 
	[LYMP_PRC]			Text (14), 
	[MONO_PRC]			Text (14), 
	[GRAN_PRC]			Text (14), 
	[LYNP_NUM]			Text (14), 
	[MONO_NUM]			Text (14), 
	[GRAN_NUM]			Text (14), 
	[FLAG_WBC]			Text (24), 
	[FLAG_PLT]			Text (12)
);

CREATE TABLE [GrAn]
 (
	[GroupID]			Long Integer, 
	[Kod]			Long Integer
);

CREATE TABLE [Groups]
 (
	[GroupID]			Long Integer, 
	[Name]			Text (100)
);

CREATE TABLE [Алергия4]
 (
	[калибратор]			Single, 
	[проба]			Single, 
	[едениц калибратора]			Single, 
	[коментарий]			Text (500), 
	[счетчик]			Long Integer, 
	[код паціента]			Long Integer, 
	[D pteron]			Text (100), 
	[D farinae]			Text (100), 
	[домашняя пыль]			Text (100), 
	[шерсть собаки]			Text (100), 
	[гусь(перо)]			Text (100), 
	[курица (перо)]			Text (100), 
	[утка(перо)]			Text (100), 
	[шерсть кошки]			Text (100)
);

CREATE TABLE [Алергиябактериальна]
 (
	[калибратор]			Single, 
	[проба]			Single, 
	[едениц калибратора]			Single, 
	[коментарий]			Text (500), 
	[счетчик]			Long Integer, 
	[код паціента]			Long Integer, 
	[Staph aureus]			Text (100), 
	[Staph epidermidis]			Text (100), 
	[Strept pneumoniae]			Text (100), 
	[Strept pyogenes]			Text (100), 
	[Strept faecalis]			Text (100), 
	[E colli]			Text (100), 
	[Pseudomonas aurugenosa]			Text (100), 
	[Corinebacteria pseudodifteriy]			Text (100)
);

CREATE TABLE [Алергиягрибкова]
 (
	[калибратор]			Single, 
	[проба]			Single, 
	[едениц калибратора]			Single, 
	[коментарий]			Text (500), 
	[счетчик]			Long Integer, 
	[код паціента]			Long Integer, 
	[Candida utilis]			Text (100), 
	[Candida albicans]			Text (100), 
	[Penicillium tardum]			Text (100), 
	[Aspergillus flavus]			Text (100), 
	[Aspergillus niger]			Text (100), 
	[Rhizopus nigricans]			Text (100), 
	[Fuzarium oxysporum]			Text (100), 
	[Cladosporum herbarum]			Text (100)
);

CREATE TABLE [Аналізи]
 (
	[Код аналіза]			Long Integer, 
	[Аналіз]			Text (510) NOT NULL, 
	[Ціна]			Single, 
	[Друк]			Boolean NOT NULL, 
	[Норма]			Text (400), 
	[Хронічна]			Text (400), 
	[Вибір в прайс]			Boolean NOT NULL, 
	[Строк]			Text (100), 
	[Вибір на звіт]			Boolean NOT NULL, 
	[Группа]			Text (150), 
	[К+]			Single, 
	[К-]			Single, 
	[ТО]			Single, 
	[Эталон]			Single, 
	[Запомнить]			Text (500), 
	[Служебний]			Boolean NOT NULL, 
	[исполнитель]			Text (100), 
	[група інша]			Text (100), 
	[исключение]			Boolean NOT NULL, 
	[Вивод кода]			Boolean NOT NULL, 
	[Аксесс]			Boolean NOT NULL, 
	[холодильник]			Long Integer, 
	[распечатка]			Long Integer, 
	[производитель]			Text (100), 
	[кодпроизводителя]			Long Integer, 
	[запасной]			Long Integer, 
	[запасной1]			Text (100), 
	[себестоим]			Double, 
	[реестрация]			Boolean NOT NULL, 
	[Норма детская]			Text (400)
);

CREATE TABLE [Бакексель]
 (
	[дата]			DateTime, 
	[Дослід]			Double, 
	[ПІБ]			Text (510), 
	[матер#]			Text (510), 
	[F8]			Text (510), 
	[ст#обсем]			Text (510), 
	[амоксіцилін]			Text (510), 
	[моксіфлоксацин]			Text (510), 
	[офлоксацин]			Text (510), 
	[ципрофлоксацин]			Text (510), 
	[левофлоксацин]			Text (510), 
	[цефтріаксон]			Text (510), 
	[цефтізоксім(зоксіцеф)]			Text (510), 
	[цефподоксим]			Text (510), 
	[гентаміцин]			Text (510), 
	[доксіциклин]			Text (510), 
	[азітроміцин]			Text (510), 
	[роксітроміцин]			Text (510), 
	[кларітроміцин]			Text (510), 
	[метронідазол]			Text (510), 
	[флюконазол]			Text (510), 
	[ністатин]			Text (510), 
	[нізорал]			Text (510), 
	[клотрімазол]			Text (510), 
	[ітраконазол]			Text (510)
);

CREATE TABLE [Гематологія]
 (
	[Код паціента]			Long Integer, 
	[Еритроцити]			Text (100), 
	[Гемоглобин]			Text (100), 
	[Гематокріт]			Text (100), 
	[Тромбоцити]			Text (100), 
	[Еозінофіли]			Text (100), 
	[Базофіли]			Text (100), 
	[Лейкоцити]			Text (100), 
	[Гранулоцити]			Text (100), 
	[Моноцити]			Text (100), 
	[Лімфоцити]			Text (100), 
	[Юні]			Text (100), 
	[Палочки]			Text (100), 
	[Сегменти]			Text (100), 
	[СОЭ]			Text (100), 
	[Група крові]			Text (100), 
	[Резус фактор]			Text (100), 
	[ретикулоцити]			Text (100), 
	[мієлоцити]			Text (100), 
	[метамієлоцити]			Text (100), 
	[анізоцитоз]			Text (100), 
	[пойкілоцитоз]			Text (100), 
	[поліхромофілія]			Text (100), 
	[бласти]			Text (100), 
	[мегабласти]			Text (100), 
	[гіперсегменти]			Text (100), 
	[target]			Text (100), 
	[sphero]			Text (100), 
	[anizo]			Text (100), 
	[micro]			Text (100), 
	[macro]			Text (100), 
	[sickle cells]			Text (100), 
	[baso stip]			Text (100), 
	[vacuoles]			Text (100), 
	[коментар]			Text (500), 
	[счетчик]			Long Integer, 
	[Дата]			DateTime, 
	[средний обьем эритроцита]			Text (100), 
	[среднее содержание гемоглобина в 1эритроците]			Text (100), 
	[средняя концентрация гемоглобина в эритроцитах]			Text (100), 
	[средний обьем тромбоцитов]			Text (100), 
	[распределение тромбоцитов по обьему]			Text (100), 
	[распределение эритроцитов по обьему]			Text (100), 
	[тромбокрит]			Text (100), 
	[%лимфоцитов]			Text (100), 
	[%моноцитов]			Text (100), 
	[%гранулоцитов]			Text (100), 
	[дослід]			Text (100)
);

CREATE TABLE [Закази]
 (
	[Код паціента]			Long Integer, 
	[Код аналіза]			Long Integer, 
	[Результат]			Text (500), 
	[вимір]			Single, 
	[кодстарий]			Long Integer
);

CREATE TABLE [Зірка якості]
 (
	[Логотип]			OLE (255), 
	[Реквизиты]			Text (500), 
	[Реклама]			Text (400)
);

CREATE TABLE [Лікарі]
 (
	[Лікар]			Text (500), 
	[Код лікаря]			Long Integer, 
	[Співробітники]			Text (500), 
	[Тел]			Text (500), 
	[Адрес]			Text (500), 
	[Угода]			Boolean NOT NULL, 
	[Активність]			Long Integer, 
	[Процент]			Double, 
	[к]			Single, 
	[платня]			Boolean NOT NULL, 
	[Діаграма]			Boolean NOT NULL, 
	[Безнал]			Boolean NOT NULL, 
	[Діаграма фірми]			Boolean NOT NULL, 
	[Менеджер]			Text (100), 
	[Пункт Мишуги]			Boolean NOT NULL, 
	[Инфекционка]			Boolean NOT NULL, 
	[час роботи]			Text (300), 
	[місце роботи]			Text (500), 
	[статистика]			Boolean NOT NULL, 
	[Дата ввода]			DateTime, 
	[свой прайс]			Boolean NOT NULL, 
	[вид]			Boolean NOT NULL, 
	[ка]			Single, 
	[Алик]			Boolean NOT NULL, 
	[ковбаса]			Boolean NOT NULL, 
	[район]			Text (300), 
	[четний]			Boolean NOT NULL, 
	[нечетний]			Boolean NOT NULL, 
	[email]			Text (100), 
	[назва]			Text (500)
);

CREATE TABLE [Лікарі-лаборанти]
 (
	[код]			Long Integer, 
	[Лікарі-лаборанти]			Text (200)
);

CREATE TABLE [Логотип]
 (
	[Логотип]			OLE (255), 
	[Реквизиты]			Text (500), 
	[Реклама]			Text (400)
);

CREATE TABLE [Мазки]
 (
	[Код паціента]			Long Integer, 
	[Дата]			DateTime, 
	[Код заказа]			Long Integer, 
	[лейкоциты]			Text (200), 
	[лейкоцитыцер]			Text (200), 
	[лейкоцитываг]			Text (200), 
	[эритроциты]			Text (100), 
	[эритроцитыцер]			Text (100), 
	[эритроцитываг]			Text (100), 
	[палочки]			Text (200), 
	[палочкицер]			Text (200), 
	[палочкиваг]			Text (200), 
	[кокки]			Text (200), 
	[коккицер]			Text (200), 
	[коккиваг]			Text (200), 
	[диплококки]			Text (100), 
	[диплококкицер]			Text (100), 
	[диплококкиваг]			Text (100), 
	[ключевые клетки]			Text (100), 
	[ключевые клеткицер]			Text (100), 
	[ключевые клеткиваг]			Text (100), 
	[патогенные грибы]			Text (200), 
	[патогенные грибыцер]			Text (200), 
	[патогенные грибываг]			Text (200), 
	[плоский эпителий]			Text (200), 
	[плоский эпителийцер]			Text (200), 
	[плоский эпителийваг]			Text (200), 
	[цилиндрический эпителий]			Text (100), 
	[цилиндрический эпителийцер]			Text (100), 
	[цилиндрический эпителийваг]			Text (100), 
	[GN]			Text (100), 
	[GNцер]			Text (100), 
	[GNваг]			Text (100), 
	[Trichomonas vag]			Text (100), 
	[Trichomonas vagцер]			Text (100), 
	[Trichomonas vagваг]			Text (100), 
	[примечание]			Text (400), 
	[слизь]			Text (100), 
	[слизьцер]			Text (100), 
	[слизьваг]			Text (100)
);

CREATE TABLE [Папаниколау]
 (
	[Код паціента]			Long Integer, 
	[Дата]			DateTime, 
	[Код заказа]			Long Integer, 
	[Матеріал]			Text (100), 
	[Спосіб отимання]			Text (100), 
	[Тип]			Text (500), 
	[Опис]			Text (500), 
	[Палички]			Text (100), 
	[Коки]			Text (100), 
	[Диплококи]			Text (100), 
	[Біоценоз]			Text (200), 
	[Ключеві]			Text (200), 
	[Характерні]			Text (500), 
	[Примітки]			Text (500)
);

CREATE TABLE [Паціенти]
 (
	[Код паціента]			Long Integer, 
	[ПІБ]			Text (300) NOT NULL, 
	[Стать]			Text (10), 
	[Вік]			Long Integer, 
	[Дослід]			Long Integer NOT NULL, 
	[Прим]			Text (500), 
	[Дата]			DateTime NOT NULL, 
	[Лікар]			Text (300) NOT NULL, 
	[Прим1]			Text (100), 
	[сеча]			Boolean NOT NULL, 
	[ліквор]			Boolean NOT NULL, 
	[інше]			Boolean NOT NULL, 
	[вичеркнути]			Boolean NOT NULL, 
	[поліс]			Text (100), 
	[врач велеса]			Text (100), 
	[строк вагітності]			Text (100), 
	[зіскріб]			Boolean NOT NULL, 
	[кров]			Boolean NOT NULL, 
	[слина]			Boolean NOT NULL, 
	[датабокса]			DateTime, 
	[нумер]			Long Integer, 
	[email]			Text (100), 
	[код лікаря]			Text (100), 
	[import]			Boolean NOT NULL
);

CREATE TABLE [Печать]
 (
	[Печать]			OLE (255), 
	[зірка]			OLE (255), 
	[Підпис]			OLE (255)
);

CREATE TABLE [Подпись]
 (
	[Подпись]			OLE (255)
);

CREATE TABLE [Прайс всех]
 (
	[Лікар]			Text (500), 
	[Код аналіза]			Long Integer, 
	[Ціна всіх]			Long Integer
);

CREATE TABLE [Сертификат]
 (
	[Код паціента]			Long Integer, 
	[HBV]			Text (100), 
	[HCV]			Text (100), 
	[HAV]			Text (100), 
	[HIV 1,2]			Text (100), 
	[LUETIC]			Text (100), 
	[номер]			Text (100), 
	[Doctor]			Text (300), 
	[примечание]			Text (500)
);

CREATE TABLE [Сеча з калом]
 (
	[Код паціента]			Long Integer, 
	[Дата]			DateTime, 
	[кількість сечі]			Text (100), 
	[уд вага]			Text (100), 
	[колір]			Text (100), 
	[прозорість]			Text (100), 
	[цукерман]			Text (100), 
	[білок]			Text (100), 
	[ацетон]			Text (100), 
	[осадок]			Text (500), 
	[кислотність]			Text (100), 
	[уробілін]			Text (100), 
	[білірубін]			Text (100), 
	[індексен]			Text (100), 
	[жовчні пігменти]			Text (100), 
	[лейкоцити]			Text (100), 
	[єритроцити]			Text (100), 
	[циліндри]			Text (100), 
	[єпітелій]			Text (100), 
	[епітелій уретри]			Text (100), 
	[єпітелій міхура]			Text (100), 
	[епітелій з нирок]			Text (100), 
	[солі]			Text (100), 
	[оксалати]			Text (100), 
	[урати]			Text (100), 
	[аморфні фосфати]			Text (100), 
	[трипельфосфати]			Text (100), 
	[циліндроїди]			Text (100), 
	[фібрін]			Text (100), 
	[слиз]			Text (100), 
	[форма кала]			Text (100), 
	[консистенція]			Text (100), 
	[колір калу]			Text (100), 
	[запах]			Text (100), 
	[слиз в калі]			Text (100), 
	[гной]			Text (100), 
	[реакція на кров]			Text (100), 
	[реакція з оцтом]			Text (100), 
	[реакція з ТХУ]			Text (100), 
	[реакція з сулемой]			Text (100), 
	[реакція на стеркобілин]			Text (100), 
	[реакція на білірубин]			Text (100), 
	[соединительная ткань]			Text (100), 
	[м"язові  волокна]			Text (100), 
	[нейтрольный жир]			Text (100), 
	[жирные кислоты]			Text (100), 
	[мила]			Text (100), 
	[непереваримая клетчатка]			Text (100), 
	[переваримая клетчатка]			Text (100), 
	[крахмал]			Text (100), 
	[иодоф бактерії]			Text (100), 
	[лейкоцити кала]			Text (100), 
	[єритроцити кала]			Text (100), 
	[єпітелій кала]			Text (100), 
	[простейшие]			Text (100), 
	[яйця глист]			Text (500), 
	[результат]			Text (500), 
	[мікроскопія калу]			Text (100)
);

CREATE TABLE [Скрининг]
 (
	[Код паціента]			Long Integer, 
	[Ureaplasma urealyticum]			Text (100), 
	[Mycoplasma hominis]			Text (100), 
	[Trichomonas vaginalis]			Text (100), 
	[Escherihia coli]			Text (100), 
	[Proteus]			Text (100), 
	[Pseudomonas]			Text (100), 
	[Gardnerella vaginalis]			Text (100), 
	[Staphylococcus aureus]			Text (100), 
	[Streptococcus feacalis]			Text (100), 
	[Neisseria gonorrheae]			Text (100), 
	[Streptococcus agalactiae]			Text (100), 
	[Candida]			Text (100), 
	[Date]			DateTime, 
	[Tetracycline 8mkg/ml]			Text (100), 
	[Perfloxacin 16mkg/ml]			Text (100), 
	[Ofloxacine 4mkg/ml]			Text (100), 
	[Erythromycin 16mkg/ml]			Text (100), 
	[Clarithromicin 16mkg/ml]			Text (100), 
	[Mynocycline 8mkg/ml]			Text (100), 
	[Josamicin 8mkg/ml]			Text (100), 
	[Clindamycin 8mkg/ml]			Text (100), 
	[Doxycycline 8mkg/ml]			Text (100), 
	[Pristinamycin 2mg/l]			Text (100), 
	[Roxithromycin 2mg/l]			Text (100), 
	[Cyprofloxacin 2mg/l]			Text (100), 
	[Levofloxacin 4mg/l]			Text (100)
);

CREATE TABLE [Спермограма]
 (
	[Код паціента]			Long Integer, 
	[дата]			DateTime, 
	[кількість]			Text (100), 
	[колір]			Text (100), 
	[запах]			Text (100), 
	[реакція]			Text (100), 
	[час розрідження]			Text (100), 
	[консистенція]			Text (100), 
	[кількість в 1 мл]			Text (100), 
	[рухливість]			Text (100), 
	[нормокінезис]			Text (100), 
	[гіпокінезис]			Text (100), 
	[акінезис]			Text (100), 
	[кількість живих]			Text (100), 
	[кількість мертвих]			Text (100), 
	[патологічні форми]			Text (100), 
	[головка]			Text (100), 
	[шийка]			Text (100), 
	[хвіст]			Text (100), 
	[тіло]			Text (100), 
	[лейкоцити]			Text (100), 
	[эритроцити]			Text (100), 
	[эпітелий]			Text (100), 
	[лецитинові зерна]			Text (100), 
	[кристали Бехтерева]			Text (100), 
	[клітини сперматогенеза]			Text (100), 
	[аглютинація]			Text (100), 
	[Заключение]			Text (500), 
	[слиз]			Text (100), 
	[нормальні форми]			Text (100), 
	[дегенеративні форми]			Text (100), 
	[загальна кількість]			Text (100)
);

CREATE TABLE [Фемофлор]
 (
	[Код паціента]			Long Integer, 
	[Дата]			DateTime, 
	[Код заказа]			Long Integer, 
	[Контроль взятия материала]			Boolean NOT NULL, 
	[Общая бактериальная масса]			Single, 
	[ВК]			Single, 
	[Lactobacillus spp]			Single, 
	[семейсто Enterobacteriaceae]			Single, 
	[Streptococcus spp]			Single, 
	[Staphylococcus spp]			Single, 
	[Маркер S]			Single, 
	[Gardnerella vag]			Single, 
	[Eubacterium spp]			Single, 
	[Sneathia spp]			Single, 
	[Megasphaera spp]			Single, 
	[Lachnobacterium spp]			Single, 
	[Mobiluncus spp]			Single, 
	[Peptostreptococcus spp]			Single, 
	[Atopobium vaginae]			Single, 
	[Candida spp]			Single, 
	[КВМ]			Single, 
	[Маркер]			Single, 
	[Mycoplasma spp]			Single, 
	[Uteaplasma urea parvum]			Single, 
	[Mycoplasma genitalium]			Single, 
	[Заключение]			Text (500)
);

CREATE TABLE [Цитологія]
 (
	[Код паціента]			Long Integer, 
	[Дата]			DateTime, 
	[Код заказа]			Long Integer, 
	[Висновок]			Text (500), 
	[Клітини епітелію у великій кількості]			Boolean NOT NULL, 
	[у помірній кількості]			Boolean NOT NULL, 
	[відсутні]			Boolean NOT NULL, 
	[Розташований переважно пластами та окремо]			Boolean NOT NULL, 
	[окремо]			Boolean NOT NULL, 
	[серед еритроцитів]			Boolean NOT NULL, 
	[Представлений клітинами поверхневого та проміжного шарів]			Boolean NOT NULL, 
	[поверхневого шару]			Boolean NOT NULL, 
	[парабазальні клітини в невеликій кількості]			Boolean NOT NULL, 
	[в великій кількості]			Boolean NOT NULL, 
	[парабазальні клітини відсутні]			Boolean NOT NULL, 
	[Без дегенеративних змін]			Boolean NOT NULL, 
	[З дегенеративними змінами]			Boolean NOT NULL, 
	[з явищами вираженого дискоріозу]			Boolean NOT NULL, 
	[двух ядерних клітин]			Boolean NOT NULL, 
	[Циліндричний епітелій в невеликій кількості]			Boolean NOT NULL, 
	[Циліндричний епітелій в помірній кількості]			Boolean NOT NULL, 
	[Циліндричний епітелій в великій кількості]			Boolean NOT NULL, 
	[Розташован групами]			Boolean NOT NULL, 
	[на слизу]			Boolean NOT NULL, 
	[сукупченнями]			Boolean NOT NULL, 
	[поодиноко]			Boolean NOT NULL, 
	[Деякі клітини у стані плоскоклітинної метаплазії]			Boolean NOT NULL, 
	[з явищами проліферації]			Boolean NOT NULL, 
	[без особливостей]			Boolean NOT NULL, 
	[слиз у невеликій кількості]			Boolean NOT NULL, 
	[слиз у помірній кількості]			Boolean NOT NULL, 
	[дріжподібний грибок відсутній]			Boolean NOT NULL, 
	[виявлені елементи дріжподібних грибів]			Boolean NOT NULL, 
	[Лейкоцити у полі зору]			Text (200), 
	[Палички Дедерлейна відсутні]			Boolean NOT NULL, 
	[Палички Дедерлейна присутні]			Boolean NOT NULL, 
	[Флора палички в помірній кількості]			Boolean NOT NULL, 
	[Флора палички відсутні]			Boolean NOT NULL, 
	[Флора коки в помірній кількості]			Boolean NOT NULL, 
	[Флора коки відсутня]			Boolean NOT NULL, 
	[1Клітини епітелію у великій кількості]			Boolean NOT NULL, 
	[1у помірній кількості]			Boolean NOT NULL, 
	[1відсутні]			Boolean NOT NULL, 
	[1Розташований переважно пластами та окремо]			Boolean NOT NULL, 
	[1окремо]			Boolean NOT NULL, 
	[1серед еритроцитів]			Boolean NOT NULL, 
	[1Представлений клітинами поверхневого та проміжного шарів]			Boolean NOT NULL, 
	[1поверхневого шару]			Boolean NOT NULL, 
	[1парабазальні клітини в невеликій кількості]			Boolean NOT NULL, 
	[1в великій кількості]			Boolean NOT NULL, 
	[1парабазальні клітини відсутні]			Boolean NOT NULL, 
	[1Без дегенеративних змін]			Boolean NOT NULL, 
	[1З дегенеративними змінами]			Boolean NOT NULL, 
	[1з явищами вираженого дискоріозу]			Boolean NOT NULL, 
	[1двух ядерних клітин]			Boolean NOT NULL, 
	[1Циліндричний епітелій в невеликій кількості]			Boolean NOT NULL, 
	[1Циліндричний епітелій в помірній кількості]			Boolean NOT NULL, 
	[1Циліндричний епітелій в великій кількості]			Boolean NOT NULL, 
	[1Розташован групами]			Boolean NOT NULL, 
	[1на слизу]			Boolean NOT NULL, 
	[1сукупченнями]			Boolean NOT NULL, 
	[1поодиноко]			Boolean NOT NULL, 
	[1Деякі клітини у стані плоскоклітинної метаплазії]			Boolean NOT NULL, 
	[1з явищами проліферації]			Boolean NOT NULL, 
	[1без особливостей]			Boolean NOT NULL, 
	[1слиз у невеликій кількості]			Boolean NOT NULL, 
	[1слиз у помірній кількості]			Boolean NOT NULL, 
	[1дріжподібний грибок відсутній]			Boolean NOT NULL, 
	[1виявлені елементи дріжподібних грибів]			Boolean NOT NULL, 
	[1Лейкоцити у полі зору]			Text (200), 
	[1Палички Дедерлейна відсутні]			Boolean NOT NULL, 
	[1Палички Дедерлейна присутні]			Boolean NOT NULL, 
	[1Флора палички в помірній кількості]			Boolean NOT NULL, 
	[1Флора палички відсутні]			Boolean NOT NULL, 
	[1Флора коки в помірній кількості]			Boolean NOT NULL, 
	[1Флора коки відсутня]			Boolean NOT NULL, 
	[2Клітини епітелію у великій кількості]			Boolean NOT NULL, 
	[2у помірній кількості]			Boolean NOT NULL, 
	[2відсутні]			Boolean NOT NULL, 
	[2Розташований переважно пластами та окремо]			Boolean NOT NULL, 
	[2окремо]			Boolean NOT NULL, 
	[2серед еритроцитів]			Boolean NOT NULL, 
	[2Представлений клітинами поверхневого та проміжного шарів]			Boolean NOT NULL, 
	[2поверхневого шару]			Boolean NOT NULL, 
	[2парабазальні клітини в невеликій кількості]			Boolean NOT NULL, 
	[2в великій кількості]			Boolean NOT NULL, 
	[2парабазальні клітини відсутні]			Boolean NOT NULL, 
	[2Без дегенеративних змін]			Boolean NOT NULL, 
	[2З дегенеративними змінами]			Boolean NOT NULL, 
	[2з явищами вираженого дискоріозу]			Boolean NOT NULL, 
	[2двух ядерних клітин]			Boolean NOT NULL, 
	[2Циліндричний епітелій в невеликій кількості]			Boolean NOT NULL, 
	[2Циліндричний епітелій в помірній кількості]			Boolean NOT NULL, 
	[2Циліндричний епітелій в великій кількості]			Boolean NOT NULL, 
	[2Розташован групами]			Boolean NOT NULL, 
	[2на слизу]			Boolean NOT NULL, 
	[2сукупченнями]			Boolean NOT NULL, 
	[2поодиноко]			Boolean NOT NULL, 
	[2Деякі клітини у стані плоскоклітинної метаплазії]			Boolean NOT NULL, 
	[2з явищами проліферації]			Boolean NOT NULL, 
	[2без особливостей]			Boolean NOT NULL, 
	[2слиз у невеликій кількості]			Boolean NOT NULL, 
	[2слиз у помірній кількості]			Boolean NOT NULL, 
	[2дріжподібний грибок відсутній]			Boolean NOT NULL, 
	[2виявлені елементи дріжподібних грибів]			Boolean NOT NULL, 
	[2Лейкоцити у полі зору]			Text (200), 
	[2Палички Дедерлейна відсутні]			Boolean NOT NULL, 
	[2Палички Дедерлейна присутні]			Boolean NOT NULL, 
	[2Флора палички в помірній кількості]			Boolean NOT NULL, 
	[2Флора палички відсутні]			Boolean NOT NULL, 
	[2Флора коки в помірній кількості]			Boolean NOT NULL, 
	[2Флора коки відсутня]			Boolean NOT NULL
);

CREATE TABLE [Алергиястрип]
 (
	[калибратор]			Single, 
	[проба]			Single, 
	[едениц калибратора]			Single, 
	[коментарий]			Text (500), 
	[счетчик]			Long Integer, 
	[код паціента]			Long Integer, 
	[Клещ домашней пыли 1]			Text (100), 
	[Клещ домашней пыли 2]			Text (100), 
	[Пыльца березы]			Text (100), 
	[Пыльца смеси трав]			Text (100), 
	[Эпителий и шерсть кошки]			Text (100), 
	[Эпителий и шерсть собаки]			Text (100), 
	[Грибок Alternaria alternate]			Text (100), 
	[Молоко]			Text (100), 
	[Альфа-лактоальбумин]			Text (100), 
	[Бета-лактоглобулин]			Text (100), 
	[Казеин]			Text (100), 
	[Яйцо-белок]			Text (100), 
	[Яйцо-желток]			Text (100), 
	[Говяжий сывороточный альбумин]			Text (100), 
	[Соя (бобы)]			Text (100), 
	[Морковь]			Text (100), 
	[Картофель]			Text (100), 
	[Пшеница-мука]			Text (100), 
	[Лесной орех (фундук)]			Text (100), 
	[Арахис]			Text (100)
);

CREATE TABLE [Записна]
 (
	[Дата]			DateTime, 
	[ПІБ]			Text (100), 
	[телефон]			Text (100), 
	[місце роботи]			Text (100), 
	[адреса]			Text (100), 
	[хто він]			Text (100), 
	[чим займається]			Text (100), 
	[припис]			Text (500)
);

CREATE TABLE [Кал]
 (
	[Код паціента]			Long Integer, 
	[Дата]			DateTime, 
	[Кількість]			Text (100), 
	[Форма]			Text (100), 
	[Консистенція]			Text (100), 
	[Колір]			Text (100), 
	[Запах]			Text (100), 
	[Залишки їжі]			Text (100), 
	[Слиз]			Text (100), 
	[Кров]			Text (500), 
	[Гній]			Text (100), 
	[Конкременти]			Text (100), 
	[Реакція]			Text (100), 
	[Прихована кров]			Text (100), 
	[Жовчні пігменти]			Text (100), 
	[Проба]			Text (100), 
	[Мьязові волокна]			Text (500), 
	[Послинна клійковина перетравлена]			Text (500), 
	[Жир нейтральний]			Text (100), 
	[Жирні кислоти]			Text (100), 
	[Мила]			Text (100), 
	[Кристали]			Text (100), 
	[Епітеліальні клітини]			Text (100), 
	[Лейкоцити]			Text (100), 
	[Еритроцити]			Text (100), 
	[Ознака злоякісності]			Text (100), 
	[Йодофільна флора]			Text (100), 
	[Найпростіші]			Text (100), 
	[Яйця гельмінтів]			Text (100), 
	[Гриби]			Text (100), 
	[Висновок]			Text (510), 
	[Фрагменти гельмінтів]			Text (100)
);

CREATE TABLE [Менеджери]
 (
	[ПІБ]			Text (100), 
	[код]			Long Integer, 
	[відшкодування]			Single, 
	[хто]			Text (100)
);


