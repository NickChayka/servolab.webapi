<?php
//ini_set('display_errors','true');

require_once('../liqpay/constant.php');

//    $fp = fopen('/var/www/html/pumb/payment_result.log', 'a+');
//    fputs($fp, $_SERVER['REQUEST_URI']."\n\n");
//    fclose($fp);

$merch_id = $_GET['merch_id'];
$trx_id = $_GET['trx_id'];
$merchant_trx = $_GET['merchant_trx'];
$result_code = $_GET['result_code'];
$amount = $_GET['amount'];
$account_id = $_GET['account_id'];
$p_rrn = $_GET['p_rrn'];
$p_transmissionDateTime = $_GET['p_transmissionDateTime'];
$p_maskedPan = $_GET['p_maskedPan'];
$p_cardholder = $_GET['p_cardholder'];
$p_authcode = $_GET['p_authcode'];
$p_isFullyAuthenticated = $_GET['p_isFullyAuthenticated'];
$ts = $_GET['ts'];
$signature = $_GET['signature'];
$user_id = $_GET['o_user_id'];
$order_id = (int)$_GET['o_order_id'];

if($merch_id != PUMB_MERCH_ID) {
    $res_desc = 'Это не наша операция, merch_id не совпадает.';
} else {
    $SERVER_PROTOCOL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') ? "https://" : "http://";
    $request = explode('&signature=', $SERVER_PROTOCOL . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    $data = $request[0];
    $signature = base64_decode(urldecode($request[1]));
    $fp = fopen($_SERVER['DOCUMENT_ROOT'].'/pumb/pumb-prod.pem', "r");
    $cert = fread($fp, 8192);
    fclose($fp);
    $public_key = openssl_pkey_get_public($cert);
    $signature_check = openssl_verify($data, $signature, $public_key, OPENSSL_ALGO_SHA1);
    openssl_free_key($public_key);
    if($signature_check != 1) {
        $res_desc = 'Сигнатура не совпадает.';
    } else {
        $rs = $link->query('select * from servolab_watchdog.payment_request where status="new" and id='.$order_id);
        if($rs->num_rows == 1) {
            $r = $rs->fetch_assoc();
            if($user_id == md5($r['fio']) and $amount == $r['sum']*100) {
                // надо обработать результат
                $order_status = ($result_code == 1 ? 'success' : 'error');

                $sql = 'update servolab_watchdog.payment_request set status="'.$link->escape_string($order_status).'", 
                    answer="'.$link->escape_string($data).'" 
                    where id='.(int)$order_id;
                $link->query($sql);

                $res_desc = 'OK';
            } else {
                $res_desc = 'Что-то пошло не так, не совпадают данные платежа.';
            }
        } else {
            $res_desc = 'Извините, не могу найти такого платежа.';
        }
    }
}
?>
<?xml version="1.0"?>
<register-payment-response>
    <result>
        <code>1</code>
        <desc><?php echo $res_desc?></desc>
    </result>
</register-payment-response>