[TOC]

## Регистрация результата платежа RegisterPayment
После звершения платежа сервер банка обращается GET-запросом по *RegisterPayment URL* магазина для передачи статуса и параметров платежа.
### Параметры GET-запроса RegisterPayment
Параметр    | Тип    | Описание
-----------:|:------:|:--------:
merch_id    | String | Идентификатор магазина. Длина **32** символа.
trx_id      | String | Идентификатор транзакции. Длина **32** символа.
merchant_trx| String | Идентификатор транзакции в магазине, полученный в ответе на этапе <a href="/Выполнение_платежа/Проверка_возможности_приема_платежа_CheckPaymentAvail">CheckPaymentAvail</a>
result_code | Number | Код результата платежа. `1` — успешный платеж, `2` — неудачный платеж.
amount      | Number | Сумма платежа.
account_id  | String | Идентификатор счета магазина, на который был осуществлен перевод денег.
p.rrn       | String | Идентификатор платежа в процессинговом центре банка-эквайера. 
p.transmissionDateTime | String | Дата и время авторизации успешного платежа в формате `mdYHis.` Параметр присутствует если `result_code` = 1.
p.maskedPan | String | Маскированный номер карты.
p.cardholder | String | Имя держателя карты.
p.authcode  | String | Код авторизации, который отражается у банка-эмитента, банка-эквайера и платежной системы.
p.isFullyAuthenticated | String | Признак наличия проверки 3-D Secure при выполнении транзакции.
ts          | String | Дата и время выполнения запроса в формате `Ymd H:i:s`.
signature   | String | ЭЦП банка-эквайера под запросом в формате PEM (кодированный Base64). Подписывается весь сформированный URL начиная c `https://` (включительно) вплоть до `&signature=` (не включая `&signature=`). Параметр используется в целях безопасности и служит для проверки достоверности ответа сервера банка.
o.*         | String | Дополнительные параметры заказа. Набор параметров и их названия определяются магазином на этапе <a href="/Выполнение_платежа/Инициирование_платежа">инициирования платежа</a>.

### Пример GET-запроса RegisterPayment

``https://www.merchant.ua/register-payment.php?trx_id=46622B749D0946EB37791DA383E315A9&merch_id=06A4A0FDA274668349DDC006D3CA0739&merchant_trx=624292&result_code=1&amount=27282&account_id=12E890A5564AA0665F4976F0A6AB1F0E&o.user_id=285167&o.amount=272.82&o.order_id=5907133&o.t_id=624292&p.maskedPan=487424xxxxxx9133&p.authcode=961195&p.cardholder=Mr.+Cardholder&p.rrn=009666668732&p.isFullyAuthenticated=N&ts=20160617+18%3A31%3A36&signature=B%2BfykbO9j5iOD3Wnqdg0ilD7aKWcwil421zXr00qI2jz6zLq7GHBx7Nz3rcLHZXVQxTaHuHuW%2B16%0AYk4kY0geDYkwB57stsBOMR2a84TWxy%2BQKJ9EjYF%2FUbqGPrInNtPkN7s5LkWJ81t9TiAvEGAa%2F9km%0ACX3Igxmspt1b5kR4f2fwsanMpBmWyDW621vcd0cIP25lQIMokk1Awn5oVeIjRhDL7%2BREtyZSIfmr%0ADa1fPN4fy9vgVqcmwPvP2iN7DaRhGLzpLgtFJmc54wZGKDXP2Nv2vFvBa8ck3AxH1Mtp8L3UCwgX%0Ah5M6ms9j6VKguwb8rff7d2dzqUpzZngk%2BWvgiw%3D%3D``

### Параметры ответа магазина на RegisterPayment
По *RegisterPayment URL* магазин отвечает банку в формате XML о получении результата платежа. Магазин может ответить двумя способами:
* подтвердить получение результата платежа;
* сообщить о возникновении проблемы при регистрации результата по техническим причинам. В этом случае сервер банка предпримет новые попытки зарегистрировать результат платежа позже (в соответствии с настройками).

Параметр           | Обязательность | Тип    | Описание
------------------:|:--------------:|:------:|:--------:
result.code        | √              | Number | Результат регистрации платежа. `1` — успешный, `2` — неуспешный.
result.desc        | √              | String | Описание результата. Максимальная длина **125** символов.

#### Пример ответа магазина на RegisterPayment при result.code = 1
	<?xml version="1.0"?>
	<register-payment-response>
		<result>
			<code>1</code>
			<desc>OK</desc>
		</result>
	</register-payment-response>
	
#### Пример ответа магазина на RegisterPayment при result.code = 2
	<?xml version="1.0"?>
	<result>
		<code>2</code>
		<desc>Temporary unavailable.</desc>
	</result>
	</register-payment-response>
#### Пример проверки ЭЦП банка-эквайера
	<?php
	
	$SERVER_PROTOCOL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') ? "https://" : "http://";

	$request = explode('&signature=', $SERVER_PROTOCOL . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

	$data = $request[0];
	$signature = base64_decode(urldecode($request[1]));

	$fp = fopen($_SERVER['DOCUMENT_ROOT'].'/path/to/*.crt', "r");
	$cert = fread($fp, 8192);
	fclose($fp);
	
	$public_key = openssl_pkey_get_public($cert);
	$signature_check = openssl_verify($data, $signature, $public_key, OPENSSL_ALGO_SHA1);
    openssl_free_key($public_key);
	
	?>