<?php
    require_once('../LiqPay/constant.php');

    $merch_id = $_GET['merch_id'];
    $user_id = $_GET['o_user_id'];
    $order_id = (int)$_GET['o_order_id'];
    $amount = $_GET['o_amount'];

    if($merch_id != PUMB_MERCH_ID) {
        $res_code = 2;
        $res_desc = 'Это не наша операция, merch_id не совпадает.';
        $payment = '';
    } else {
        $rs = $link->query('select * from servolab_watchdog.payment_request where status="new" and id='.$order_id);
        if($rs->num_rows == 1) {
            $r = $rs->fetch_assoc();

            if($user_id == md5($r['fio']) and $amount == $r['sum']) {
                $res_code = 1;
                $res_desc = 'OK';
                $payment = '
            <merchant-trx>'.$order_id.'</merchant-trx>
            <purchase>
                <shortDesc>'.substr($r['descr'], 0, 30).'</shortDesc>
                <longDesc>'.substr($r['descr'], 0, 125).'</longDesc>
                <account-amount>
                    <id>'.PUMB_ACCOUNT_ID.'</id>
                    <amount>'.($r['sum']*100).'</amount>
                    <currency>980</currency>
                    <exponent>2</exponent>
                </account-amount>
            </purchase>';
            } else {
                $res_code = 2;
                $res_desc = 'Что-то пошло не так, не совпадают данные платежа.';
                $payment = '';
            }
        } else {
            $res_code = 2;
            $res_desc = 'Извините, не могу найти такого платежа.';
            $payment = '';
        }
    }
?>
<?xml version="1.0"?>
<payment-avail-response>
    <result>
        <code><?php echo $res_code?></code>
        <desc><?php echo $res_desc?></desc>
    </result>
    <?php echo $payment?>
</payment-avail-response>