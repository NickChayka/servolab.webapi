<?php
ini_set('display_errors', 'true');

define('DB_HOST', '146.148.29.126');
define('DB_USERNAME', 'webapi');
define('DB_PASSWORD', 'cTkERhuYM3pe97NW');
define('DB_NAME', 'servolab_watchdog');

    $link = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if(!$link) die('Mysql connect error');
    $link->query("set names utf8mb4");
    $link->query('SET time_zone = "Europe/Kiev"');

    $fp = fopen('lpu.csv', 'r');
    if(!$fp) die("Can't open lpu.csv");
    $str = fgets($fp); // пропустим заголовки
    $tname = 'lpu_20191121';
    $tname_tmp = $tname.'_tmp';
    @$link->query('drop table if exists '.$tname_tmp);
    @$link->query('create table '.$tname_tmp.' like '.$tname);

//    $c = 0;

    while($str = fgets($fp)) {
        if(trim($str)=='') continue;
        $sql = 'insert into '.$tname_tmp.'(num, name, type, obl, city, lat, lon) values(';
        $str = str_replace("#N/A", "", $str);
        $str = str_replace("#DIV/0!", "", $str);
        $str = str_replace('"', "", $str);
        list($num, $name, $type, $obl, $city, $lat, $lon) = explode("\t", $str);

        $num = trim($num);
        $name = trim($name);
        $type = trim($type);
        $obl = trim($obl);
        $city = trim($city);
        $lat = trim($lat); if($lat == '') $lat=0;
        $lon = trim($lon); if($lon == '') $lon=0;

        $sql.='"'.$num.'","'.$link->escape_string($name).'","'.$link->escape_string($type).'","'.$link->escape_string($obl).'","'.$link->escape_string($city).
            '","'.$lat.'","'.$lon.'"';
        $sql.=')';
        if(!@$link->query($sql)) {
            echo $sql."<br>\n";
            $error_no = $link->errno;
            $error_msg = $link->error;
            echo 'Error code: '.$error_no.', description: '.$error_msg."<br>\n";
        }

//        $c++;
    }

    @$link->query('drop table if exists '.$tname.'_old');
    @$link->query('rename table '.$tname.' to '.$tname.'_old');
    @$link->query('rename table '.$tname_tmp.' to '.$tname);

    $link->close();
    echo 'ok.'."\n";

////////////////////////////////////////

?>
