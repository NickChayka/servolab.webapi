<?php
//    ini_set('display_errors','true');

    require_once('constant.php');
    if($_POST) {

        $pfio = $_POST['pfio'];
        $psum = $_POST['psum'];
        $desc = $_POST['pdesc'];
        $bank = $_POST['bank'];
        $request_id = (int)$_POST['request_id'];

        if($pfio != '' and $desc != '' and $psum > 0) {
            $hkey = md5(time().$pfio.$psum.$desc);
            $link->query('insert into servolab_watchdog.payment_request(bank, fio, sum, descr, status, hkey, created, online_request_id) 
                values("'.$link->escape_string($bank).'","'.$link->escape_string($pfio).'","'.$link->escape_string($psum).'",
                    "'.$link->escape_string($desc).'","new","'.$link->escape_string($hkey) .'",
                    "'.date('Y-m-d H:i:s').'", "'.$request_id.'")');
            $order_id = $link->insert_id;
            if($order_id > 0) {
                $html = $liqpay->cnb_form(array(
                        'action'         => 'pay',
                        'amount'         => $psum,
                        'currency'       => 'UAH',
                        'description'    => $desc,
                        'order_id'       => SERVER_ENV.'_'.$order_id,
                        'version'        => '3',
                        'result_url'     => SERVER_URI.'payment_result.php'
                    ));
                echo 'Для совершения оплаты нужно перейти на сайт LiqPay нажав на кнопку ниже:<p>';
                echo $html;

                $mlink = SERVER_URI.'ready_to_pay.php?hkey='.$hkey;
                $json = file_get_contents('https://cutt.ly/api/api.php?key=3f38c6c78d64b372fb58fa0a0da018115c2a9&short='.$mlink/*.'&name=order_'.$order_id*/);
                $data = json_decode ($json, true);
                $short_link = '';
                if($data["url"]["status"] == 7) {
                    $short_link = $data["url"]["shortLink"];
                    echo '<p>Или можно воспользоваться короткой ссылкой: '.$short_link;
                    $link->query('update servolab_watchdog.payment_request set short_link="'.$link->escape_string($short_link).'" where id='.$order_id);
                } else {
                    $err = '';
                    switch($data["url"]["status"]) {
                        case 1: $err = 'The shortened link comes from the domain that shortens the link, i.e. the link has already been shortened.'; break;
                        case 2: $err = 'The entered link is not a link.'; break;
                        case 3: $err = 'The preferred link name is already taken.'; break;
                        case 4: $err = 'Invalid API key.'; break;
                        case 5: $err = 'The link has not passed the validation. Includes invalid characters.'; break;
                        case 6: $err = 'The link provided is from a blocked domain.'; break;
                    }
                    echo 'Не смог получить короткую ссылку. Код ошибки '.$data["url"]["status"].'. '.$err;
                }
                exit();
            } else die('SQL Ошибка при сохранении ордера. <a href="create_order.php">Вернуться</a>');
        } else die('Проверьте введённые данные. <a href="create_order.php">Вернуться</a>');
    }