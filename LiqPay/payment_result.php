<?php
    require_once('constant.php');

    $data = $_POST['data'];
    $signature = $_POST['signature'];

//    $fp = fopen('/home/servolab/webapi/www/LiqPay/payment_result.log', 'a+');
//    fputs($fp, $data."\n".$signature."\n\n");
//    fclose($fp);

    $sign = base64_encode(sha1(PRIVATE_KEY.$data.PRIVATE_KEY, 1));

    if($sign == $signature) { // Да мы совпадаем, всё хорошо..
        $jsonStr = base64_decode($data);
        $json = json_decode($jsonStr);

        list($mode, $order_id) = explode('_', $json->order_id);

        $rs = $link->query('select * from servolab_watchdog.payment_request where id='.(int)$order_id);
        if($rs->num_rows != 1) {
            header('Location: https://servolab.one/');
            exit();
        }
        $r = $rs->fetch_assoc();
        if($r['online_request_id'] > 0 ) {
            $link->query('update servolab_medrep.request_detail set 
                actual_cost="'.($json->amount*100).'", payment_type="online", is_payed=1
                where request_id='.(int)$r['online_request_id']);
        }

        $sql = 'update servolab_watchdog.payment_request set status="'.$link->escape_string($json->status).'", answer="'.$link->escape_string($jsonStr).'" where id='.(int)$order_id;
        $link->query($sql);

        if($json->status != 'success' and $json->status != 'wait_accept') {
            header('Location: https://servolab.one/');
            exit();
        } else {
            echo 'Дякуємо, платіж зараховано.';
        }
        //echo 'Operation status: '.$json->status;

    } else echo 'Сигнатуры не совпадают';
?>
<p><button onclick="document.location.href='https://servolab.one/'">Повернутись до сайту</button></p>
