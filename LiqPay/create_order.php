<?php
//    ini_set('display_errors','true');

    require_once('constant.php');

    check_session();

    $order_id=(int)$_GET['order_id'];

    if($_POST) {

        $pfio = $_POST['pfio'];
        $psum = $_POST['psum'];
        $desc = $_POST['pdesc'];
        $bank = $_POST['bank'];

        if($pfio != '' and $desc != '' and $psum > 0) {

            $hkey = md5(time().$pfio.$psum.$desc);

            $link->query('insert into servolab_watchdog.payment_request(bank, fio, sum, descr, status, hkey, created) 
                values("'.$link->escape_string($bank).'","'.$link->escape_string($pfio).'","'.$link->escape_string($psum).'","'.$link->escape_string($desc).'","new","'.$link->escape_string($hkey) .'","'.date('Y-m-d H:i:s').'")');
            $order_id = $link->insert_id;
            if($order_id > 0) {
                if($bank == 'liqpay') {
                    $html = $liqpay->cnb_form(array(
                        'action'         => 'pay',
                        'amount'         => $psum,
                        'currency'       => 'UAH',
                        'description'    => $desc,
                        'order_id'       => SERVER_ENV.'_'.$order_id,
                        'version'        => '3',
                        'result_url'     => SERVER_URI.'payment_result.php'
                    ));
                    echo 'Для совершения оплаты нужно перейти на сайт LiqPay нажав на кнопку ниже:<p>';
                    echo $html;
                } elseif($bank == 'pumb') {
                    echo 'Для совершения оплаты нужно перейти на сайт PUMB нажав на кнопку ниже:<p>';
                    $html = '<form action="'.PUMB_API_URI.'" method="GET" charset="utf-8" enctype="application/x-www-form-urlencoded">
                        <input type="hidden" name="merch_id" value="'.PUMB_MERCH_ID.'" />
                        <input type="hidden" name="back_url_s" value="'.PUMB_BACK_SUCCESS.'" />
                        <input type="hidden" name="back_url_f" value="'.PUMB_BACK_FAIL.'" />
                        <input type="hidden" name="o.user_id" value="'.md5($pfio).'" />
                        <input type="hidden" name="o.order_id" value="'.$order_id.'" />
                        <input type="hidden" name="o.amount" value="'.$psum.'" />
                        <button type="submit">Оплатить</button>
                    </form>';
                    echo $html;
                }

                $mlink = SERVER_URI.'ready_to_pay.php?hkey='.$hkey;
                $json = file_get_contents('https://cutt.ly/api/api.php?key=3f38c6c78d64b372fb58fa0a0da018115c2a9&short='.$mlink/*.'&name=order_'.$order_id*/);
                $data = json_decode ($json, true);
                $short_link = '';
                if($data["url"]["status"] == 7) {
                    $short_link = $data["url"]["shortLink"];
                    echo '<p>Или можно воспользоваться короткой ссылкой: '.$short_link;
                    $link->query('update servolab_watchdog.payment_request set short_link="'.$link->escape_string($short_link).'" where id='.$order_id);
                } else {
                    $err = '';
                    switch($data["url"]["status"]) {
                        case 1: $err = 'The shortened link comes from the domain that shortens the link, i.e. the link has already been shortened.'; break;
                        case 2: $err = 'The entered link is not a link.'; break;
                        case 3: $err = 'The preferred link name is already taken.'; break;
                        case 4: $err = 'Invalid API key.'; break;
                        case 5: $err = 'The link has not passed the validation. Includes invalid characters.'; break;
                        case 6: $err = 'The link provided is from a blocked domain.'; break;
                    }
                    echo 'Не смог получить короткую ссылку. Код ошибки '.$data["url"]["status"].'. '.$err;
                }
                exit();
            } else die('SQL Ошибка при сохранении ордера. <a href="create_order.php">Вернуться</a>');
        } else die('Проверьте введённые данные. <a href="create_order.php">Вернуться</a>');
    } else {

        $rs = $link->query('select * from servolab_watchdog.payment_request where id='.$order_id);
        if($rs->num_rows == 1) {
            $r = $rs->fetch_assoc();
            $pfio = $r['fio'];
            $psum = $r['sum'];
            $desc = $r['descr'];
            $bank = $r['bank'];
        } else {
            $pfio = $psum = $desc = '';
            $bank = 'liqpay';
        }
    }

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Создать заявку на оплату</title>
    <script language="JavaScript">
        function form_check() {
            let frm = document.forms[0];
            let ret = false;
            if(frm.pfio.value == "") alert('Укажите ФИО контрагента, которому выставляем счёт');
            else if(frm.psum.value == "") alert('Укажите сумму, на которую выставляем счёт');
            else if(frm.pdesc.value == "") alert('Укажите назначение платежа');
            else ret = true;
            return ret;
        }
    </script>
</head>
<body bgcolor="blue">
<form action="create_order.php" method="post" onsubmit="return form_check()">
    <table align="center" border="1" width="500" cellpadding="5" cellspacing="0" bgcolor="aqua">
        <tr>
            <td nowrap>ФИО плательщика</td>
            <td width="100%"><input type="text" name="pfio" style="width:100%" value="<?php echo $pfio?>"></td>
        </tr>
        <tr>
            <td>Сумма</td>
            <td><input type="text" name="psum" style="width:100%" value="<?php echo $psum?>"></td>
        </tr>
        <tr>
            <td>Платёжная система:</td>
            <td>
                <select name="bank"  style="width:100%">
                    <option value="liqpay" <?php if($bank == 'liqpay') echo 'selected'?>>LiqPay</option>
                    <option value="pumb" <?php if($bank == 'pumb') echo 'selected'?>>ПУМБ (метод тестируется)</option>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2"><textarea maxlength="255" name="pdesc" placeholder="Назначение платежа" style="width:100%; height: 100px"><?php echo $desc?></textarea></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Создать заявку">
                <input type="reset" value="Сбросить">
                <button onclick="window.history.back(); return false">Назад</button>
            </td>
        </tr>
    </table>
</form>
</body>
</html>