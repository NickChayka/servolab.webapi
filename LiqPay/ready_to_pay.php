<?php
    require_once('constant.php');

    $hkey = $_GET['hkey'];

    $rs = $link->query('select * from servolab_watchdog.payment_request where status!="success" and hkey="'.$link->escape_string($hkey).'"');
    if($rs->num_rows == 1) {
        $r = $rs->fetch_assoc();

        if($r['bank'] == 'liqpay') {
            $form_data = $liqpay->cnb_form_raw(array(
                'action'         => 'pay',
                'amount'         => $r['sum'],
                'currency'       => 'UAH',
                'description'    => $r['descr'],
                'order_id'       => SERVER_ENV.'_'.$r['id'],
                'version'        => '3',
                'result_url'     => SERVER_URI.'payment_result.php'
            ));

            echo '<form method="POST" action="'.$form_data['url'].'" accept-charset="utf-8" name="pay_form">';
            echo '<input type="hidden" name="data" value="'.$form_data['data'].'" />';
            echo '<input type="hidden" name="signature" value="'.$form_data['signature'].'" />';
            echo '</form>';
        } elseif($r['bank'] == 'pumb') {
            echo '<form action="'.PUMB_API_URI.'" method="GET" charset="utf-8" enctype="application/x-www-form-urlencoded">
                        <input type="hidden" name="merch_id" value="'.PUMB_MERCH_ID.'" />
                        <input type="hidden" name="back_url_s" value="'.PUMB_BACK_SUCCESS.'" />
                        <input type="hidden" name="back_url_f" value="'.PUMB_BACK_FAIL.'" />
                        <input type="hidden" name="o.user_id" value="'.md5($r['fio']).'" />
                        <input type="hidden" name="o.order_id" value="'.$r['id'].'" />
                        <input type="hidden" name="o.amount" value="'.$r['sum'].'" />
                        <button type="submit" style="display: none">Оплатить</button>
                  </form>';
        }
        echo '<script language="javascript">document.forms[0].submit()</script>';

    } else echo 'Извините, не могу найти такого платежа.<p><button onclick="document.location.href=\'index.php\'">Вернуться на главную страницу</button></p>';
