<?php

ini_set('display_errors', 1);

    require_once('constant.php');

    check_session();

    $order_id = (int)$_GET['order_id'];

    $rs = $link->query('select * from servolab_watchdog.payment_request where id='.$order_id);
    if($rs->num_rows == 1) {
        $r = $rs->fetch_assoc();

        echo 'ID платежа: '.$r['id'].'<br>';
        echo 'ФИО: '.$r['fio'].'<br>';
        echo 'Сумма: '.$r['sum'].'<br>';
        echo 'Назначение: '.$r['descr'].'<br>';
        echo 'Статус: '.$r['status'].'<br>';

        if($r['bank'] == 'liqpay') {
            $res = $liqpay->api("request", array(
                'action'        => 'status',
                'version'       => '3',
                'order_id'      => SERVER_ENV.'_'.$order_id
            ));

            if($r['status'] != $res->status) {
                echo '<h3 style="color: red">ВНИМАНИЕ! Изменился статус платежа! Было <i style="color: blue">'.$r['status'].'</i>, стало <i style="color: green">'.$res->status.'</i>.</h3>';
                $link->query('update servolab_watchdog.payment_request set status = "'.$link->escape_string($res->status).'" where id='.$order_id);
            }

            echo 'Ответ от LiqPay шлюза:<pre>';
            print_r($res);
            echo '</pre>';
        } elseif($r['bank'] == 'pumb') {
            // тут надо проверить статус платежа в ПУМБ

        }





    } else echo 'Извините, не могу найти такого платежа.';
?>
<p><button onclick="window.history.back()">Вернуться назад</button></p>
