<?php
    require_once('constant.php');

    check_session();

    $page = (int)$_GET['page'];
    $per_page = 50;

    if($page < 0) $page = 0;

    $start = $page * $per_page;
    $rs = $link->query('select id, fio, sum, descr, status, hkey, short_link, updated, created, bank from servolab_watchdog.payment_request order by id desc');

    $total = $rs->num_rows;
    $rs->data_seek($start);
?>

<button onclick="document.location.href='create_order.php'">Создать новый платёж</button>
<p>
    <?php
        if($page > 0) {?>
            <button onclick="document.location.href='list_orders.php?page=<?=($page-1)?>'">Предыдущая страница</button>
        <?php }
        if($start+$per_page < $total) {?>
            <button onclick="document.location.href='list_orders.php?page=<?=($page+1)?>'">Следующая страница</button>
        <?php }
    ?>
</p>

<table border="1" cellspacing="0" cellpadding="3">
    <tr>
        <th>Id</th>
        <th>Банк</th>
        <th>ФИО</th>
        <th>Сумма</th>
        <th>Статус</th>
        <th>Описание</th>
        <!--th>Key</th-->
        <th>Ссылка</th>
        <th>Создано</th>
        <th>Изменено</th>
        <th>&nbsp;</th>
    </tr>
<?php
    for($i=0; $i<$per_page; $i++) {
        $r = $rs->fetch_assoc();
        if(!$r) break;
        echo '<tr>';
        echo '<td align="center">'.$r['id'].'</td>
            <td align="center" nowrap><img src="img/'.$r['bank'].'.png" hspace="5" height="32" border="0" alt="'.$r['bank'].'" title="'.$r['bank'].'"></td>
            <td align="center" nowrap>'.$r['fio'].'</td>
            <td align="center" nowrap>'.$r['sum'].'</td>
            <td align="center" nowrap>'.$r['status'].'</td>
            <td>'.$r['descr'].'</td>
            <!--td align="center" nowrap>'.$r['hkey'].'</td-->
            <td align="center" nowrap>'.($r['status']=='new' ? $r['short_link'] : '' ).'</td>
            <td align="center" nowrap>'.$r['created'].'</td>
            <td align="center" nowrap>'.$r['updated'].'</td>
            <td align="left" nowrap>
                <a href="order_status.php?order_id='.$r['id'].'"><img src="img/detail.png" hspace="5" height="32" border="0" alt="Детали" title="Детали"></a>
                <a href="create_order.php?order_id='.$r['id'].'"><img src="img/clone.png" hspace="5" height="32" border="0" alt="Клонировать" title="Клонировать"></a>';
        if($r['status']=='new')
            echo '<a href="order_operation.php?op=del&order_id='.$r['id'].'" onclick="return confirm(\'Вы уверены? Эта операция необратима!\')"><img src="img/delete.png" hspace="5" height="32" border="0" alt="Удалить" title="Удалить"></a>';
        echo '</td>';
        echo '</tr>';
    }
?>

</table>

<p>
    <?php
    if($page > 0) {?>
        <button onclick="document.location.href='list_orders.php?page=<?=($page-1)?>'">Предыдущая страница</button>
    <?php }
    if($start+$per_page < $total) {?>
        <button onclick="document.location.href='list_orders.php?page=<?=($page+1)?>'">Следующая страница</button>
    <?php }
    ?>
</p>
